/*
 * config.h - configuration options
 *
 *   Author: Gabor Toth <tgbp@freemail.hu>
 *   License: GPL
 *
 *   $Log: config.h,v $
 *   Revision 1.2  2006/05/10 21:22:34  koversrac
 *   Comment with author and license has been added.
 *
 */

#ifndef __config_h
#define __config_h

#define SERVER_ADDRESS		"127.0.0.1"
#define SERVER_PORT		8000
#define SERVER_LISTENQ		10

#define DEBUG_LEVEL		1

#endif
