//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, U$
//
// Copyright (C) 2005-2010  Oliver Schroeder
//

//////////////////////////////////////////////////////////////////////
//
//  server for FlightGear
//
//////////////////////////////////////////////////////////////////////

#if !defined FGMS_HXX
#define FGMS_HXX

using namespace std;

#include <fglib/fg_daemon.hxx>
#include <fglib/fg_list.hxx>
#include <flightgear/Network/fg_proto.hxx>

//////////////////////////////////////////////////////////////////////
//
//  a class for relay servers
//
//////////////////////////////////////////////////////////////////////
class Relay
{
public:
	uint64_t	ID;
	time_t		Time;
	NetAddr		Addr;
	string		Name;
	string  	Location;
	time_t		LastSeen;
	// statistical data
	uint64_t	PktsRcvd;
	uint64_t	PktsSent;
	uint64_t	BytesRcvd;
	uint64_t	BytesSent;

	Relay ();
	void UpdateSent ( size_t bytes );
	void UpdateRcvd ( size_t bytes );
	virtual bool operator ==  ( const Relay& R )
	{
		if ( Addr == R.Addr )
			return ( true );
		return ( false );
	}

	virtual bool operator !=  ( const Relay& R )
	{
		if ( Addr == R.Addr )
			return ( false );
		return ( true );
	}
	friend std::ostream& operator << ( std::ostream& o, Relay& R );
}; // class Relay

//////////////////////////////////////////////////////////////////////
//
//  a class for clients
//
//////////////////////////////////////////////////////////////////////
class Pilot
{
public:
	enum eSTATE
	{
		REGISTER,
		LOCALLY_AUTHENTICATED,
		REMOTLY_AUTHNTICATED
	};
	uint64_t	ID;
	time_t		Time;
	NetAddr		Addr;
	string		Name;
	string  	Password;
	string  	Origin;
	bool		IsLocal;
	time_t		JoinTime;
	time_t		LastSeen;
	time_t		LastSent;
	uint64_t	NumPingsSent;
	uint64_t	NumPingsRcvd;
	uint64_t	NumPongs;
	fgms::eCLIENTTYPES Type;

	/// when did we sent updates of this player to inactive relays
	time_t		LastRelayedToInactive;
	/// true if we need to send updates to inactive relays
	bool		DoUpdate;
	// statistical data
	uint64_t	PktsRcvd;
	uint64_t	PktsSent;
	uint64_t	BytesRcvd;
	uint64_t	BytesSent;
	eSTATE		State;
	SGPropertyNode	Properties;
	
	Pilot ();
	virtual bool operator ==  ( const Pilot& R )
	{
		if ( Addr == R.Addr )
			return ( true );
		return ( false );
	}

	virtual bool operator !=  ( const Pilot& R )
	{
		if ( Addr == R.Addr )
			return ( false );
		return ( true );
	}
	friend std::ostream& operator << ( std::ostream& o, Pilot& P );
}; // class Pilot

//////////////////////////////////////////////////////////////////////
//
// Servers of infrastructure: FGLS and FGAS
//
//////////////////////////////////////////////////////////////////////
class Server
{
public:
	time_t	LastSeen;
	time_t	RegisteredAt;
	int	NumPingsSent;
	int	NumPingsRcvd;
	int	NumPongs;
	bool	IsValid;
	NetAddr	Addr;

	Server ()
	{
		LastSeen	= 0;
		RegisteredAt	= 0;
		NumPingsSent	= 0;
		NumPingsRcvd	= 0;
		NumPongs	= 0;
		IsValid		= 0;
	};
};

//////////////////////////////////////////////////////////////////////
//
//  the FGMS class
//
//////////////////////////////////////////////////////////////////////
class FGMS : public ProtocolHandler
{
public:
	//////////////////////////////////////////////////
	//
	//  constructors
	//
	//////////////////////////////////////////////////
	FGMS ();
	~FGMS ();

	//////////////////////////////////////////////////
	//
	//  methods
	//
	//////////////////////////////////////////////////
	int  Init ();
	int  Loop ();
	void Done ();
	void OpenLogfile ();
	void WantExit ();
	int  ParseParams ( int argc, char* argv[] );
private:
	enum eSTATE
	{
		REGISTER,
		RUNNING
	};
	typedef fgms::list<Pilot>::iterator  pilot_iterator;
	typedef fgms::list<Relay>::iterator  relay_iterator;
	typedef fgms::list<Server>::iterator server_iterator;
	eSTATE		m_State;  
	bool		m_IamHUB;
	bool		m_WantExit;
	bool		m_RequireAuth;
	bool		m_RegisterFGLS;	// shall we register at fgls?
	int		m_ResendTime;
	int		m_OutOfReach;	// FIXME: it is a per client option
	uint64_t	m_NumClients; 
	ofstream	m_LogFile;
	uint32_t	m_CheckInterval;
	time_t		fgls_lastseen;
	Server		m_FGAS;
	Server		m_FGLS;
	fgms::list<Relay>	RelayList;
	fgms::list<Pilot>	PilotList;
	void	Register ();
	void	RegisterFGFS ( const NetAddr& Sender );
	void	HandleAuthOK ();
	void	HandleAuthFailed ();
	void	HandleRequest ();
	void	HandlePilot ( t_ReceivePacket* Packet );
	void	HandlePong ( const NetAddr& Sender, t_ReceivePacket* Packet);
	void	AddRelay ( const NetAddr& Sender );
	void	RemoveRelay ();
	pilot_iterator	RemoveFGFS ( pilot_iterator It );
	void	RemoveFGFS ( uint64_t ID );
	void	Maintainance ();
	void	PrintHelp ();
	void	SendError ( const NetAddr & Sender, const uint32_t Cmd, const string & Msg );
	void	HandleCommand ( int Command, const NetAddr& Sender, t_ReceivePacket* Packet );
	relay_iterator FindRelayByID ( const uint64_t & ID );
	pilot_iterator FindPilot ( const Pilot & Pilot );
	pilot_iterator FindPilotByID ( const uint64_t & ID );
	pilot_iterator FindPilotByName ( const std::string & Name );
}; // FGMS

#endif


