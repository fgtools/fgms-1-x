///
/// @file fgls.cxx
/// flightgear list server
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2008-2015
/// @copyright	GPLv3
///
/// This is the list server for the flightgear multiplayer
/// network. All servers register at this server. All clients ask fgls
/// which server they should use.
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#define FGLS_VERSION "0.0.1-pre-alpha1"

#include <fglib/fg_list.hxx>
#include <fglib/fg_typcnvt.hxx>
#include <flightgear/Network/fg_proto.hxx>
#ifdef _MSC_VER
#include <getopt/getopt.h>
#endif
using namespace std;

// #define ENABLE_ENCRYPTION

//////////////////////////////////////////////////////////////////////

class Server
{
public:
	fgms::eCLIENTTYPES	Type;
	uint64_t		ID;
	NetAddr			Addr;
	string			Name;
	string			Location;
	time_t			LastSeen;	// timestamp of last messages received/sent
	time_t			RegisteredAt;	// timestamp of registration

	Server ();
	Server ( const std::string & Name );
	Server ( const Server &  S );
	void operator =  ( const Server & S );
	bool operator ==  ( const Server& S );
	bool operator !=  ( const Server& S );
	friend std::ostream& operator << ( std::ostream& o, const Server& S);
protected:
	void assign ( const Server & S );
}; // class Server

//////////////////////////////////////////////////////////////////////

class fgls : public ProtocolHandler
{
public:
	fgls ();
	int  Init ();
	void Loop ();
	int  ParseParams ( int argc, char* argv[] );
private:
	typedef fgms::list<Server>::iterator server_iterator;
	void SendFailed ( const NetAddr & Sender, fgms::Commands Cmd,
	  const string & What );
	void RegisterFGAS ( const NetAddr& Sender, Server& NewServer );
	void RegisterFGMS ( const NetAddr& Sender, Server& NewServer );
	void RegisterServer ( const NetAddr& Sender );
	void RegisterFGFS ( const NetAddr& Sender );
	void Maintainance ();
	void HandlePing ( const NetAddr& Sender, t_ReceivePacket* Packet );
	void HandlePong ( const NetAddr& Sender, t_ReceivePacket* Packet );
	void HandleCommand ( int Command, const NetAddr& Sender,
	  t_ReceivePacket* Packet );
	void HandleRequest ();
	void PrintHelp ();
	server_iterator RemoveFGMS ( server_iterator It );
	void RemoveFGMS ( uint64_t ID );
	server_iterator RemoveFGAS ( server_iterator It );
	void RemoveFGAS ( uint64_t ID );
	void RemoveServer ( int Type, uint64_t ID );
	void SetHubProps ( const Server& HUB );
	void SendWhoIsHUB ( const NetAddr& Receiver );
	void SendListOfServers ( const NetAddr& Receiver,
	  fgms::eCLIENTTYPES Type );
	void SelectNextFGMS ( fgms::list<Server>::iterator It );
	void PropagateFGAS ();
	server_iterator Find ( fgms::list<Server> & List, const Server & S );
	server_iterator FindByID ( fgms::list<Server> & List, const size_t ID );
	server_iterator FindByAddr ( fgms::list<Server> & List, const NetAddr & Addr );
	uint32_t m_CheckInterval;
	uint64_t m_NumClients;
	fgms::list<Server> ServerList;
	fgms::list<Server> FGASList;
	server_iterator	m_CurrentHUB;
	server_iterator	m_CurrentFGMS;
	server_iterator	m_CurrentFGAS;
}; // class fgls

//////////////////////////////////////////////////////////////////////

Server::Server
()
{
	ID	= ( size_t ) -1;
	Name	= "";
	LastSeen = time ( 0 );
	RegisteredAt = LastSeen;
	Addr.Assign ( 0, 0 );
} // Server::Server ()

//////////////////////////////////////////////////////////////////////

Server::Server
(
	const std::string & Name
)
{
	ID		= ( size_t ) -1;
	this->Name	= Name.c_str ();
	LastSeen = time ( 0 );
	RegisteredAt = LastSeen;
	Addr.Assign ( 0, 0 );
} // Server::Server ( string & )

//////////////////////////////////////////////////////////////////////

Server::Server
(
	const Server & S
)
{
	this->assign ( S );
} // Server::Server ( Server & )

//////////////////////////////////////////////////////////////////////

void
Server::operator =
(
	const Server & S
)
{
	this->assign ( S );
} // Server::operator =

//////////////////////////////////////////////////////////////////////

bool
Server::operator ==
(
	const Server& S
)
{
	if ( ( Name == S.Name ) && ( Addr == S.Addr ) )
		return ( true );
	return ( false );
} // Server::operator ==

//////////////////////////////////////////////////////////////////////

bool
Server::operator !=
(
	const Server& S
)
{
	if ( ( Name == S.Name ) && ( Addr == S.Addr ) )
		return ( false );
	return ( true );
} // Server::operator !=

//////////////////////////////////////////////////////////////////////

void
Server::assign
(
	const Server & S
)
{
	ID		= S.ID;
	Type		= S.Type;
	Addr		= S.Addr;
	Name		= S.Name.c_str ();
	Location	= S.Location.c_str ();
	LastSeen	= S.LastSeen;
	RegisteredAt	= S.RegisteredAt;
} // Server::assign ( Server )

//////////////////////////////////////////////////////////////////////

std::ostream&
operator <<
(
	std::ostream& o,
	const Server& S
)
{
	o << S.Name << " " << S.Location
	  << " " << S.Addr
	  << " (ID " << S.ID << ")";
	return ( o );
}

//////////////////////////////////////////////////////////////////////

fgls::fgls
()
{
	m_CheckInterval = 10;
	m_NumClients	= 1;
	m_CurrentFGAS.invalidate ();
	m_CurrentFGMS.invalidate ();
	m_CurrentHUB.invalidate  ();
	m_Properties.setIntValue ( "/fgms/log_level", SG_WARN );
	sglog().setLogLevels ( SG_ALL, SG_WARN );
}

//////////////////////////////////////////////////////////////////////

int
fgls::Init
()
{
	string Addr;
	int    Port;

	Addr = m_Properties.getStringValue ( "/fgls/addr" );
	Port = m_Properties.getIntValue    ( "/fgls/port" );
	if (! m_Peer.Listen ( Addr, Port ) )
	{
		return (fgms::FAILED);
	}
	m_CheckInterval = m_Properties.getIntValue (
	  "/multiplayer/system/checkinterval" );
	if ( m_CheckInterval == 0 )
		m_CheckInterval = 10;
	m_Peer.SetClientType (fgms::FGLS);
	m_Peer.SetSenderID ( 1 ); // FIXME: make this configurable
	SG_LOG ( SG_NETWORK, SG_ALERT,
	  "### listening to port " << Port );
	sglog().enable_with_date ( true );
	return (fgms::SUCCESS);
} // fgls::Init ()

//////////////////////////////////////////////////////////////////////

void
fgls::PrintHelp
()
{
	cerr << endl;
	cerr << "syntax: fgls [options] configname" << endl;
	cerr << "\n"
		"options are:\n"
		"-h        print this help screen\n"
		"-p PORT   listen to PORT\n"
		"-v LEVEL  verbosity (loglevel) in range 1 (much) and 4 (few)\n"
		"          default is 3, 0 prints *allot*\n"
		"\n"
		"commandline parameters always override config file options\n"
		"\n";
} // fgls::PrintHelp ()

//////////////////////////////////////////////////////////////////////

int
fgls::ParseParams
(
	int   argc,
	char* argv[]
)
{
	int m;
	int e;

	//
	// parse the commandline twice. First run to get the name
	// of the config file
	//
	while ( ( m=getopt ( argc, argv, "hp:v:" ) ) != -1 )
	{
		switch ( m )
		{
		case 'h':
			PrintHelp ();
			return ( fgms::FAILED );
		case 'p':
		case 'v':
			break;
		default:
			cerr << endl;
			PrintHelp ();
			return ( fgms::FAILED );
		}
	}
	if ( optind >= argc )
	{
		PrintHelp ();
		cerr << "Expected configfilename." << endl;
		cerr << endl;
		return ( fgms::FAILED );
	}
	if ( optind < argc-1 )
	{
		PrintHelp ();
		cerr << "Too much parameters, only expected configfilename."
		     << endl;
		cerr << endl;
		return ( fgms::FAILED );
	}
	m_Properties.setStringValue ( "/fgms/config_name", argv[optind] );
	if ( ReadConfig ( argv[optind], m_Properties ) == fgms::FAILED )
	{
		return ( fgms::FAILED );
	}
	//
	// Parse the commandline a second time, so that the
	// options overwrite the config file values
	//
	optind = 1; // reset index
	int v;
	while ( ( m=getopt ( argc, argv, "p:v:" ) ) != -1 )
	{
		switch ( m )
		{
		case 'p':
			v = StrToNum<int>  ( optarg, e );
			if ( e )
			{
				cerr << "invalid value for listen port: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgls/port", v );
			break;
		case 'v':
			v = StrToNum<int>  ( optarg, e );
			if ( e )
			{
				cerr << "invalid value for Loglevel: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgms/log_level", v );
			sglog().setLogLevels ( SG_ALL, ( sgDebugPriority ) v);
			break;
		}
	}
	if ( optind >= argc )
	{
		cerr << endl;
		cerr << "syntax: " << argv[0] << " [options] configname"
		     << endl;
		cerr << "Expected configfilename." << endl;
		return ( fgms::FAILED );
	}
	if ( optind < argc-1 )
	{
		cerr << endl;
		cerr << "syntax: " << argv[0] << " [options] configname"
		     << endl;
		cerr << "Too much parameters, only expected configfilename."
		     << endl;
		return ( fgms::FAILED );
	}

	return ( fgms::SUCCESS );
} // fgls::ParseParams ()

//////////////////////////////////////////////////////////////////////

void
fgls::SendFailed
(
	const NetAddr & Sender,
	fgms::Commands Cmd,
	const string & Msg
)
{
	fgms::t_PropertyList    SendProps;

	SendProps.push_back ( Cmd );
	SendProps.push_back ( m_Prop2ID["/multiplayer/system/errormessage"] );
	m_Properties.setStringValue (
	  "/multiplayer/system/errormessage", Msg.c_str() );
	SG_LOG ( SG_NETWORK, SG_DEBUG, "sending " << Msg << "..." );
	m_Peer.SetTarget (Sender);
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // fgls::SendFailed ()

//////////////////////////////////////////////////////////////////////

fgls::server_iterator
fgls::Find
(
	fgms::list<Server> & List,
	const Server& S
)
{
	server_iterator i;

	for ( i = List.begin(); i != List.end(); i++ )
	{
		if ( *i == S )
		{
			return i;
		}
	}
	return i;
} // fgls::Find ()

//////////////////////////////////////////////////////////////////////

fgls::server_iterator
fgls::FindByID
(
	fgms::list<Server> & List,
	const size_t ID
)
{
	server_iterator i;

	for ( i = List.begin(); i != List.end(); i++ )
	{
		if ( i->ID == ID )
		{
			return i;
		}
	}
	return i;
} // fgls::FindByID ()

//////////////////////////////////////////////////////////////////////

fgls::server_iterator
fgls::FindByAddr
(
	fgms::list<Server> & List,
	const NetAddr& Addr
)
{
	server_iterator i;

	for ( i = List.begin(); i != List.end(); i++ )
	{
		if ( i->Addr == Addr )
		{
			return i;
		}
	}
	return i;
} // fgls::FindByAddr ()

//////////////////////////////////////////////////////////////////////
// Send information about HUB to Receiver
//////////////////////////////////////////////////////////////////////
void
fgls::SendWhoIsHUB
(
	const NetAddr& Receiver
)
{
	fgms::t_PropertyList SendProps;

	SendProps.push_back ( fgms::YOU_ARE_LEAVE );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/name"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/location"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/address"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/port"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/client_id"] );
	m_Properties.setStringValue ( "/multiplayer/fgms/name",
	  m_Properties.getStringValue ( "/multiplayer/hub/name" ) );
	m_Properties.setStringValue ( "/multiplayer/fgms/location",
	  m_Properties.getStringValue ( "/multiplayer/hub/location" ) );
	m_Properties.setStringValue ( "/multiplayer/fgms/address",
	  m_Properties.getStringValue ( "/multiplayer/hub/address" ) );
	m_Properties.setIntValue    ( "/multiplayer/fgms/port",
	  m_Properties.getIntValue    ( "/multiplayer/hub/port" ) );
	m_Properties.setStringValue ( "/multiplayer/fgms/client_id",
	  m_Properties.getStringValue ( "/multiplayer/hub/client_id" ) );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending HUB information to "
	 << Receiver.ToString() << ":" << Receiver.Port() );
	m_Peer.SetTarget ( Receiver );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // fgls::SendWhoIsHUB ()

//////////////////////////////////////////////////////////////////////
// Write the list of servers to Receiver.
// We send every server in a single packet. More efficient would be
// to send all servers in one packet, but therefor the fg_proto must
// be extended to write and read a subtree into the property tree.
// Since the list of servers is seldom sent it's not worth the effort
// and nobody cares anyway.
//////////////////////////////////////////////////////////////////////
void
fgls::SendListOfServers
(
	const NetAddr& Receiver,
	fgms::eCLIENTTYPES Type
)
{
	server_iterator It;
	fgms::t_PropertyList SendProps;

	SG_LOG ( SG_NETWORK, SG_INFO, "sending list of servers to "
	 << Receiver );
	SendProps.push_back ( fgms::NEW_SERVER );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/name"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/location"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/address"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/port"] );
	if ( Type == fgms::FGMS )
	{
		SendProps.push_back (
		  m_Prop2ID["/multiplayer/fgms/client_id"] );
	}
	m_Peer.SetTarget ( Receiver );
	for ( It = ServerList.begin(); It != ServerList.end(); It++ )
	{
		if ( It->Addr == Receiver )
		{
			continue;
		}
		m_Properties.setStringValue (
		  "/multiplayer/fgms/name", It->Name.c_str ());
		m_Properties.setStringValue (
		  "/multiplayer/fgms/location", It->Location.c_str() );
		m_Properties.setStringValue (
		  "/multiplayer/fgms/address",
		  It->Addr.ToString().c_str () );
		m_Properties.setIntValue    (
		  "/multiplayer/fgms/port", It->Addr.Port() );
		if ( Type == fgms::FGMS )
		{
			string s = NumToStr<uint64_t> ( It->ID, 0 );
			m_Properties.setStringValue (
			  "/multiplayer/fgms/client_id", s.c_str () );
		}
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
	}
} // fgls::SendListOfServers ()

//////////////////////////////////////////////////////////////////////

void
fgls::SelectNextFGMS
(
	server_iterator It
)
{
	if ( ServerList.size() == 0 )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "no more servers left!" );
		m_CurrentFGMS.invalidate ();
		m_CurrentFGAS.invalidate ();
	}
	It++;
	if ( It == ServerList.end () )
	{
		It = ServerList.begin ();
	}
	if ( ( ServerList.size() > 3 ) && ( m_CurrentHUB->Addr == It->Addr ) )
	{
		It++;
	}
	m_CurrentFGMS = It;
	SG_LOG ( SG_NETWORK, SG_ALERT, "Next FGMS is " << *m_CurrentFGMS ); 
} // fgls::SelectNextFGMS ()

//////////////////////////////////////////////////////////////////////

void
fgls::RemoveFGAS
(
	uint64_t ID
)
{
	server_iterator It;

	for ( It = FGASList.begin (); It != FGASList.end(); It++ )
	{
		if ( It->ID == ID )
			break;
	}
	if ( It == FGASList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "fgls::RemoveFGAS: "
		  << "I should remove server ID " << ID
		  << " but have it not in my list!"
		);
		return;
	}
	RemoveFGAS ( It );
} // fgls::RemoveFGAS ( ID )

//////////////////////////////////////////////////////////////////////

fgls::server_iterator
fgls::RemoveFGAS
(
	fgls::server_iterator It
)
{
	server_iterator NextFGAS;
	fgms::t_PropertyList SendProps;
	NetAddr Addr;
	time_t  now = time (0);

	SG_LOG ( SG_NETWORK, SG_WARN, *It
	  << " is leaving the network. Online for "
	  << (now - It->RegisteredAt) << " seconds.");
	Addr = It->Addr;
	NextFGAS = FGASList.erase ( It );
	cout << "removed : " << Addr << " current: " << m_CurrentFGAS->Addr << endl;
	if ( m_CurrentFGAS->Addr == Addr )
	{
		SG_LOG ( SG_NETWORK, SG_WARN,
		  "fgls::RemoveFGAS: quitting server is FGAS!" );
		It = FGASList.begin ();
		if ( It != FGASList.end () )
		{
			m_CurrentFGAS = It;
			SG_LOG ( SG_NETWORK, SG_WARN, "Now using "
			  << *m_CurrentFGAS << " as FGAS" );
			PropagateFGAS ();
			return ( It );
		}
		// tell all servers that we have no FGAS any more
		SG_LOG ( SG_NETWORK, SG_ALERT, "No FGAS available!" );
		m_CurrentFGAS.invalidate ();
		SendProps.push_back ( fgms::NO_FGAS );
		for ( It = ServerList.begin(); It != ServerList.end(); It++ )
		{
			m_Peer.SetTarget ( It->Addr );
			WriteProps (m_Peer, m_Properties, SendProps );
			m_Peer.Send ();
		}
	}
	return ( NextFGAS );
} // fgls::RemoveFGAS ( It )

//////////////////////////////////////////////////////////////////////

void
fgls::RemoveFGMS
(
	uint64_t ID
)
{
	server_iterator It;

	It = FindByID ( ServerList, ID );
	if ( It == ServerList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "fgls::RemoveFGMS: "
		  << "I should remove server ID " << ID
		  << " but have it not in my list!"
		);
		return;
	}
	RemoveFGMS ( It );
} // fgls::RemoveFGMS ( ID )

//////////////////////////////////////////////////////////////////////

fgls::server_iterator
fgls::RemoveFGMS
(
	fgls::server_iterator It
)
{
	fgms::t_PropertyList SendProps;
	NetAddr  CurrentFGMS;
	NetAddr	 CurrentHUB;
	NetAddr  LeavingAddr;
	uint32_t LeavingID;
	time_t   now = time (0);
	server_iterator NextServer;

	SG_LOG ( SG_NETWORK, SG_WARN, *It
	  << " is leaving the network. Online for "
	  << (now - It->RegisteredAt) << " seconds.");
	LeavingID   = It->ID;
	LeavingAddr = It->Addr;
	CurrentFGMS = m_CurrentFGMS->Addr;
	CurrentHUB  = m_CurrentHUB->Addr;
	NextServer  = ServerList.erase ( It );
	if ( ServerList.size() == 0 )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "fgls::RemoveFGMS no more servers!!" );
		m_CurrentFGMS.invalidate ();
		m_CurrentHUB.invalidate  ();
		return ( ServerList.end() );
	}
	//////////////////////////////////////////////////
	// If the leaving server is the current FGMS,
	// select a new (next server in list)
	//////////////////////////////////////////////////
	if ( CurrentFGMS == LeavingAddr )
	{
		SelectNextFGMS ( NextServer );
	}
	//////////////////////////////////////////////////
	// If the leaving server is the current HUB,
	// select a new HUB (next server in list) and
	// inform all others about it
	//////////////////////////////////////////////////
	if ( CurrentHUB == LeavingAddr )
	{
		SG_LOG ( SG_NETWORK, SG_INFO, "Quitting server is HUB!" );
		// the first server is online for the longest time
		// so presumably the most stable
		m_CurrentHUB = ServerList.begin (); // the first entry in list
		SetHubProps ( *m_CurrentHUB );
		CurrentHUB = m_CurrentHUB->Addr;
		SendProps.push_back ( fgms::YOU_ARE_HUB );
		m_Peer.SetTarget ( m_CurrentHUB->Addr );
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
		// send list of relays to the new HUB
		SendListOfServers ( m_CurrentHUB->Addr, fgms::FGMS );
		// inform all servers about the new HUB
		for ( It = ServerList.begin (); It != ServerList.end(); It++ )
		{
			if ( m_CurrentHUB->Addr == It->Addr )
			{
				continue;
			}
			SendWhoIsHUB ( It->Addr );
		}
		return ( m_CurrentHUB );
	}
	//////////////////////////////////////////////////
	// tell HUB that a server has left
	//////////////////////////////////////////////////
	SG_LOG ( SG_NETWORK, SG_INFO,
	  "fgls::RemoveFGMS: quitting server is a LEAVE!" );
	string s = NumToStr<uint64_t> ( LeavingID, 0 );
	m_Properties.setStringValue (
	  "/multiplayer/fgms/client_id", s.c_str () );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/client_id"] );
	SendProps.push_back ( fgms::SERVER_QUIT );
	m_Peer.SetTarget ( m_CurrentHUB->Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	return ( NextServer );
} // fgls::RemoveFGMS ( It )

//////////////////////////////////////////////////////////////////////

void
fgls::RemoveServer
(
	int      Type,
	uint64_t ID
)
{
	if ( Type == fgms::FGMS )
	{
		RemoveFGMS ( ID );
	}
	else if ( Type == fgms::FGAS )
	{
		RemoveFGAS ( ID );
	}
	else
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "fgls::RemoveServer: "
		  << "can not remove client type " << Type
		);
	}
}  // fgls::RemoveServer ()

//////////////////////////////////////////////////////////////////////

void
fgls::Maintainance
()
{
	server_iterator It;
	fgms::t_PropertyList SendProps;
	time_t now = time ( 0 );
	time_t timeout = m_CheckInterval * 3; // miss 3 pings
	time_t diff;

	SendProps.push_back ( fgms::PING );
	//////////////////////////////////////////////////
	// all FGMS
	//////////////////////////////////////////////////
	for ( It = ServerList.begin (); It != ServerList.end (); It++ )
	{
		SG_LOG ( SG_NETWORK, SG_DEBUG, "checking server: " << *It );
		diff = now - It->LastSeen;
		SG_LOG ( SG_NETWORK, SG_DEBUG, "last seen "
		  << diff << " seconds ago" );
		if ( diff > timeout )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, *It
			  << " last seen " << diff << " seconds ago!" );
			It = RemoveFGMS ( It );
			continue;
		}
		m_Peer.SetTarget ( It->Addr );
		WriteProps (m_Peer, m_Properties, SendProps );
		SG_LOG ( SG_NETWORK, SG_INFO, "sending PING to " << *It );
		m_Peer.Send ();
	}
	//////////////////////////////////////////////////
	// all FGAS
	//////////////////////////////////////////////////
	for ( It = FGASList.begin (); It != FGASList.end (); It++ )
	{
		SG_LOG ( SG_NETWORK, SG_DEBUG, "checking FGAS: " << *It );
		diff = now - It->LastSeen;
		SG_LOG ( SG_NETWORK, SG_DEBUG, "last seen "
		  << diff << " seconds ago" );
		if ( diff > timeout )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, *It
			  << " last seen " << diff
			  << " seconds ago!" );
			It = RemoveFGAS ( It );
			continue;
		}
		m_Peer.SetTarget ( It->Addr );
		WriteProps (m_Peer, m_Properties, SendProps );
		SG_LOG ( SG_NETWORK, SG_INFO, "sending PING to " << *It );
		m_Peer.Send ();
	}
} // fgls::Maintainance

//////////////////////////////////////////////////////////////////////

void
fgls::SetHubProps
(
	const Server& HUB
)
{
	string s = NumToStr<uint64_t> ( HUB.ID, 0 );
	m_Properties.setStringValue ( "/multiplayer/hub/name",
	  HUB.Name.c_str ());
	m_Properties.setStringValue ( "/multiplayer/hub/location",
	  HUB.Location.c_str() );
	m_Properties.setStringValue ( "/multiplayer/hub/address",
	  HUB.Addr.ToString().c_str () );
	m_Properties.setIntValue    ( "/multiplayer/hub/port",
	  HUB.Addr.Port() );
	m_Properties.setStringValue ( "/multiplayer/hub/client_id",
	  s.c_str () );
} // fgls::SetHubProps ()

//////////////////////////////////////////////////////////////////////

void
fgls::RegisterFGMS
(
	const NetAddr& Sender,
	Server& NewServer
)
{
	uint64_t SenderID;
	bool UpdateHUB = false;
	fgms::t_PropertyList SendProps;
	server_iterator It;

	//////////////////////////////////////////////////
	// check if we already know the new server
	// and add it to the list
	//////////////////////////////////////////////////
	It = Find ( ServerList, NewServer );
	if ( It == ServerList.end () )
	{	// add the new server to the list
		m_NumClients++;
		SenderID = m_NumClients;
		NewServer.ID = m_NumClients;
		ServerList.push_back ( NewServer );
		SG_LOG ( SG_NETWORK, SG_ALERT, "New FGMS " << NewServer );
	}
	else
	{
		SenderID = It->ID;
		SG_LOG ( SG_NETWORK, SG_WARN, "Know FGMS already: "
		  << NewServer.Name );
		if ( m_CurrentFGMS->Addr == It->Addr )
		{
			m_CurrentFGMS.invalidate ();
		}
		if ( m_CurrentHUB->Addr == It->Addr )
		{
			m_CurrentHUB.invalidate ();
		}
	}
	//////////////////////////////////////////////////
	// either make it HUB or LEAVE
	//////////////////////////////////////////////////
	if ( m_CurrentFGMS.is_valid () == false )
	{
		SG_LOG ( SG_NETWORK, SG_INFO, "using FGMS "
		  << NewServer.Name << " as HUB" );
		It = FindByID ( ServerList, SenderID );
		m_CurrentFGMS = It;
		m_CurrentHUB  = It;
		// It's the first FGMS Server, so make it the HUB
		SendProps.push_back ( fgms::YOU_ARE_HUB );
		SetHubProps ( NewServer );
	}
	else
	{
		UpdateHUB = true;
	}
	//////////////////////////////////////////////////
	// tell registering server about the
	// successfull registration
	//////////////////////////////////////////////////
	SendProps.push_back ( fgms::REGISTER_OK );
	SendProps.push_back ( fgms::SET_CLIENT_ID );
	SendProps.push_back (
	  m_Prop2ID["/multiplayer/registration/client_id"] );
	m_Properties.setStringValue ( "/multiplayer/registration/client_id", 
	  NumToStr<uint64_t> ( SenderID, 0 ).c_str() );
	if ( m_CurrentFGAS.is_valid () )
	{	// we have a FGAS, so tell new fgms to use it
		m_Properties.setStringValue ( "/multiplayer/fgas/address",
		  m_CurrentFGAS->Addr.ToString().c_str() );
		m_Properties.setIntValue    ( "/multiplayer/fgas/port",
		  m_CurrentFGAS->Addr.Port() );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgas/address"] );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgas/port"] );
		SendProps.push_back ( fgms::USE_FGAS );
	}
	else
	{
		SendProps.push_back ( fgms::NO_FGAS );
	}
	SG_LOG ( SG_NETWORK, SG_INFO, "sending REGISTISTER_OK" );
	m_Peer.SetTarget ( Sender );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	//////////////////////////////////////////////////
	// we have a new leave server, so tell the HUB
	// about it
	//////////////////////////////////////////////////
	if ( UpdateHUB )
	{
		// tell registering server who is HUB
		SendWhoIsHUB ( Sender );
		// send the ip and port to the HUB
		string s = NumToStr<uint64_t> ( SenderID, 0 );
		SendProps.clear ();
		SendProps.push_back ( fgms::NEW_SERVER );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/name"] );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/location"] );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/address"] );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/port"] );
		SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/client_id"]);
		m_Properties.setStringValue ( "/multiplayer/fgms/name",
		  NewServer.Name.c_str ());
		m_Properties.setStringValue ( "/multiplayer/fgms/location",
		  NewServer.Location.c_str() );
		m_Properties.setStringValue ( "/multiplayer/fgms/address",
		  NewServer.Addr.ToString().c_str () );
		m_Properties.setIntValue    ( "/multiplayer/fgms/port",
		  NewServer.Addr.Port() );
		m_Properties.setStringValue ( "/multiplayer/fgms/client_id",
		  s.c_str () );
		SG_LOG ( SG_NETWORK, SG_INFO, "Telling HUB about "
		  << NewServer.Name );
		m_Peer.SetTarget ( m_CurrentHUB->Addr );
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
	}
	// only for debugging, remove later
	SG_LOG ( SG_NETWORK, SG_INFO, "My Server list is:");
	string s;
	for ( It = ServerList.begin(); It != ServerList.end (); It++ )
	{
		s = "";
		if ( m_CurrentHUB->Addr  == It->Addr )
			s += " is HUB";
		else
			s += " is LEAVE";
		if ( m_CurrentFGMS->Addr == It->Addr )
			s += " is Current";
		SG_LOG ( SG_NETWORK, SG_INFO, *It << s );
	}
} // fgls::RegisterFGMS ()

//////////////////////////////////////////////////////////////////////

void
fgls::PropagateFGAS
()
{
	fgms::t_PropertyList SendProps;
	server_iterator It;

	SendProps.clear ();
	m_Properties.setStringValue ( "/multiplayer/fgas/address",
	  m_CurrentFGAS->Addr.ToString().c_str() );
	m_Properties.setIntValue    ( "/multiplayer/fgas/port",
	  m_CurrentFGAS->Addr.Port() );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgas/address"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgas/port"] );
	SendProps.push_back ( fgms::USE_FGAS );
	SG_LOG ( SG_NETWORK, SG_INFO, "Propagating who is FGAS" );
	for ( It = ServerList.begin(); It != ServerList.end(); It++ )
	{
		m_Peer.SetTarget (It->Addr);
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
	}
} // fgls::PropagateFGAS ()

//////////////////////////////////////////////////////////////////////

void
fgls::RegisterFGAS
(
	const NetAddr& Sender,
	Server& NewServer
)
{
	uint64_t SenderID;
	fgms::t_PropertyList SendProps;
	server_iterator It;
	//////////////////////////////////////////////////
	// check if we already know the new server
	// and add it to the list
	//////////////////////////////////////////////////
	It = Find ( FGASList, NewServer );
	if ( It == FGASList.end () )
	{	// add the new server to the list
		m_NumClients++;
		SenderID = m_NumClients;
		NewServer.ID = SenderID;
		FGASList.push_back ( NewServer );
		SG_LOG ( SG_NETWORK, SG_WARN, "New FGAS " << NewServer );
	}
	else
	{
		SenderID = It->ID;
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "Know FGAS already: " << NewServer.Name );
	}
	//////////////////////////////////////////////////
	// tell registering server about the
	// successfull registration
	//////////////////////////////////////////////////
	SendProps.push_back ( fgms::REGISTER_OK );
	SendProps.push_back ( fgms::SET_CLIENT_ID );
	SendProps.push_back (
	  m_Prop2ID["/multiplayer/registration/client_id"] );
	m_Properties.setStringValue ( "/multiplayer/registration/client_id", 
	  NumToStr<uint64_t> ( SenderID, 0 ).c_str() );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending REGISTER OK to "
	  << NewServer.Name );
	m_Peer.SetTarget    ( Sender );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	//////////////////////////////////////////////////
	// If it's our first FGAS tell every server 
	// to use it
	//////////////////////////////////////////////////
	if ( m_CurrentFGAS.is_valid () == false )
	{
		SG_LOG ( SG_NETWORK, SG_WARN, "using FGAS "
		  << NewServer.Name );
		It = FindByID ( FGASList, SenderID );
		m_CurrentFGAS = It;
		PropagateFGAS ();
	}
	// only for debugging, remove later
	SG_LOG ( SG_NETWORK, SG_INFO, "My FGAS list is:");
	string s;
	for ( It = FGASList.begin(); It != FGASList.end(); It++ )
	{
		s = "";
		if ( m_CurrentFGAS->Addr  == It->Addr )	s = " is PRIME";
		SG_LOG ( SG_NETWORK, SG_INFO, *It << s );
	}
} // fgls::RegisterFGAS ()

//////////////////////////////////////////////////////////////////////

void
fgls::RegisterServer
(
	const NetAddr& Sender
)
{
	Server	NewServer;

	NewServer.Type     = (fgms::eCLIENTTYPES) m_Properties.getIntValue (
	  "/multiplayer/registration/clienttype" );
	NewServer.Name     = m_Properties.getStringValue (
	  "/multiplayer/registration/name" );
	NewServer.Location = m_Properties.getStringValue (
	  "/multiplayer/registration/location" );
	uint32_t Port      = m_Properties.getIntValue    (
	  "/multiplayer/registration/port" );
	//////////////////////////////////////////////////
	// check properties
	//////////////////////////////////////////////////
	if ( ( NewServer.Type != fgms::FGMS )
	&&   ( NewServer.Type != fgms::FGAS ) )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "invalid server type: " << NewServer );
		SendFailed (Sender, fgms::GENERAL_ERROR, "invalid server type");
		return;
	}
	if (NewServer.Name == "")
	{
		SendFailed (Sender, fgms::GENERAL_ERROR, "invalid name");
		return;
	}
	if (NewServer.Location == "")
	{
		SendFailed (Sender, fgms::GENERAL_ERROR, "invalid location");
		return;
	}
	if (Port == 0)
	{
		SendFailed (Sender, fgms::GENERAL_ERROR, "invalid port");
		return;
	}
	NewServer.Addr.Assign ( Sender.ToString(), Port );
	NewServer.LastSeen = time ( 0 );
	NewServer.RegisteredAt = NewServer.LastSeen;
	if ( NewServer.Type == fgms::FGMS )
	{
		RegisterFGMS ( Sender, NewServer );
	}
	else if ( NewServer.Type == fgms::FGAS )
	{
		RegisterFGAS ( Sender, NewServer );
	}
	// reset registration properties
	m_Properties.setIntValue    (
	  "/multiplayer/registration/clienttype", fgms::NOCLIENT);
	m_Properties.setStringValue ( "/multiplayer/registration/name", "");
	m_Properties.setStringValue ( "/multiplayer/registration/location", "");
	m_Properties.setStringValue ( "/multiplayer/registration/address", "");
	m_Properties.setIntValue    ( "/multiplayer/registration/port", 0);
	SG_LOG ( SG_NETWORK, SG_DEBUG, "registration done " );
} // fgls::RegisterServer ()

//////////////////////////////////////////////////////////////////////

void
fgls::RegisterFGFS
(
	const NetAddr& Sender
)
{
	fgms::t_PropertyList SendProps;
	server_iterator It;

	if ( m_CurrentFGMS.is_valid () == false )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  Sender << " requested Server, but I have none!" );
		SendFailed (Sender, fgms::GENERAL_ERROR,
		  "no servers available");
		return;
	}
	It = FindByAddr ( ServerList, m_CurrentFGMS->Addr );
	m_Properties.setStringValue ( "/multiplayer/fgms/address",
	  It->Addr.ToString().c_str() );
	m_Properties.setIntValue    ( "/multiplayer/fgms/port", It->Addr.Port() );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/address"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/fgms/port"] );
	SendProps.push_back ( fgms::USE_SERVER );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending USE_SERVER "
	  << *It << " to " << Sender );
	m_Peer.SetTarget ( Sender );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	SG_LOG ( SG_NETWORK, SG_INFO,
	  Sender << " requested Server, gave " << *It );
	SelectNextFGMS ( It );
} // fgls::RegisterFGFS ()

//////////////////////////////////////////////////////////////////////

void
fgls::HandlePing
(
	const NetAddr& Sender,
	t_ReceivePacket* Packet
)
{
	server_iterator It;
	fgms::t_PropertyList SendProps;

	if ( Packet->ClientType == fgms::FGMS )
	{
		It = FindByID ( ServerList, Packet->SenderID );
		if ( It == ServerList.end () )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "got PING from unknown FGMS ID "
			  << Packet->SenderID );
			SendProps.push_back ( fgms::REGISTER_AGAIN );
			m_Peer.SetTarget ( Sender );
			WriteProps (m_Peer, m_Properties, SendProps );
			m_Peer.Send ();
			return;
		}
	}
	if ( Packet->ClientType == fgms::FGAS )
	{
		It = FindByID ( FGASList, Packet->SenderID );
		if ( It == FGASList.end () )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "got PING from unknown FGAS ID "
			  << Packet->SenderID );
			SendProps.push_back ( fgms::REGISTER_AGAIN );
			m_Peer.SetTarget ( Sender );
			WriteProps (m_Peer, m_Properties, SendProps );
			m_Peer.Send ();
			return;
		}
	}
	It->LastSeen = time ( 0 );
	SG_LOG ( SG_NETWORK, SG_INFO, "got PING from "
	  << It->Name << " (ID " << It->ID << ")" );
	return;
} // fgms::HandlePing ()

//////////////////////////////////////////////////////////////////////

void
fgls::HandlePong
(
	const NetAddr& Sender,
	t_ReceivePacket* Packet
)
{
	fgms::t_PropertyList SendProps;
	time_t now = time( 0 );
	time_t ts  = m_Properties.getIntValue (
	  "/multiplayer/system/pong_timestamp" );
	time_t diff = now - ts;
	server_iterator It;

	if ( Packet->ClientType == fgms::FGMS )
	{
		It = FindByID ( ServerList, Packet->SenderID );
		if ( It == ServerList.end () )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "got PONG from unknown FGMS ID "
			  << Packet->SenderID );
			return;
		}
		SG_LOG ( SG_NETWORK, SG_INFO, "got PONG from FGMS " << *It );
	}
	if ( Packet->ClientType == fgms::FGAS )
	{
		It = FindByID ( FGASList, Packet->SenderID );
		if ( It == FGASList.end () )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "got PONG from unknown FGAS ID "
			  << Packet->SenderID );
			return;
		}
		SG_LOG ( SG_NETWORK, SG_INFO, "got PONG from FGAS " << *It );
	}
	It->LastSeen = now;
	if (diff > 0)
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "got PONG from " << It->Name
		  << " very late!" );
	}
	return;
} // fgls::HandlePong ()

//////////////////////////////////////////////////////////////////////

void
fgls::HandleCommand
(
	int Command,
	const NetAddr& Sender,
	t_ReceivePacket*  Packet
)
{
	switch (Command)
	{
	case fgms::REGISTER_SERVER:
		RegisterServer ( Sender );
		if ( ServerList.size() )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #1 >>> HUB is "  << *m_CurrentHUB );
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #1 >>> FGMS is " << *m_CurrentFGMS );
		}
		else	SG_LOG ( SG_NETWORK, SG_ALERT, "  #1 >>> no FGMS server" );
		if ( FGASList.size () )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> FGAS is " << *m_CurrentFGAS );
		}
		else	SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> no FGAS server" );
		break;
	case fgms::SERVER_QUIT:
		RemoveServer ( Packet->ClientType, Packet->SenderID );
		if ( ServerList.size() )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> HUB is "  << *m_CurrentHUB );
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> FGMS is " << *m_CurrentFGMS );
		}
		else	SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> no FGMS server" );
		if ( FGASList.size () )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> FGAS is " << *m_CurrentFGAS );
		}
		else	SG_LOG ( SG_NETWORK, SG_ALERT, "  #2 >>> no FGAS server" );
		break;
	case fgms::REQUEST_SERVER:
		RegisterFGFS ( Sender );
		SG_LOG ( SG_NETWORK, SG_ALERT, "Server requested" );
		if ( ServerList.size() > 0 )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #3 >>> HUB is "  << *m_CurrentHUB );
			SG_LOG ( SG_NETWORK, SG_ALERT, "  #3 >>> FGMS is " << *m_CurrentFGMS );
		}
		else	SG_LOG ( SG_NETWORK, SG_ALERT, "  #3 >>> no FGMS server" );
		break;
	case fgms::PING:
		HandlePing (Sender, Packet);
		break;
	case fgms::PONG:
		HandlePong (Sender, Packet);
		break;
	default:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "unhandled command: " << Command );
		SendFailed (Sender, fgms::GENERAL_ERROR, "invalid command");
	}
} // fgls::HandleCommand ()

//////////////////////////////////////////////////////////////////////

void
fgls::HandleRequest
()
{
	NetAddr	Sender;
	t_ReceivePacket*  Packet;

	m_Peer.Receive ();
	while (m_Peer.HasPackets())
	{
		Sender = m_Peer.GetSender ();
		try
		{
			Packet = m_Peer.NextPacket ();
		}
		catch (sg_exception & ex)
		{
			SendFailed (Sender, fgms::GENERAL_ERROR,
			  ex.getFormattedMessage () );
			continue;
		}
		if (Packet == 0)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "no packet !" );
			continue;
		}
		fgms::t_PropertyList RcvdProps;
		ParsePacket (Packet, m_Peer, RcvdProps, m_Properties );
		while ( RcvdProps.size () > 0 )
		{
			int Command = RcvdProps.front ();
			RcvdProps.pop_front ();
			if ( Command < fgms::INTERNAL_COMMAND )
			{
				HandleCommand (Command, Sender, Packet);
			}
		}
		m_Peer.DisableCrypt ();
	}
} // fgls::HandleRequest

//////////////////////////////////////////////////////////////////////

void
fgls::Loop
()
{
	int Client;
	time_t now;
	static time_t LastCheck = time ( 0 );

	for (;;)
	{
		Client = m_Peer.WaitForClients (m_CheckInterval);
		if ( Client == UDP_Peer::ne_TIMEOUT )
		{
		}
		else if ( Client < 0 )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "fgls::Loop() - Bytes <= 0!" );
			continue;
		}
		else if (Client > 0)
		{	// something on the wire (clients)
			HandleRequest ();
		}
		now = time ( 0 );
		if ( ( now - LastCheck ) >= m_CheckInterval )
		{
			Maintainance ();
			LastCheck = now;
		}
	}
} // fgls::Loop ()

//////////////////////////////////////////////////////////////////////

int
main
(
	int argc,
	char* argv[]
)
{
	fgls Server;
	if (! Server.ParseParams ( argc, argv ) )
	{
		return ( fgms::FAILED );
	}
	if (! Server.Init () )
	{
		return (fgms::FAILED);
	}
	Server.Loop ();
	return (fgms::SUCCESS);
}

