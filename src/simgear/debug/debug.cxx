/****
*
* NAME          debug.cpp
* DESCRIPTION   implementation of some debug functions
* AUTHOR        Oliver Schroeder <post@o-schroeder.de>
* CREATION DATE Jan-2003
*
* This file is part of SDA
*
****/

#include <simgear/debug/debug.hxx>

namespace sg_debug {

//////////////////////////////////////////////////////////////////////
// dump out a buffer as hex and ascii to stdout
//////////////////////////////////////////////////////////////////////
void 
Dump
(
    const void*   Data,
    int     DataLen
)
{
    const char Hex [] = "0123456789ABCDEF";
    int I,J,K;
    unsigned char *D;

    std::cerr << "----------------------------------" << std::endl;
    D = (unsigned char *) Data;
    J = 0;
    for (I = 0; I < (DataLen); I+=16)
    {
        for (J = I; J < I+16; J++)
        {
            if (J >= DataLen)
            {
                break;
            }
            std::cerr << Hex [D [J] >> 4];
            std::cerr << Hex [D [J] & ~240];
            std::cerr << " ";
        }
        while (J < I+16)
        {
            std::cerr << "   ";
            J++;
        }
        std::cerr << "    ";
        for (J = I; J < I+16; J++)
        {
            if (J >= DataLen)
            {
                break;
            }
            if ((D [J] > 32) && (D [J] < 128))
            {
                std::cerr << D [J];
            }
            else
            {
                std::cerr << ".";
            }
        }
        std::cerr << std::endl;
    }
    K = 0;
    for (J = I; J < I+16; J++)
    {
        if (J >= DataLen)
        {
            break;
        }
        K++;
        std::cerr << Hex [D [J] >> 4];
        std::cerr << Hex [D [J] & ~240];
        std::cerr << " ";
    }
    if (K > 0)
    {
        while (J < I+16)
        {
            std::cerr << " ";
            J++;
        }
        std::cerr << "    ";
        for (J = I; J < I+16; J++)
        {
            if (J >= DataLen)
            {
                break;
            }
            if ((D [J] > 32) && (D [J] < 128))
            {
                std::cerr << D [J];
            }
            else
            {
                std::cerr << ".";
            }
        }
        std::cerr << std::endl;
    }
    std::cerr << "----------------------------------" << std::endl;
}

unsigned int debug_indent;

std::string
space
(
    int n
)
{
    std::string r;
    for (int i=0; i<n; i++)
        r += " ";
    return (r);
}

void
ReportError
(
     const std::string& msg,
     const std::string& file,
     int line,
     const std::string& func_name,
     bool is_crit
)
{
    std::cerr << space(sg_debug::debug_indent);
    if( is_crit )
        std::cerr << "CRITICAL: ";
    else
        std::cerr << "WARNING : ";
    std::cerr << msg << std::endl;
    std::cerr << space(sg_debug::debug_indent);
    std::cerr << "          ";
    std::cerr << "in " << file << " line " << line;
    std::cerr << std::endl;
    if( func_name != "(unknown)" )
    {
        std::cerr << space(sg_debug::debug_indent);
        std::cerr << "          ";
        std::cerr << "in function: '" << func_name << "'";
        std::cerr << std::endl;
    }
    if( is_crit )
        exit (1);
}

}; // namespace sg_debug

