/****
*
* NAME          debug.hpp
* DESCRIPTION   some debug macros and functions
* AUTHOR        Oliver Schroeder <post@o-schroeder.de>
* CREATION DATE Jan-2003
*
****/

//////////////////////////////////////////////////////////////////////
// some debug functions
//////////////////////////////////////////////////////////////////////

#ifndef SG_DEBUG_HEADER
#define SG_DEBUG_HEADER 1

#include <iostream>
#include <cstdlib>
#ifdef _MSC_VER
#include <fstream>
#include <sstream>  // use ostringstream
#endif

//////////////////////////////////////////////////////////////////////
//
//  enable/disable debugging at once
//
//////////////////////////////////////////////////////////////////////
#if defined SG_DEBUG_ALL
#   define SG_ENABLE_DEBUG
#   define SG_ENABLE_TRACE
#   define SG_ENABLE_DEBUG_OUT
#endif

//////////////////////////////////////////////////////////////////////
//
//  debug macros
//
//////////////////////////////////////////////////////////////////////
namespace sg_debug {

    //////////////////////////////////////////////////
    //
    //  helper function
    //  Determines the name of the current function.
    //  Used for trace.
    ///////////////////////////////////////////////////
    inline void current_function_helper ()
    {
        #if defined(__GNUC__) \
        || (defined(__MWERKS__) && (__MWERKS__ >= 0x3000)) \
        || (defined(__ICC) && (__ICC >= 600))
            #define SG_CURRENT_FUNCTION __PRETTY_FUNCTION__
        #elif defined(__FUNCSIG__)
            #define SG_CURRENT_FUNCTION __FUNCSIG__
        #elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) \
        ||    (defined(__IBMCPP__) && (__IBMCPP__ >= 500))
            #define SG_CURRENT_FUNCTION __FUNCTION__
        #elif defined(__BORLANDC__) && (__BORLANDC__ >= 0x550)
            #define SG_CURRENT_FUNCTION __FUNC__
        #elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901)
            #define SG_CURRENT_FUNCTION __func__
        #else
            #define SG_CURRENT_FUNCTION "(unknown)"
        #endif
    }

    std::string space ( int n );

    void ReportError ( const std::string& msg, const std::string& file, 
         int line, const std::string& func_name, bool is_crit = false );

    extern unsigned int debug_indent;

    //////////////////////////////////////////////////////////////////////
    // dump out a buffer as hex and ascii to stdout
    //////////////////////////////////////////////////////////////////////
    void Dump ( const void* Data, int DataLen );

}; // namespace sg_debug

// MS compatible compilers support #pragma once
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
    #pragma once
#endif
#define SG_NOT_IMPLEMENTED \
    sg_debug::ReportError ("Function not implemented", \
                __FILE__,__LINE__, SG_CURRENT_FUNCTION, false);

#if defined SG_ENABLE_DEBUG
    //////////////////////////////////////////////////
    //
    //  enable/disable debug output
    //
    //////////////////////////////////////////////////
    #if defined SG_ENABLE_DEBUG_OUT
        #define SG_DEBUG_OUT(X) \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
            << X << std::endl;
        #define SG_DEBUG_COND(X) \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
            << X << std::endl;
        #define SG_DEBUG_EXP(X) \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
            << #X << " = " << X << std::endl
    #else
        #define SG_DEBUG_OUT(X)
        #define SG_DEBUG_COND(X)
        #define SG_DEBUG_EXP(X)
    #endif
    //////////////////////////////////////////////////
    //
    //  enable/disable assert replacements
    //
    //////////////////////////////////////////////////
    // check expression, report and exit the application on failure
    #define SG_REQUIRE(X) \
        if (! (X)) \
        { \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
              << (#X) << " = " << (X) << std::endl; \
            sg_debug::ReportError ("Condition failed", __FILE__,__LINE__, \
              SG_CURRENT_FUNCTION, true); \
        }
    // check expression and report only on failure
    #define SG_CHECK(X) \
        if (! (X)) \
        { \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
              << (#X) << " = " << (X) << std::endl; \
            sg_debug::ReportError ("Condition failed", __FILE__,__LINE__, \
              SG_CURRENT_FUNCTION, false); \
        }
    #if defined SG_ENABLE_TRACE
        #define SG_TRACE_START \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
                << "TRACE: " << SG_CURRENT_FUNCTION << " START" << std::endl; \
            sg_debug::debug_indent++;
        #define SG_TRACE_END   \
            sg_debug::debug_indent--; \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
                << "TRACE: " << SG_CURRENT_FUNCTION << " END" << std::endl;
        #define SG_TRACE_RETURN(X)   \
            sg_debug::debug_indent--; \
            std::cerr << sg_debug::space(sg_debug::debug_indent) \
                << "TRACE: " << SG_CURRENT_FUNCTION  \
                << " RETURNED " << X << std::endl;
    #else
        #define SG_TRACE_START
        #define SG_TRACE_END
        #define SG_TRACE_RETURN(X)
    #endif

#else
    #define SG_DEBUG_OUT(X)
    #define SG_DEBUG_COND(X)
    #define SG_DEBUG_EXP(X)
    #define SG_REQUIRE(X)
    #define SG_CHECK(X)
    #define SG_TRACE_START
    #define SG_TRACE_END
    #define SG_TRACE_RETURN(X)
#endif

#endif
