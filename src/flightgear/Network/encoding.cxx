// encoding.cxx
// implement different encodings for network packets
//
// Copyright (C) 2006  Oliver Schroeder
//
// This file is part of fgms
//
// $Id: encoding.cxx,v 1.1.1.1 2009/10/12 07:24:05 oliver Exp $

#if !defined(NDEBUG) && defined(_MSC_VER) // If MSVC, turn on ALL debug if Debug build
#define SG_DEBUG_ALL
#define SG_ENABLE_DEBUG
#else
#undef SG_ENABLE_DEBUG
#endif // MSVC Debug build y/n

#include "encoding.hxx"
#include <simgear/debug/debug.hxx>

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_int8 ( const int8_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        int8_t      raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << (int) Val
      << std::hex << "  encoded: " << (int) tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_int8 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
int8_t
XDR_decode_int8 ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        int8_t      raw;
    } tmp;

    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << (int) Val
      << std::hex << "  decoded: " << (int) tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_int8 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_uint8 ( const uint8_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        uint8_t      raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_uint8 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
uint8_t
XDR_decode_uint8 ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        uint8_t      raw;
    } tmp;

    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_uint8 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_int16 ( const int16_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        int16_t     raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_int16 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
int16_t
XDR_decode_int16 ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        int16_t     raw;
    } tmp;

    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_int16 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_uint16 ( const uint16_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        uint16_t    raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_uint16 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
uint16_t
XDR_decode_uint16 ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        uint16_t    raw;
    } tmp;

    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_uint16 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_int32 ( const int32_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        int32_t     raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_int32 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
int32_t
XDR_decode_int32 ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        int32_t     raw;
    } tmp;

    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_int32 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_uint32 ( const uint32_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        uint32_t    raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_uint32 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
uint32_t
XDR_decode_uint32 ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        uint32_t    raw;
    } tmp;

    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_uint32 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data2_t
XDR_encode_int64 ( const int64_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data2_t encoded;
        int64_t     raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP64 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_int64 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
int64_t
XDR_decode_int64 ( const xdr_data2_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data2_t encoded;
        int64_t     raw;
    } tmp;

    tmp.encoded = SWAP64 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_int64 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data2_t
XDR_encode_uint64 ( const uint64_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data2_t encoded;
        uint64_t    raw;
    } tmp;

    tmp.raw = Val;
    tmp.encoded = SWAP64 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_uint64 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
uint64_t
XDR_decode_uint64 ( const xdr_data2_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data2_t encoded;
        uint64_t    raw;
    } tmp;

    tmp.encoded = SWAP64 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_uint64 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data_t
XDR_encode_float ( const float & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        float       raw;
    } tmp;

    if (sizeof(float) != sizeof (xdr_data_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "XDR_encode_float: "
                  << "sizeof (float) != sizeof (xdr_data_t) !!!");
    }
    tmp.raw = Val;
    tmp.encoded = SWAP32 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_float ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
float
XDR_decode_float ( const xdr_data_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data_t  encoded;
        float       raw;
    } tmp;

    if (sizeof(float) != sizeof (xdr_data_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "XDR_decode_float: "
                  << "sizeof (float) != sizeof (xdr_data_t) !!!");
    }
    tmp.encoded = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_float ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
xdr_data2_t
XDR_encode_double ( const double & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data2_t encoded;
        double      raw;
    } tmp;

    if (sizeof(double) != sizeof (xdr_data2_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "XDR_encode_double: "
                  << "sizeof (double) != sizeof (xdr_data2_t) !!!");
    }
    tmp.raw = Val;
    tmp.encoded = SWAP64 (tmp.encoded);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.encoded);
    SG_TRACE_END
    return (tmp.encoded);
} // XDR_encode_double ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
double
XDR_decode_double ( const xdr_data2_t & Val )
{
    SG_TRACE_START
    union
    {
        xdr_data2_t encoded;
        double      raw;
    } tmp;

    if (sizeof(double) != sizeof (xdr_data2_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "XDR_decode_double: "
                  << "sizeof (double) != sizeof (xdr_data2_t) !!!");
    }
    tmp.encoded = SWAP64 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // XDR_decode_double ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
//      encode to network byte order
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int8_t
NET_encode_int8 ( const int8_t & Val )
{
    SG_TRACE_START
    SG_TRACE_END
    return (Val);
} // NET_encode_int8 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int8_t
NET_decode_int8 ( const int8_t & Val )
{
    SG_TRACE_START
    SG_TRACE_END
    return (Val);
} // NET_decode_int8 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint8_t
NET_encode_uint8 ( const uint8_t & Val )
{
    SG_TRACE_START
    SG_TRACE_END
    return (Val);
} // NET_encode_uint8 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint8_t
NET_decode_uint8 ( const uint8_t & Val )
{
    SG_TRACE_START
    SG_TRACE_END
    return (Val);
} // NET_decode_uint8 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int16_t
NET_encode_int16 ( const int16_t & Val )
{
    SG_TRACE_START
    union
    {
        int16_t  net;
        int16_t  raw;
    } tmp;

    tmp.raw = Val;
    tmp.net = SWAP16 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_int16 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int16_t
NET_decode_int16 ( const int16_t & Val )
{
    SG_TRACE_START
    union
    {
        int16_t  net;
        int16_t  raw;
    } tmp;

    tmp.net = SWAP16 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_int16 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint16_t
NET_encode_uint16 ( const uint16_t & Val )
{
    SG_TRACE_START
    union
    {
        uint16_t  net;
        uint16_t  raw;
    } tmp;

    tmp.raw = Val;
    tmp.net = SWAP16 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_uint16 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint16_t
NET_decode_uint16 ( const uint16_t & Val )
{
    SG_TRACE_START
    union
    {
        uint16_t  net;
        uint16_t  raw;
    } tmp;

    tmp.net = SWAP16 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_uint16 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int32_t
NET_encode_int32 ( const int32_t & Val )
{
    SG_TRACE_START
    union
    {
        int32_t  net;
        int32_t  raw;
    } tmp;

    tmp.raw = Val;
    tmp.net = SWAP32 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_int32 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int32_t
NET_decode_int32 ( const int32_t & Val )
{
    SG_TRACE_START
    union
    {
        int32_t  net;
        int32_t  raw;
    } tmp;

    tmp.net = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_int32 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint32_t
NET_encode_uint32 ( const uint32_t & Val )
{
    SG_TRACE_START
    union
    {
        uint32_t  net;
        uint32_t  raw;
    } tmp;

    tmp.raw = Val;
    tmp.net = SWAP32 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_uint32 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint32_t
NET_decode_uint32 ( const uint32_t & Val )
{
    SG_TRACE_START
    union
    {
        uint32_t  net;
        uint32_t  raw;
    } tmp;

    tmp.net = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_uint32 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int64_t
NET_encode_int64 ( const int64_t & Val )
{
    SG_TRACE_START
    union
    {
        int64_t  net;
        int64_t  raw;
    } tmp;

    tmp.raw = Val;
    tmp.net = SWAP64 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_int64 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
int64_t
NET_decode_int64 ( const int64_t & Val )
{
    SG_TRACE_START
    union
    {
        int64_t  net;
        int64_t  raw;
    } tmp;

    tmp.net = SWAP64 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_int64 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint64_t
NET_encode_uint64 ( const uint64_t & Val )
{
    SG_TRACE_START
    union
    {
        uint64_t  net;
        uint64_t  raw;
    } tmp;

    tmp.raw = Val;
    tmp.net = SWAP64 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_uint64 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint64_t
NET_decode_uint64 ( const uint64_t & Val )
{
    SG_TRACE_START
    union
    {
        uint64_t  net;
        uint64_t  raw;
    } tmp;

    tmp.net = SWAP64 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_uint64 ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint32_t
NET_encode_float ( const float & Val )
{
    SG_TRACE_START
    union
    {
        uint32_t  net;
        float     raw;
    } tmp;

    if (sizeof(float) != sizeof (uint32_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "NET_encode_float: " << "sizeof (float) != 4 !!!");
    }
    tmp.raw = Val;
    tmp.net = SWAP32 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_float ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
float
NET_decode_float ( const uint32_t & Val )
{
    SG_TRACE_START
    union
    {
        uint32_t  net;
        float     raw;
    } tmp;

    if (sizeof(float) != sizeof (uint32_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "NET_decode_float: " << "sizeof (float) != 4 !!!");
    }
    tmp.net = SWAP32 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_float ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint64_t
NET_encode_double ( const double & Val )
{
    SG_TRACE_START
    union
    {
        uint64_t  net;
        double    raw;
    } tmp;

    if (sizeof(double) != sizeof (uint64_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "NET_encode_double: "
                  << "sizeof (double) != sizeof (uint64_t) !!!");
    }
    tmp.raw = Val;
    tmp.net = SWAP64 (tmp.net);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  encoded: " << tmp.net);
    SG_TRACE_END
    return (tmp.net);
} // NET_encode_double ()
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
double
NET_decode_double ( const uint64_t & Val )
{
    SG_TRACE_START
    union
    {
        uint64_t  net;
        double    raw;
    } tmp;

    if (sizeof(double) != sizeof (uint64_t))
    {
        SG_LOG ( SG_NETWORK, SG_ALERT, "NET_decode_double: "
                  << "sizeof (double) != sizeof (uint64_t) !!!");
    }
    tmp.net = SWAP64 (Val);
    SG_DEBUG_OUT ("val: " << std::hex << Val
      << std::hex << "  decoded: " << tmp.raw);
    SG_TRACE_END
    return (tmp.raw);
} // NET_decode_double ()
/////////////////////////////////////////////////////////////////////

// vim: ts=4:sw=4:sts=0

