// netpacketpool.hxx -  NetPacket is a buffer for network packets
//
// This file is part of fgms
//
// Copyright (C) 2006-2010  Oliver Schroeder
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see
// <http://www.gnu.org/licenses/>.
//


#ifndef NET_BUFFER_POOL_H
#define NET_BUFFER_POOL_H

#include <list>
#include <Network/netpacket.hxx>

/** A pool of network buffers.
 * NetPacketPool manages a pool of NetPackets. The purpose is to
 * "recycle" already used buffers. So an application requests a
 * buffer from the pool and gives it back after use. This way we should
 * save some time for (re-) allocating new buffers at cost of some bytes
 * of memory but speed up a lot.
 */
class NetPacketPool
{
public:
    /// create a default pool. 10 Buffers are allocated, if
    /// the pool has less than 3 buffers left, buffers are
    /// allocated until we have 10 again.
    NetPacketPool ();
    /// destroy the pool
    ~NetPacketPool ();
    /// create a pool NetPackets
    /// @param Capacity
    ///  the Capacity of the NetPackets to create.
    /// @param MaxBuffers
    ///  number of NetPackets to create at startup. If we have more
    ///  then MaxBuffers buffers in the pool, the additional buffers
    ///  get deleted.
    /// @param MinBuffers
    ///  if the pool has less than MinBuffers free NetPackets,
    ///  it creates new buffers until there are MaxBuffers
    ///  NetPackets again
    NetPacketPool ( const uint32_t Capacity, const int MaxBuffers,
                    const int MinBuffers );
    /// Set the Capacity of NetPackets. If we already have NetPackets
    /// of a different Capacity, we discard them and create new ones
    void SetCapacity ( const uint32_t Capacity );
    /// Set the minimum number of reserved NetPackets
    void SetMinBuffers ( const int MinBuffers );
    /// Set the maximum number of reserved NetPackets
    void SetMaxBuffers ( const int MaxBuffers );
    /// if set to true, all recycled buffers get clearea() before
    /// they get reused. The default is false, no clear before reuse.
    /// "Clearing" means that all values in a buffer get set to 0.
    /// @param ClearBuffers
    ///  true: buffers get cleared
    ///  false: buffers get reset but not cleared
    void SetClearBuffers ( const bool ClearBuffers );
    /// request a buffer from the pool
    NetPacket* GetBuffer ();
    /// give back a NetPacket to the pool
    void RecycleBuffer ( NetPacket* & Buffer );
    /// retrun the number of free buffers in the pool
    inline int Size() const
    {
        return m_Buffers.size();
    };
    /// pool mainteinance. delete/create buffers so our
    /// settings are met
    void Maintain ();
private:
    /// the Capacity of the NetPackets
    uint32_t m_Capacity;
    /// Minimum number of NetPackets to hold
    uint32_t m_MinBuffers;
    /// Maximum number of NetPackets to hold
    uint32_t m_MaxBuffers;
    /// flag if buffers should be cleared before reuse
    bool  m_ClearBuffers;
    /// create given number of buffers and add them
    /// to the pool
    /// @param NumBuffers
    ///  the number of buffers to create
    void m_AddBuffers ( const int NumBuffers );
    //////////////////////////////////////////////////
    //  disallow standard and copy constructor
    //////////////////////////////////////////////////
    NetPacketPool( const NetPacket & Buffer );
    std::list<NetPacket*>  m_Buffers;
}; // NetPacketPool

#endif

// vim: ts=4:sw=4:sts=0

