//
// udp_peer.cxx - manage packets for server/clients
//
// This file is part of fgms.
//
// Copyright (C) 2006-2010 Oliver Schroeder <fgms@postrobot.de>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see
// <http://www.gnu.org/licenses/>.
//

#if !defined(NDEBUG) && defined(_MSC_VER) // If MSVC, turn on ALL debug if Debug build
#define SG_DEBUG_ALL
#define SG_ENABLE_DEBUG
#endif // 

#include <simgear/debug/logstream.hxx>
#include <simgear/debug/debug.hxx>
#include "udp_peer.hxx"
#include "md5.hxx"

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::SetDefaults
()
{
	SG_TRACE_START
	m_HaveSocket = false;
	m_AutomaticHeaders = false;
	m_BufferPool	= new NetPacketPool(UDP_Peer::MAX_PACKET_SIZE, 10, 10 );
	m_SenderID	= 0;
	m_UseCrypt	= false;
	m_Encoding	= NetPacket::XDR;
	m_MaxRetries	= 3;  // resend a packet only 3 times, then discard it
	m_ResendTime	= 5;  // resend after 5 seconds
	m_MessageCounter= 1;
	m_HasHeader	= false;
	m_RcvdPacket	= 0;
	m_Flags		= 0;
	m_CurrentPacket	= m_BufferPool->GetBuffer ();
	m_CurrentPacket->SetEncoding ( m_Encoding );
	SG_TRACE_END
} // UDP_Peer::SetDefaults ()

/////////////////////////////////////////////////////////////////////

UDP_Peer::UDP_Peer
()
{
	SG_TRACE_START
	SetDefaults ();
	SG_TRACE_END
} // UDP_Peer::UDP_Peer ()

/////////////////////////////////////////////////////////////////////

UDP_Peer::UDP_Peer
(
        const NetSocket& Socket
)
{
	SG_TRACE_START
	SetDefaults ();
	if ( Socket.GetHandle() != -1 )
	{
		m_DataSocket = Socket;
		m_HaveSocket = true;
	}
	SG_TRACE_END
} // UDP_Peer::UDP_Peer ( netSocket* Socket )

/////////////////////////////////////////////////////////////////////

UDP_Peer::UDP_Peer
(
        const string& HostIP,
        const uint32_t Port
)
{
	SG_TRACE_START
	SetDefaults ();
	Listen ( HostIP, Port );
	SG_TRACE_END
} // UDP_Peer::UDP_Peer ( HostIP, Port )

/////////////////////////////////////////////////////////////////////

UDP_Peer::~UDP_Peer
()
{
	SG_TRACE_START
	SG_TRACE_END
	// close all sockets
	// m_BufferPool is destroyed by its destructor
} // UDP_Peer::~UDP_Peer ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::Clone
(
	const UDP_Peer Peer
)
{
	m_SenderID		= Peer.m_SenderID;
	m_UseCrypt		= Peer.m_UseCrypt;
	m_Encoding		= Peer.m_Encoding;
	m_MaxRetries		= Peer.m_MaxRetries;
	m_ResendTime		= Peer.m_ResendTime;
	m_Flags			= Peer.m_Flags;
	m_Magic			= Peer.m_Magic;
	m_ClientType		= Peer.m_ClientType;
	m_HeaderSize		= Peer.m_HeaderSize;
	m_CheckSumIndex		= Peer.m_CheckSumIndex;
	m_FlagIndex		= Peer.m_FlagIndex;
	m_ProtocolVersion	= Peer.m_ProtocolVersion;
	m_CryptPassword[0]	= Peer.m_CryptPassword[0];
	m_CryptPassword[1]	= Peer.m_CryptPassword[1];
	m_CryptPassword[2]	= Peer.m_CryptPassword[2];
	m_CryptPassword[3]	= Peer.m_CryptPassword[3];
	m_AutomaticHeaders	= Peer.m_AutomaticHeaders;
	SetSocket ( Peer.m_DataSocket ); // FIXME: does'nt work
	m_CurrentPacket->SetEncoding ( m_Encoding );
} // UDP_Peer::Clone ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::SetSocket
(
        const NetSocket& Socket
)
{
	SG_TRACE_START
	if ( Socket.GetHandle() != -1 )
	{
		m_DataSocket.Assign ( Socket );
		m_HaveSocket = true;
	}
	SG_TRACE_END
} // UDP_Peer::SetSocket ( netSocket* Socket )

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::SetSocket
(
        const UDP_Peer& Peer
)
{
	SG_TRACE_START
	if ( Peer.m_DataSocket.GetHandle() != -1 )
	{
		m_DataSocket.Assign ( Peer.m_DataSocket );
		m_HaveSocket = true;
	}
	SG_TRACE_END
} // UDP_Peer::SetSocket ( netSocket* Socket )

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Listen
(
        const string& HostIP,
        const uint32_t Port
)
{
	SG_TRACE_START
	if ( m_DataSocket.Open ( NetSocket::UDP ) == 0 )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, 
		        "UDP_Peer::Listen() - failed to create socket"
		);
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	m_DataSocket.SetBlocking ( false );
	if ( ! m_DataSocket.Bind ( "", Port ) )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, 
		        "UDP_Peer::Listen() - failed to bind receive socket"
			<< " port " << Port
		);
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	m_HaveSocket = true;
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // UDP_Peer::Listen (HostIP, Port)

/////////////////////////////////////////////////////////////////////

int
UDP_Peer::WaitForClients
( int Timeout )
{
	SG_TRACE_START
	NetSocket*  ListenSockets[2];
	ListenSockets[0] = &m_DataSocket;
	ListenSockets[1] = 0;
	int ret = m_DataSocket.Select ( ListenSockets, 0, Timeout );
	SG_TRACE_RETURN ( ret );
	return ( ret );
} // UDP_Peer::WaitForClients ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::EnableAutomaticHeaders
()
{
	SG_TRACE_START
	m_AutomaticHeaders = true;
	// We only write the header to have the size
	WriteHeader ();
	m_CurrentPacket->Clear ();
	m_HasHeader = false;
	SG_TRACE_END
} // UDP_Peer::EnableAutomaticHeaders ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::DisableAutomaticHeaders
()
{
	SG_TRACE_START
	m_AutomaticHeaders = false;
	m_HeaderSize = 0;
	m_CurrentPacket->Clear ();
	SG_TRACE_END
} // UDP_Peer::DisableAutomaticHeaders ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::PacketHasData
() const
{
	SG_TRACE_START
	uint32_t BytesUsed = m_CurrentPacket->BytesUsed ();
	if ( m_AutomaticHeaders )
	{
		if ( BytesUsed > m_HeaderSize )
		{
			SG_TRACE_RETURN ( "autoheaders true" );
			return ( true );
		}
	}
	else if ( BytesUsed > 0 )
	{
		SG_TRACE_RETURN ( "true" );
		return ( true );
	}
	SG_TRACE_RETURN ( "false" );
	return ( false );
} // UDP_Peer::PacketHasData ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::WriteHeader
()
{
	SG_TRACE_START
	if ( ! m_AutomaticHeaders )
	{
		SG_TRACE_END
		return;
	}
	if ( ! m_CurrentPacket )
	{
		SG_DEBUG_OUT ( "UDP_Peer::WriteHeader - no buffer" );
		SG_TRACE_END
		return;
	}
	if (m_HasHeader)
	{
		SG_TRACE_END
		return;
	}
	// MAGIC
	m_CurrentPacket->Write_uint32 ( m_Magic );
	// ProtcolVersion
	m_CurrentPacket->Write_uint32 ( m_ProtocolVersion );
	// CheckSum (gets filled later)
	m_CheckSumIndex = m_CurrentPacket->CurrentIndex ();
	m_CurrentPacket->Skip ( sizeof ( xdr_data_t ) );
	// Flags
	m_FlagIndex = m_CurrentPacket->CurrentIndex ();
	m_CurrentPacket->Write_uint32 ( m_Flags );
	// MessageCounter
	m_CurrentPacket->Write_uint32 ( m_MessageCounter );
	m_MessageCounter++;
	// ClientType
	m_CurrentPacket->Write_uint32 ( m_ClientType );
	// SenderID
	m_CurrentPacket->Write_uint64 ( m_SenderID );
	// Timestamp
	//  m_CurrentPacket->Write_uint32 (m_Timestamp);
	m_HeaderSize = m_CurrentPacket->CurrentIndex ();
	m_HasHeader = true;
	SG_TRACE_END
} // UDP_Peer::WriteHeader ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::CheckSize
(
        const uint32_t Size
)
{
	SG_TRACE_START
	if ( ! m_CurrentPacket )
	{
		SG_DEBUG_OUT ( "UDP_Peer::CheckSize - no buffer, "
		  << "requesting from pool" );
		m_CurrentPacket = m_BufferPool->GetBuffer ();
		m_CurrentPacket->SetEncoding ( m_Encoding );
	}
	if ( ! m_HasHeader )
	{
		WriteHeader ();
	}
	if ( m_CurrentPacket->Available() < Size )
	{
		SG_DEBUG_OUT ( "UDP_Peer::CheckSize - available < requested" );
		PushBuffer ();
		SG_DEBUG_OUT ( "UDP_Peer::CheckSize - "
		  << "requesting buffer from pool" );
		m_CurrentPacket = m_BufferPool->GetBuffer ();
		m_CurrentPacket->SetEncoding ( m_Encoding );
	}
	SG_TRACE_END
} // UDP_Peer::CheckSize ( uint32_t Size )

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::PushBuffer
()
{
	SG_TRACE_START
	if ( PacketHasData () == false )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "UDP_Peer::PushBuffer() - no packet to send!" );
		SG_TRACE_END
		return;
	}
	SG_REQUIRE ( m_CurrentPacket != 0 );
	SG_REQUIRE ( m_CurrentPacket->Buffer() != 0 );
	if ( m_AutomaticHeaders )
	{
		Checksum Chk;
		Chk.Add ( m_CurrentPacket->Buffer() + m_HeaderSize,
		          m_CurrentPacket->BytesUsed() - m_HeaderSize );
		m_CurrentPacket->SetIndex ( m_CheckSumIndex );
		m_CurrentPacket->Write_uint32 ( Chk.Get() );
		m_CurrentPacket->SetIndex ( m_FlagIndex );
		m_CurrentPacket->Write_uint32 ( m_Flags );
		if ( ( m_Flags & RELIABLE ) > 0 )
		{
			SG_DEBUG_OUT ( "UDP_Peer::PushBuffer - "
			  << "pushing buffer to ack-queue" );
			t_AwaitAckPacket* AckPkt = new t_AwaitAckPacket;
			AckPkt->SentCounter = 1;
			AckPkt->SentTime = time ( 0 );
			AckPkt->Buffer = m_CurrentPacket;
			m_AwaitAckQueue[m_MessageCounter] = AckPkt;
		}
	}
	if ( m_UseCrypt )
	{
		m_CurrentPacket->Encrypt ( m_CryptPassword, m_HeaderSize );
	}
#ifdef SG_ENABLE_DEBUG
	sg_debug::Dump ( ( void* ) m_CurrentPacket->Buffer(),
		 m_CurrentPacket->BytesUsed() + 8 );
#endif
	m_SendQueue.push_back ( m_CurrentPacket );
	m_CurrentPacket = m_BufferPool->GetBuffer ();
	m_HasHeader = false;
	m_Flags = 0;
	SG_TRACE_END
} // UDP_Peer::PushBuffer ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::SetTarget
(
        const string& TargetHost,
        int Port
)
{
	SG_TRACE_START
	m_Target.Assign ( TargetHost, Port );
	SG_TRACE_END
} //  UDP_Peer::SetTarget (Host, Port)

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::SetTarget
(
        const NetAddr& Addr
)
{
	SG_TRACE_START
	m_Target.Assign ( Addr );
	SG_TRACE_END
} // UDP_Peer::SetTarget (Addr)

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::GotAck
(
        const uint32_t PacketID
)
{
	std::map<uint32_t, t_AwaitAckPacket*>::iterator it;
	SG_TRACE_START
	it = m_AwaitAckQueue.find ( PacketID );
	if ( it == m_AwaitAckQueue.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "UDP_Peer::GotAck() - "
		  << "Got ack for packet, but I don't have that packet" );
		SG_TRACE_END
		return;
	}
	SG_DEBUG_OUT ( "- deleting from queue" );
	delete ( it->second );
	m_AwaitAckQueue.erase ( it );
	SG_TRACE_END
} // UDP_Peer::GotAck ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::CheckAckQueue
()
{
	SG_TRACE_START
	std::map<uint32_t, t_AwaitAckPacket*>::iterator it;
	t_AwaitAckPacket* AckPkt;
	time_t            Now = time ( 0 );
	it = m_AwaitAckQueue.begin ();
	SG_DEBUG_OUT ( "ResendTime: " << m_ResendTime );
	SG_DEBUG_OUT ( "MaxRetries: " << m_MaxRetries );
	SG_DEBUG_OUT ( "Now       : " << Now );
	while ( it != m_AwaitAckQueue.end() )
	{
		AckPkt = it->second;
		SG_DEBUG_OUT ( "PacketTime: " << AckPkt->SentTime );
		SG_DEBUG_OUT ( "PacketCnt : " << AckPkt->SentCounter );
		if ( ( Now - AckPkt->SentTime ) < m_ResendTime )
		{
			SG_DEBUG_OUT ("ResendTime not reached, not sending packet " );
			it++;
			continue;
		}
		AckPkt->SentCounter++;
		SG_DEBUG_OUT ( "PacketCnt : " << AckPkt->SentCounter );
		if ( AckPkt->SentCounter > m_MaxRetries )
		{
			// FIXME: what to do here? We were asked to send
			// the packet reliably, but what shall we do if 
			// we get no response?
			SG_LOG ( SG_NETWORK, SG_ALERT, "UDP_Peer::CheckAckQueue() - "
			  << "packet reached max_retries, deleting..." );
			uint32_t Counter = it->first;
			m_AwaitAckQueue.erase ( it );
			it = m_AwaitAckQueue.upper_bound ( Counter );
			continue;
		}
		SG_DEBUG_OUT ( "- resending packet" );
		AckPkt->SentTime = Now;
		m_SendQueue.push_back ( AckPkt->Buffer );
		it++;
	}
	SG_TRACE_END
} // UDP_Peer::CheckAckQueue ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::AddBuffer
(
        NetPacket* Buffer
)
{
	SG_TRACE_START
	m_SendQueue.push_back ( Buffer );
	SG_TRACE_END
} // UDP_Peer::AddBuffer ( NetPacket* Buffer )

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::Send
()
{
	SG_TRACE_START
	if ( m_HaveSocket == false )
	{
		SG_DEBUG_OUT ( "I have no peer!" );
		SG_TRACE_END
		return;
	}
	if ( m_Target.Error() != NetAddr::E_OK )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, m_Target.GetErrorMsg() );
	}
	PushBuffer ();
	CheckAckQueue ();
	std::list<NetPacket*>::iterator Buffer;
	Buffer = m_SendQueue.begin();
	SG_DEBUG_OUT ( m_SendQueue.size() << " packets to send" );
	while ( Buffer != m_SendQueue.end() )
	{
		SG_DEBUG_OUT ( 
			"UDP_Peer::Send - sending packet "
			<< ( *Buffer )->BytesUsed () << " bytes"
		);
		m_DataSocket.SendTo ( ( void* ) ( *Buffer )->Buffer(),
		                      ( *Buffer )->BytesUsed (), m_Target );
		RecycleBuffer ( *Buffer );
		Buffer = m_SendQueue.erase ( Buffer );
	}
} // UDP_Peer::Send ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::WriteFlags
(
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	m_Flags |= Flags;
	SG_TRACE_END
} // UDP_Peer::WriteFlags ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_int8
(
        const int8_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_int8 ( Data ) );
} // UDP_Peer::Write_int8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_uint8
(
        const uint8_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_uint8 ( Data ) );
} // UDP_Peer::Write_uint8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_int16
(
        const int16_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_int16 ( Data ) );
} // UDP_Peer::Write_int16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_uint16
(
        const uint16_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_uint16 ( Data ) );
} // UDP_Peer::Write_uint16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_int32
(
        const int32_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( Data ) ); // 32-Bit, regardless of encoding
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_int32 ( Data ) );
} // UDP_Peer::Write_int32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_uint32
(
        const uint32_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( Data ) ); // 32-Bit, regardless of encoding
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_uint32 ( Data ) );
} // UDP_Peer::Write_uint32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_int64
(
        const int64_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( Data ) ); // 64-Bit, regardless of encoding
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_int64 ( Data ) );
} // UDP_Peer::Write_int64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_uint64
(
        const uint64_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( Data ) ); // 64-Bit, regardless of encoding
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_uint64 ( Data ) );
} // UDP_Peer::Write_uint64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_float
(
        const float& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( Data ) ); // 32-Bit, regardless of encoding
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_float ( Data ) );
} // UDP_Peer::Write_float ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::Write_double
(
        const double& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( Data ) ); // 64-Bit, regardless of encoding
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	SG_TRACE_END
	return ( m_CurrentPacket->Write_double ( Data ) );
} // UDP_Peer::Write_double ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_int8
(
        const uint32_t ID,
        const int8_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int8 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_int8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_uint8
(
        const uint32_t ID,
        const uint8_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint8 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_uint8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_int16
(
        const uint32_t ID,
        const int16_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int16 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_int16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_uint16
(
        const uint32_t ID,
        const uint16_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint16 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_uint16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_int32
(
        const uint32_t ID,
        const int32_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int32 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_int32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_uint32
(
        const uint32_t ID,
        const uint32_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint32 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_uint32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_int64
(
        const uint32_t ID,
        const int64_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int64 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_int64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_uint64
(
        const uint32_t ID,
        const uint64_t& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint64 ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_uint64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_float
(
        const uint32_t ID,
        const float& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_float ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_float ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID1_double
(
        const uint32_t ID,
        const double& Data,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_double ( Data );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID1_double ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_int8
(
        const uint32_t ID,
        const int8_t& Data1,
        const int8_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int8 ( Data1 );
	m_CurrentPacket->Write_int8 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_int8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_uint8
(
        const uint32_t ID,
        const uint8_t& Data1,
        const uint8_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint8 ( Data1 );
	m_CurrentPacket->Write_uint8 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_uint8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_int16
(
        const uint32_t ID,
        const int16_t& Data1,
        const int16_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int16 ( Data1 );
	m_CurrentPacket->Write_int16 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_int16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_uint16
(
        const uint32_t ID,
        const uint16_t& Data1,
        const uint16_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint16 ( Data1 );
	m_CurrentPacket->Write_uint16 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_uint16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_int32
(
        const uint32_t ID,
        const int32_t& Data1,
        const int32_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int32 ( Data1 );
	m_CurrentPacket->Write_int32 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_int32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_uint32
(
        const uint32_t ID,
        const uint32_t& Data1,
        const uint32_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint32 ( Data1 );
	m_CurrentPacket->Write_uint32 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_uint32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_int64
(
        const uint32_t ID,
        const int64_t& Data1,
        const int64_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int64 ( Data1 );
	m_CurrentPacket->Write_int64 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_int64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_uint64
(
        const uint32_t ID,
        const uint64_t& Data1,
        const uint64_t& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint64 ( Data1 );
	m_CurrentPacket->Write_uint64 ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_uint64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_float
(
        const uint32_t ID,
        const float& Data1,
        const float& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_float ( Data1 );
	m_CurrentPacket->Write_float ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_float ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID2_double
(
        const uint32_t ID,
        const double& Data1,
        const double& Data2,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_double ( Data1 );
	m_CurrentPacket->Write_double ( Data2 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID2_double ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_int8
(
        const uint32_t ID,
        const int8_t& Data1,
        const int8_t& Data2,
        const int8_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t )
		            + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 )
		            + sizeof ( Data2 ) + sizeof ( Data3 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int8 ( Data1 );
	m_CurrentPacket->Write_int8 ( Data2 );
	m_CurrentPacket->Write_int8 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_int8 ()

/////////////////////////////////////////////////////////////////////

bool UDP_Peer::WriteID3_uint8
(
        const uint32_t ID,
        const uint8_t& Data1,
        const uint8_t& Data2,
        const uint8_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t )
		            + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize (
		        sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint8 ( Data1 );
	m_CurrentPacket->Write_uint8 ( Data2 );
	m_CurrentPacket->Write_uint8 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_uint8 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_int16
(
        const uint32_t ID,
        const int16_t& Data1,
        const int16_t& Data2,
        const int16_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t )
		            + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 )
		            + sizeof ( Data2 ) + sizeof ( Data3 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int16 ( Data1 );
	m_CurrentPacket->Write_int16 ( Data2 );
	m_CurrentPacket->Write_int16 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_int16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_uint16
(
        const uint32_t ID,
        const uint16_t& Data1,
        const uint16_t& Data2,
        const uint16_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( m_Encoding == NetPacket::XDR )
	{
		CheckSize ( sizeof ( ID ) + sizeof ( xdr_data_t )
		            + sizeof ( xdr_data_t ) + sizeof ( xdr_data_t ) );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Data1 )
		            + sizeof ( Data2 ) + sizeof ( Data3 ) );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint16 ( Data1 );
	m_CurrentPacket->Write_uint16 ( Data2 );
	m_CurrentPacket->Write_uint16 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_uint16 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_int32
(
        const uint32_t ID,
        const int32_t& Data1,
        const int32_t& Data2,
        const int32_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int32 ( Data1 );
	m_CurrentPacket->Write_int32 ( Data2 );
	m_CurrentPacket->Write_int32 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_int32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_uint32
(
        const uint32_t ID,
        const uint32_t& Data1,
        const uint32_t& Data2,
        const uint32_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint32 ( Data1 );
	m_CurrentPacket->Write_uint32 ( Data2 );
	m_CurrentPacket->Write_uint32 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_uint32 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_int64
(
        const uint32_t ID,
        const int64_t& Data1,
        const int64_t& Data2,
        const int64_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_int64 ( Data1 );
	m_CurrentPacket->Write_int64 ( Data2 );
	m_CurrentPacket->Write_int64 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_int64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_uint64
(
        const uint32_t ID,
        const uint64_t& Data1,
        const uint64_t& Data2,
        const uint64_t& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_uint64 ( Data1 );
	m_CurrentPacket->Write_uint64 ( Data2 );
	m_CurrentPacket->Write_uint64 ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_uint64 ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_float
(
        const uint32_t ID,
        const float& Data1,
        const float& Data2,
        const float& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_float ( Data1 );
	m_CurrentPacket->Write_float ( Data2 );
	m_CurrentPacket->Write_float ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_float ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteID3_double
(
        const uint32_t ID,
        const double& Data1,
        const double& Data2,
        const double& Data3,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	CheckSize ( sizeof ( ID ) + sizeof ( Data1 ) + sizeof ( Data2 ) + sizeof ( Data3 ) );
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->Write_double ( Data1 );
	m_CurrentPacket->Write_double ( Data2 );
	m_CurrentPacket->Write_double ( Data3 );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteID3_double ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteOpaque
(
        const char* Data,
        const uint32_t Size,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( Size == 0 )
	{
		SG_TRACE_END
		return ( false );
	}
	if ( m_Encoding == NetPacket::XDR )
	{
		int rndup = Size % sizeof ( xdr_data_t );
		CheckSize ( sizeof ( Size ) + Size + rndup );
	}
	else
	{
		CheckSize ( sizeof ( Size ) + Size );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->WriteOpaque ( Data, Size );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteOpaque ( Data )

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteOpaque
(
        const uint32_t ID,
        const char* Data,
        const uint32_t Size,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	if ( Size == 0 )
	{
		SG_TRACE_END
		return ( false );
	}
	if ( m_Encoding == NetPacket::XDR )
	{
		int rndup = Size % sizeof ( xdr_data_t );
		CheckSize ( sizeof ( ID ) + sizeof ( Size ) + Size + rndup );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Size ) + Size );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->WriteOpaque ( Data, Size );
	SG_TRACE_END
	return ( true );
} //  UDP_Peer::WriteOpaque ( ID, Data )

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteString
(
        const string& Str,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	int Size = Str.size() + 1; // copy trailing zero
	if ( Size == 1 )
	{
		SG_TRACE_END
		return ( false );
	}
	if ( m_Encoding == NetPacket::XDR )
	{
		int rndup = Size % sizeof ( xdr_data_t );
		CheckSize ( sizeof ( Size ) + Size + rndup );
	}
	else
	{
		CheckSize ( sizeof ( Size ) + Size );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->WriteOpaque ( Str.c_str(), Size );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteString ( string )

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteString
(
        const uint32_t ID,
        const string& Str,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	int Size = Str.size() + 1; // copy trailing zero
	if ( Size == 1 )
	{
		SG_TRACE_END
		return ( false );
	}
	if ( m_Encoding == NetPacket::XDR )
	{
		int rndup = Size % sizeof ( xdr_data_t );
		CheckSize ( sizeof ( ID ) + sizeof ( Size ) + Size + rndup );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Size ) + Size );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->WriteOpaque ( Str.c_str(), Size );
	SG_TRACE_END
	return ( true );
} //  UDP_Peer::WriteString ( ID, Data )

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteCStr
(
        const char* Str,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	int Size = strlen ( Str ) + 1; // copy trailing zero
	if ( Size == 1 )
	{
		SG_TRACE_END
		return ( false );
	}
	if ( m_Encoding == NetPacket::XDR )
	{
		int rndup = Size % sizeof ( xdr_data_t );
		CheckSize ( sizeof ( Size ) + Size + rndup );
	}
	else
	{
		CheckSize ( sizeof ( Size ) + Size );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->WriteCStr ( Str, Size );
	SG_TRACE_END
	return ( true );
} // UDP_Peer::WriteString ( string )

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::WriteCStr
(
        const uint32_t ID,
        const char* Str,
        const eNETPACKET_FLAGS Flags
)
{
	SG_TRACE_START
	SG_REQUIRE ( m_CurrentPacket != NULL )
	int Size = strlen ( Str ) + 1; // copy trailing zero
	if ( Size == 1 )
	{
		SG_TRACE_END
		return ( false );
	}
	if ( m_Encoding == NetPacket::XDR )
	{
		int rndup = Size % sizeof ( xdr_data_t );
		CheckSize ( sizeof ( ID ) + sizeof ( Size ) + Size + rndup );
	}
	else
	{
		CheckSize ( sizeof ( ID ) + sizeof ( Size ) + Size );
	}
	if ( Flags != UDP_Peer::UNRELIABLE )
	{
		WriteFlags ( Flags );
	}
	m_CurrentPacket->Write_uint32 ( ID );
	m_CurrentPacket->WriteCStr ( Str, Size );
	SG_TRACE_END
	return ( true );
} //  UDP_Peer::WriteCStr ( ID, Data )

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::CheckHeader
(
        t_ReceivePacket* ReceivedPacket
)
{
	SG_TRACE_START
	ReceivedPacket->HasErrors = false;
	ReceivedPacket->ErrorMesg += "";
	NetPacket* Buffer = ReceivedPacket->Buffer;
	SG_DEBUG_OUT (
		"UDP_Peer::CheckHeader - "
		<< "received " << Buffer->BytesUsed() << " bytes"
	);
	Buffer->Start ();
	if ( ! m_AutomaticHeaders )
	{
		ReceivedPacket->Flags           = 0;
		ReceivedPacket->ClientType      = fgms::NOCLIENT;
		ReceivedPacket->MessageCounter  = 0;
		ReceivedPacket->SenderID        = 0;
		ReceivedPacket->HasErrors       = 0;
		m_HeaderSize                    = 0;
		SG_DEBUG_OUT (
			"UDP_Peer::CheckHeader - "
			<< "automatic headers are disabled, don't checking"
		);
		SG_TRACE_END
		return;
	}
	try
	{
		uint32_t tmp;
		// MAGIC
		tmp = Buffer->Read_uint32();
		if ( tmp != m_Magic )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, 
				"UDP_Peer::CheckHeader - "
				"Magic is " << tmp
				<< " but should be " << m_Magic
			);
			ReceivedPacket->HasErrors = true;
			ReceivedPacket->ErrorMesg += " magic missmatch";
			return;
		}
		ReceivedPacket->Magic = tmp;
		// ProtocolVersion
		tmp = Buffer->Read_uint32();
		if ( tmp != m_ProtocolVersion )
		{
			int16_t* v1 = ( int16_t* ) &tmp;
			int16_t* v2 = ( int16_t* ) &m_ProtocolVersion;
			SG_LOG ( SG_NETWORK, SG_ALERT, 
			        "Protocol is " << v1[LOW] << "." << v1[HIGH] <<
			        " but should be " << v2[LOW] << "." << v2[HIGH]
			);
			ReceivedPacket->HasErrors = true;
			ReceivedPacket->ErrorMesg += " protocol version mismatch";
			return;
		}
		// CheckSum
		uint32_t PacketChkSum = Buffer->Read_uint32 ();
		// Flags
		ReceivedPacket->Flags = Buffer->Read_uint32 ();
		// MessageCounter
		ReceivedPacket->MessageCounter = Buffer->Read_uint32 ();
		// ClientType
		ReceivedPacket->ClientType =
		        static_cast<fgms::eCLIENTTYPES> ( Buffer->Read_uint32 () );
		// SenderID
		ReceivedPacket->SenderID = Buffer->Read_uint64 ();
		// Timestamp
		//  m_Timestamp = Buffer->Read_uint32 ();
		m_HeaderSize = Buffer->CurrentIndex ();
		SG_DEBUG_OUT (
			"UDP_Peer::CheckHeader - HeaderSize: "
			<< m_HeaderSize
		);
		Checksum Chk;
		Chk.Add ( Buffer->Buffer() + m_HeaderSize,
			  Buffer->BytesUsed() - m_HeaderSize );
		if ( Chk.Get() != PacketChkSum )
		{
			ReceivedPacket->HasErrors = true;
			ReceivedPacket->ErrorMesg += " cheksum error";
			return;
		}
	}
	catch  ( sg_exception ex )
	{
		ReceivedPacket->HasErrors = true;
		ReceivedPacket->ErrorMesg += " Headersize incorrect";
	}
	SG_DEBUG_OUT ( "packet was checked" );
	SG_TRACE_END
	return;
} // UDP_Peer::CheckHeader ( NetPacket* Buffer )

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::Receive
()
{
	SG_TRACE_START
	int BytesRead = 1;
	NetPacket* Buffer;
	NetAddr    SenderAddr;

	if ( ! m_HaveSocket )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "UDP_Peer::Receive() - I have no peer!" );
		return;
	}
	while ( BytesRead > 0 )
	{
		Buffer = m_BufferPool->GetBuffer ();
		BytesRead = m_DataSocket.RecvFrom ( *Buffer, SenderAddr );
		if ( BytesRead <= 0 )
		{
#ifdef _MSC_VER
            int wsa_errno = WSAGetLastError();
            if (wsa_errno != 0)
            {
                WSASetLastError(0);
                if (wsa_errno == WSAEWOULDBLOCK)
                {
                    SG_TRACE_RETURN("EAGAIN")
                    return;
                }
            }
#else
			if ( errno == EAGAIN )
			{
				SG_TRACE_RETURN ( "EAGAIN" )
				return;
			}
#endif
			SG_LOG ( SG_NETWORK, SG_ALERT,  "UDP_Peer::Receive() - " << "recv(client) <= 0!" );
			SG_TRACE_END
			return;
		}
		m_Sender = SenderAddr;
		SG_DEBUG_OUT ( "received " << BytesRead << " bytes." );
		Buffer->SetUsed ( BytesRead );
		t_ReceivePacket*  ReceivedPacket = new t_ReceivePacket;
		ReceivedPacket->Buffer = Buffer;
		ReceivedPacket->Sender = SenderAddr;
		SG_DEBUG_OUT ( "# SenderPort1: " << m_Sender.Port() );
		m_ReceiveQueue.push_back ( ReceivedPacket );
	}
	SG_TRACE_END
} // UDP_Peer::Receive ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::HasPackets
() const
{
	SG_TRACE_START
	if ( m_ReceiveQueue.size() > 0 )
	{
		SG_TRACE_RETURN ( "true" );
		return ( true );
	}
	SG_TRACE_RETURN ( "false" );
	return ( false );
} // UDP_Peer::HasPackets ()

/////////////////////////////////////////////////////////////////////

t_ReceivePacket*
UDP_Peer::NextPacket
() throw ( sg_exception )
{
	SG_TRACE_START
	// SG_REQUIRE(m_BufferPool != NULL)
	if ( ! HasPackets() )
	{
		SG_TRACE_RETURN ( "no packet" );
		return ( ( t_ReceivePacket* ) 0 );
	}
	if ( m_RcvdPacket != 0 )
	{
		RecycleBuffer ( m_RcvdPacket->Buffer );
		m_RcvdPacket->Buffer = 0;
		delete m_RcvdPacket;
	}
	m_RcvdPacket = m_ReceiveQueue.front();
	m_RcvdPacket->Buffer->SetEncoding ( m_Encoding );
	m_ReceiveQueue.pop_front ();
	if ( m_UseCrypt )
	{
		SG_DEBUG_OUT ( "UDP_Peer::NextPacket - "
		  << "encryption enabled - decrypting packet" );
		m_RcvdPacket->Buffer->Decrypt ( m_CryptPassword, m_HeaderSize );
	}
	CheckHeader ( m_RcvdPacket );
	if ( m_RcvdPacket->HasErrors )
	{
		SG_TRACE_RETURN ( m_RcvdPacket->ErrorMesg );
		sg_exception ex ( m_RcvdPacket->ErrorMesg, "UDP_Peer::NextPacket" );
		throw ( ex );
	}
	SG_TRACE_RETURN ( "packet" );
	return ( m_RcvdPacket );
} // UDP_Peer::NextPacket ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::RecycleBuffer
(
        NetPacket* NetBuffer
)
{
	if ( NetBuffer != 0 )
	{
		NetBuffer->Clear();
		m_BufferPool->RecycleBuffer ( NetBuffer );
		delete NetBuffer;
	}
} // UDP_Peer::RecycleBuffer ()

/////////////////////////////////////////////////////////////////////

NetAddr
UDP_Peer::GetSender
() const
{
	SG_TRACE_START
	SG_TRACE_END
	return ( m_Sender );
} // UDP_Peer::GetSender ()

/////////////////////////////////////////////////////////////////////

uint32_t
UDP_Peer::GetFlags
() const
{
	SG_TRACE_START
	SG_TRACE_END
	return ( m_RcvdPacket->Flags );
} // UDP_Peer::GetFlags ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::isReliable
() const
{
	SG_TRACE_START
	SG_TRACE_END
	return ( ( m_RcvdPacket->Flags & RELIABLE ) > 0 );
} // UDP_Peer::isReliable ()

/////////////////////////////////////////////////////////////////////

uint32_t
UDP_Peer::GetMessageCounter
() const
{
	SG_TRACE_START
	SG_TRACE_END
	return ( m_RcvdPacket->MessageCounter );
} // UDP_Peer::GetMessageCounter ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::EnableCrypt
(
	const string& Password
)
{
	m_UseCrypt = true;
	MD5 md5 = MD5(Password);
	uint32_t* Key;
	Key = (uint32_t*) md5.get_digest ();
	m_CryptPassword[0] = Key[0];
	m_CryptPassword[1] = Key[1];
	m_CryptPassword[2] = Key[2];
	m_CryptPassword[3] = Key[3];
} // UDP_Peer::EnableCrypt ()

/////////////////////////////////////////////////////////////////////

void
UDP_Peer::DisableCrypt
()
{
	m_UseCrypt = false;
	m_CryptPassword[0] = 0;
	m_CryptPassword[1] = 0;
	m_CryptPassword[2] = 0;
	m_CryptPassword[3] = 0;
} // UDP_Peer::DisableCrypt ()

/////////////////////////////////////////////////////////////////////

bool
UDP_Peer::GetHasErrors
() const
{
	SG_TRACE_START
	SG_TRACE_END
	return ( m_RcvdPacket->HasErrors );
} // UDP_Peer::GetHasErrors ()

/////////////////////////////////////////////////////////////////////

string
UDP_Peer::GetErrorMesg
() const
{
	SG_TRACE_START
	SG_TRACE_END
	return ( m_RcvdPacket->ErrorMesg );
} // UDP_Peer::GetErrorMesg ()

/////////////////////////////////////////////////////////////////////

