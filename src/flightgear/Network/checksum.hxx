/*
 * CheckSum class declaration 
 * 
 * From http://www.flounder.com/checksum.htm
 * modified for FlightGear by Oliver Schroeder
 *
 * This file is part of fgms
 *
 */

#ifndef CHECKSUM_H
#define CHECKSUM_H

#include <simgear/misc/stdint.hxx>

/** This class provide checksuming service.
 */
class Checksum
{
public:
    /**
     * Default constructor
     */
    Checksum();
    /**
     * Reset to an initial state. 
     */
    void Clear();
    /**
     * add data to the checksum 
     * @param Value a 32 bit word of data 
     */
    void Add ( uint32_t Value );
    /**
     * add data to the checksum 
     * @param Value a 16 bit word of data.
     */
    void Add ( uint16_t Value );
    /**
     * Add one byte of data for checksuming 
     * @param Value a 8 bit word of data. 
     */
    void Add ( uint8_t Value );
    /**
     * add an array of byte to the checksum. 
     * @param Buffer a pointer to the buffer.
     * @param Length the size of the buffer. 
     */
    void Add ( const char* Buffer, uint32_t Length );
    /**
     * Get the checksum of the data. 
     */
    uint32_t Get ();
protected:
    uint16_t m_R;
    uint16_t m_C1;
    uint16_t m_C2;
    uint32_t m_Sum;
};

#endif

// vim: ts=4:sw=4:sts=0

