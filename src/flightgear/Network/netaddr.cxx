/// @file netaddr.cxx
/// implement a class for IP handling
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2005-2015
/// @copyright	GPLv3
///
/// long description here.
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#if !defined(NDEBUG) && defined(_MSC_VER) // If MSVC, turn on ALL debug if Debug build
#define SG_DEBUG_ALL
#endif // MSVC Debug config

#include <list>
#include <ctype.h>      // toupper()
#ifdef _MSC_VER
#include <WinSock2.h>
#else
#include <netdb.h>      // gethostbyname()
#include <arpa/inet.h>  // htons()
#endif
#include <fglib/fg_typcnvt.hxx>
#include <simgear/debug/debug.hxx>
#include "netaddr.hxx"

//////////////////////////////////////////////////////////////////////
/**
 * standard constructor. Calls @a pm_Init
 * @see NetAddr::pm_Init
 */
//////////////////////////////////////////////////////////////////////
NetAddr::NetAddr
()
{
    SG_TRACE_START
    pm_nError       = E_OK;
    pm_nPort        = 0;
    pm_Init ();
    SG_TRACE_END
} // NetAddr::NetAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * A NetAddr can be constructed as a copy of another NetAddr.
 * @param Addr The %NetAddr to copy from
 */
//////////////////////////////////////////////////////////////////////
NetAddr::NetAddr
(
    const NetAddr& Addr
)
{
    SG_TRACE_START
    pm_nError       = E_OK;
    pm_Init ();
    for (int i=0; i<NetAddr::Size; i++)
    {
        pm_nAddr[i]     = Addr.pm_nAddr[i];
        pm_nMaskAddr[i] = Addr.pm_nMaskAddr[i];
    }
    pm_nError       = Addr.pm_nError;
    pm_nType        = Addr.pm_nType;
    pm_nMask        = Addr.pm_nMask;
    pm_nPort        = Addr.pm_nPort;
    SG_TRACE_END
} // NetAddr::NetAddr ( const NetAddr& Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * A NetAddr can be constructed from a hostname and a port number.
 * @param Addr a hostname or string representation of an IP address
 * @param Port a port number
 */
//////////////////////////////////////////////////////////////////////
NetAddr::NetAddr
(
    const string& Addr,
    const int Port
)
{
    SG_TRACE_START
    pm_nError       = E_OK;
    pm_nPort        = Port;
    pm_FromString (Addr.c_str());
    SG_TRACE_END
} // NetAddr::NetAddr ( const string& Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetAddr::Assign
(
    unsigned int IP,
    const int Port
)
{
    SG_TRACE_START
    pm_Init();
    int x = 1;
    if (*((char *) &x) != 0)
    {   // little endian, swap bytes
        IP = ((IP>>8) & 0x00FF00FFL) | ((IP<< 8) & 0xFF00FF00L);
        IP = (IP >> 16) | (IP << 16);
    }
    pm_nType = NetAddr::IPv4;
    pm_nMask = 32;
    char* C = (char*) &IP;
    pm_nAddr[12] = C[0];
    pm_nAddr[13] = C[1];
    pm_nAddr[14] = C[2];
    pm_nAddr[15] = C[3];
    if ( Port != 0 )
	    pm_nPort     = Port;
    SG_TRACE_END
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetAddr::Assign
(
    const string & IP,
    const int Port
)
{
    SG_TRACE_START
    pm_nError = E_OK;
    pm_FromString (IP.c_str());
    if ( Port != 0 )
	    pm_nPort     = Port;
    SG_TRACE_END
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetAddr::Assign
(
    const NetAddr & Addr
)
{
    SG_TRACE_START
    pm_nError = E_OK;
    pm_Init ();
    for (int i=0; i<NetAddr::Size; i++)
    {
        pm_nAddr[i]     = Addr.pm_nAddr[i];
        pm_nMaskAddr[i] = Addr.pm_nMaskAddr[i];
    }
    pm_nError       = Addr.pm_nError;
    pm_nType        = Addr.pm_nType;
    pm_nMask        = Addr.pm_nMask;
    pm_nPort        = Addr.pm_nPort;
    SG_TRACE_END
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Convert a struct sockaddr* to our internal
 * Format.
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::Assign
(
    struct sockaddr* SrcSockAddr
)
{
    SG_TRACE_START
    struct sockaddr* SockAddr = (struct sockaddr*) SrcSockAddr;
    for (int i=0; i<NetAddr::Size; i++)
        pm_nAddr[i] = 0;
    if (SockAddr->sa_family == AF_INET)
    {   // IPv4
        pm_nMask  = MaxMaskIPv4;  // always host address
        pm_nType  = IPv4;
        pm_nError = E_OK;
        struct sockaddr_in* SockAddrV4 = (struct sockaddr_in*) SrcSockAddr;
        pm_nPort  = htons(SockAddrV4->sin_port);
        memcpy (& pm_nAddr[12], & (SockAddrV4->sin_addr), 4);
    }
    else if (SockAddr->sa_family == AF_INET6)
    {   // IPv6
        pm_nMask  = MaxMask;  // always host address
        pm_nType  = IPv6;
        pm_nError = E_OK;
        struct sockaddr_in6* SockAddrV6 = (struct sockaddr_in6*) SrcSockAddr;
        pm_nPort  = htons(SockAddrV6->sin6_port);
        memcpy (& pm_nAddr, & (SockAddrV6->sin6_addr), 16);
    }
    SG_TRACE_END
} // NetAddr::Assign (sockaddr)

//////////////////////////////////////////////////////////////////////

void
NetAddr::SetPort
(
	int Port
)
{
        pm_nPort  = Port;
} // NetAddr::SetPort ()

//////////////////////////////////////////////////////////////////////

void
NetAddr::Resolve
(
    const string & Host,
    const int Port
)
{
    SG_TRACE_START
    pm_nError = E_OK;
    pm_nPort  = Port;
    struct hostent *hp = gethostbyname (Host.c_str());
    if (hp != NULL)
    {
        memcpy ((char *) &pm_nAddr[12], hp->h_addr, hp->h_length);
        if (hp->h_addrtype == AF_INET)
        {
            pm_nMask = 32;
            pm_nType = NetAddr::IPv4;
        }
        if (hp->h_addrtype == AF_INET6)
        {
            pm_nMask = 128;
            pm_nType = NetAddr::IPv6;
        }
    }
    else
    {
        pm_Init ();
        pm_nError = E_CouldNotResolve;
    }
    SG_TRACE_END
} // NetAddr::Resolve ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Inititiales member variables. Creates an 'empty' address.
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::pm_Init
()
{
    SG_TRACE_START
    for (int i=0; i<NetAddr::Size; i++)
        pm_nAddr[i] = 0;
    pm_nType        = NetAddr::ANY;
    pm_nMask        = NetAddr::MaxMask;
    pm_nMaskAddr[0] = 0;
    SG_TRACE_END
} // NetAddr::pm_Init ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Set internal error number.
 * @param nError internal %ERROR_CODE
 * @see ERROR_CODE
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::pm_SetError
(
    const ERROR_CODE nError
)
{
    SG_TRACE_START
    pm_nError = nError;
    pm_nType  = NetAddr::Invalid;
    SG_TRACE_END
    return;
} // NetAddr::pm_SetError ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Return a message describing an error
 * @see pm_SetError
 */
//////////////////////////////////////////////////////////////////////
string
NetAddr::GetErrorMsg
() const
{
    switch (pm_nError)
    {
    case E_OK:
        return("no error occured!");
    case E_AddressIsEmpty:
        return("address string is empty!");
    case E_IllegalChars:
        return("illegal characters in address string!");
    case E_NetmaskDecimal:
        return("netmask must be in decimal notation!");
    case E_NetmaskMissing:
        return("expected netmask, but there is none!");
    case E_OutOfRange:
        return("netmask is out of range!");
    case E_WrongIPv4:
        return("wrong format in IPv4 address!");
    case E_WrongIPv6:
        return("wrong format in IPv6 address!");
    case E_MissingValue:
        return("no value between dots");
    case E_ExpansionOnlyOnce:
        return("::-expansion is allowed only once!");
    case E_CouldNotResolve:
        return("could not resolve hostname!");
    default:
        return("NetAddr::GetErrorMsg: unexpected error ");
    } // switch ()
    // never reached
    return ("");
} // NetAddr::pm_SetError ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/** NetAddr increment operator
 * Increment address by 1
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::operator ++
(
    int w
)
{
    SG_TRACE_START
    int     i;
    int     nResult;
    int     nOverflow;
    i = NetAddr::Size-1;
    nOverflow = 1;
    while ((nOverflow != 0) && (i>=0))
    {
        nResult = pm_nAddr[i] + nOverflow;
        nOverflow = 0;
        if (nResult > 0xff)
        {
            nOverflow = nResult - 0xff;
            nResult   = 0x00;
        }
        pm_nAddr[i] = nResult;
        i--;
    }
    SG_TRACE_END
} // NetAddr::operator ++ int
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/** NetAddr increment operator
 * Decrement address by 1
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::operator --
(
    int w
)
{
    SG_TRACE_START
    int     i;
    int     nResult;
    int     nOverflow;
    i = NetAddr::Size-1;
    nOverflow = 1;
    while ((nOverflow != 0) && (i>=0))
    {
        nResult = pm_nAddr[i] - nOverflow;
        nOverflow = 0;
        if (nResult > 0xff)
        {
            nOverflow = nResult - 0xff;
            nResult   = 0x00;
        }
        pm_nAddr[i] = nResult;
        i--;
    }
    SG_TRACE_END
} // NetAddr::operator -- int
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/** NetAddr assignment operator
 * Assign another %NetAddr.
 *
 * @param Addr the address to copy values from
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::operator =
(
    const NetAddr& Addr
)
{
    SG_TRACE_START
    for (int i=0; i<NetAddr::Size; i++)
    {
        pm_nAddr[i] = Addr.pm_nAddr[i];
    }
    pm_nType = Addr.pm_nType;
    pm_nMask = Addr.pm_nMask;
    pm_nPort = Addr.pm_nPort;
    SG_TRACE_END
} // NetAddr::operator = NetAddr
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/** NetAddr comparison OPERATOR
 * compare *this with Addr. Two addresses are equal, if all bits of
 * their address, all bits of their mask and the port are equal.
 *
 * @param Addr the NetAddr to compare to
 * @return 1 if *this is equal to @a Addr
 * @return 0 if *this is not equal to @a Addr
 */
//////////////////////////////////////////////////////////////////////
bool
NetAddr::operator ==
(
    const NetAddr& Addr
) const
{
    SG_TRACE_START
    for (int i = 0; i < NetAddr::Size; i++)
    {
        if (pm_nAddr[i] != Addr.pm_nAddr[i])
        {
            SG_TRACE_END
            return (false);
        }
    }
    if (pm_nMask != Addr.pm_nMask)
    {
        SG_TRACE_END
        return (false);
    }
    if (pm_nPort != Addr.pm_nPort)
	{
		return (false);
	}
    SG_TRACE_END
    return (true);
} // NetAddr::operator == ( const NetAddr& Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/** NetAddr comparison operator.
 * Compare *this with Addr.
 *
 * @param Addr the NetAddr to compare to
 * @return 1 if *this is not equal to @a Addr
 * @return 0 if *this is equal to @a Addr
 */
//////////////////////////////////////////////////////////////////////
bool
NetAddr::operator !=
(
    const NetAddr& Addr
) const
{
    for (int i = 0; i < NetAddr::Size; i++)
    {
        if (pm_nAddr[i] != Addr.pm_nAddr[i])
        {
            SG_TRACE_END
            return (true);
        }
    }
    if (pm_nMask != Addr.pm_nMask)
    {
        SG_TRACE_END
        return (true);
    }
    if (pm_nPort != Addr.pm_nPort)
	{
		return (true);
	}
    SG_TRACE_END
    return (false);
} // NetAddr::operator != ( const NetAddr& Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/** NetAddr assignment operator
 * Assign a string representation of an IP address.
 *
 * @param Addr a string representation of an IP address.
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::operator =
(
    const string& Addr
)
{
    SG_TRACE_START
    pm_FromString (Addr);
    SG_TRACE_END
} // NetAddr::operator = string
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * @return The number of bits for hosts of current netmask.
 */
//////////////////////////////////////////////////////////////////////
UINT
NetAddr::HostBits
() const
{
    SG_TRACE_START
    if ((pm_nType & IPv6) == 0)
    {   // must be IPv4 only
        SG_TRACE_RETURN(MaxMaskIPv4 - pm_nMask)
        return (MaxMaskIPv4 - pm_nMask);
    }
    SG_TRACE_RETURN(MaxMask - pm_nMask)
    return (MaxMask - pm_nMask);
} // HostBits()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * @return The number of hosts of current netmask.
 * only works for IPv4!
 */
//////////////////////////////////////////////////////////////////////
UINT
NetAddr::NumberOfHosts
() const
{
    SG_TRACE_START
    UINT nI = NetBits();
    UINT nM = 0;
    for (UINT i=0; i<nI; i++)
        nM |= (0x80000000 >> i);
    SG_TRACE_END
    return ((0xffffffff ^ nM) + 1);
} // HostBits()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * @return The address as umeric
 * only works for IPv4!
 */
//////////////////////////////////////////////////////////////////////
UINT
NetAddr::AddrNum
() const
{
    SG_TRACE_START
    UINT nM;
    nM  = pm_nAddr[12] << 24;
    nM |= pm_nAddr[13] << 16;
    nM |= pm_nAddr[14] << 8;
    nM |= pm_nAddr[15];
    SG_TRACE_RETURN(nM)
    return (nM);
} // HostBits()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * @return The number of bits for net of current netmask.
 */
//////////////////////////////////////////////////////////////////////
UINT
NetAddr::NetBits
() const
{
    SG_TRACE_START
    SG_TRACE_RETURN(pm_nMask)
    return (pm_nMask);
} // NetBits()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Check if @a Addr is part of *this address
 *
 * @param Addr the NetAddr to check
 * @return true if @a Addr is part of *this address
 * @return false if @a Addr is not part of *this address
 * @see NetAddr::IsPartOf
 */
//////////////////////////////////////////////////////////////////////
bool
NetAddr::Contains
(
    const NetAddr& Addr
) const
{
    SG_TRACE_START
    int      i;
    NetAddr First, Last;

    if (Addr.pm_nMask < pm_nMask)
    {
        SG_TRACE_RETURN("false")
        return (false);
    }
    First = FirstAddr();
    Last  = LastAddr();
    for (i = 0; i < NetAddr::Size; i++)
    {
        if ((Addr.pm_nAddr[i] < First.pm_nAddr[i])
        ||  (Addr.pm_nAddr[i] > Last.pm_nAddr[i]))
        {
            SG_TRACE_RETURN("false")
            return (false);
        }
    }
    SG_TRACE_RETURN("true")
    return (true);
} // NetAddr::Contains ( const NetAddr& Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Check if *this address is part of @a Addr
 *
 * @param Addr the NetAddr to check
 * @return true if @a Addr is part of *this address
 * @return false if @a Addr is not part of *this address
 * @see NetAddr::Contains
 */
//////////////////////////////////////////////////////////////////////
bool
NetAddr::IsPartOf
(
    const NetAddr& Addr
) const
{
    SG_TRACE_START
    int      i;
    NetAddr First, Last;

    if (pm_nMask < Addr.pm_nMask)
    {
        SG_TRACE_RETURN("false")
        return (false);
    }
    First = Addr.FirstAddr();
    Last  = Addr.LastAddr();
    for (i = 0; i < NetAddr::Size; i++)
    {
        if ((pm_nAddr[i] < First.pm_nAddr[i])
        ||  (pm_nAddr[i] > Last.pm_nAddr[i]))
        {
            SG_TRACE_RETURN("false")
            return (false);
        }
    }
    SG_TRACE_RETURN("true")
    return (true);
} // NetAddr::IsPartOf ( const NetAddr& Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Convert internal representation to a Posix address structure
 *
 * @return a pointer to a posix address structure (struct sockaddr)
 *         as needed by all standard C functions.
 */
//////////////////////////////////////////////////////////////////////
struct sockaddr*
NetAddr::SockAddr
() const
{
    SG_TRACE_START
    struct sockaddr*        SockAddr;
    struct sockaddr_in*     SockAddrV4;
    struct sockaddr_in6*    SockAddrV6;

    SockAddr   = (struct sockaddr*)         &pm_SockAddr;
    SockAddrV4 = (struct sockaddr_in*)      &pm_SockAddr;
    SockAddrV6 = (struct sockaddr_in6*)     &pm_SockAddr;
    memset (SockAddr, 0, sizeof (sockaddr_in6));
    SockAddrV4->sin_port = htons (pm_nPort);
    if (pm_nType == NetAddr::ANY)
    {
        SockAddrV4->sin_family = AF_INET;
        SockAddrV4->sin_addr.s_addr = INADDR_ANY;
    }
    else if (pm_nType == NetAddr::IPv4)
    {
        SockAddrV4->sin_family = AF_INET;
        memcpy (&(SockAddrV4->sin_addr), & pm_nAddr[12], 4);
    }
    else
    {
        SockAddrV6->sin6_family = AF_INET6;
        memcpy (&(SockAddrV6->sin6_addr), & pm_nAddr, 16);
    }
    SG_TRACE_END
    return (SockAddr);
} // NetAddr::SockAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Convert internal struct sockaddr pm_SockAddr to our internal
 * Format.
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::CopySockAddr
()
{
    SG_TRACE_START
    struct sockaddr*        SockAddr;

    SockAddr   = (struct sockaddr*)         &pm_SockAddr;
    for (int i=0; i<NetAddr::Size; i++)
        pm_nAddr[i] = 0;
    if (SockAddr->sa_family == AF_INET)
    {   // IPv4
        pm_nMask  = MaxMaskIPv4;  // always host address
        pm_nType  = IPv4;
        pm_nError = E_OK;
        struct sockaddr_in* SockAddrV4 = (struct sockaddr_in*) &pm_SockAddr;
        pm_nPort  = SockAddrV4->sin_port;
        memcpy (& pm_nAddr[12], & (SockAddrV4->sin_addr), 4);
    }
    else if (SockAddr->sa_family == AF_INET6)
    {   // IPv6
        pm_nMask  = MaxMask;  // always host address
        pm_nType  = IPv6;
        pm_nError = E_OK;
        struct sockaddr_in6* SockAddrV6 = (struct sockaddr_in6*) &pm_SockAddr;
        pm_nPort  = SockAddrV6->sin6_port;
        memcpy (& pm_nAddr, & (SockAddrV6->sin6_addr), 16);
    }
    SG_TRACE_END
} // NetAddr::pm_CopySockAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
uint32_t
NetAddr::GetIP
() const
{
    SG_TRACE_START
    uint32_t ret;
    memcpy (&ret, & pm_nAddr[12], 4);
    SG_TRACE_END
    return (ret);
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Return the size of our internal struct sockaddr
 *
 * @return The size of our address structure
 */
//////////////////////////////////////////////////////////////////////
socklen_t
NetAddr::AddrSize
() const
{
    SG_TRACE_START
    SG_TRACE_END
    return (sizeof (pm_SockAddr));
} // NetAddr::AddrSize ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Map an IPv4 address to an IPv4inIPv6 address.
 *
 * @return A mapped %NetAddr of *this
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::MapToV6
() const
{
    SG_TRACE_START
    NetAddr Result;

    if (((pm_nType & IPv6) > 0)
    ||  (pm_nType & IPv4) == 0)
    {   // no IPv4 address
        Result.pm_SetError (NetAddr::E_WrongIPv4);
        SG_TRACE_END
        return (Result);
    }
    Result.pm_nAddr[0] = 0x20;
    Result.pm_nAddr[1] = 0x02;
    Result.pm_nAddr[2] = pm_nAddr[12];
    Result.pm_nAddr[3] = pm_nAddr[13];
    Result.pm_nAddr[4] = pm_nAddr[14];
    Result.pm_nAddr[5] = pm_nAddr[15];
    Result.pm_nMask    = pm_nMask+16;
    Result.pm_nType    = IPv6;
    SG_TRACE_END
    return (Result);
} // NetAddr::MapToV6 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Map an IPv4inIPv6 address to an IPv4 address.
 *
 * @return A mapped %NetAddr of *this
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::MapFromV6
() const
{
    SG_TRACE_START
    NetAddr Result;

    if (((pm_nType & IPv6) == 0)
    ||  ((pm_nType & IPv4) > 0)
    ||  (pm_nAddr[0] != 0x20)
    ||  (pm_nAddr[1] != 0x02))
    {   // no IPv4 address
        SG_TRACE_END
        Result.pm_SetError (NetAddr::E_WrongIPv6);
        return (Result);
    }
    Result.pm_nAddr[12] = pm_nAddr[2];
    Result.pm_nAddr[13] = pm_nAddr[3];
    Result.pm_nAddr[14] = pm_nAddr[4];
    Result.pm_nAddr[15] = pm_nAddr[5];
    Result.pm_nMask     = pm_nMask-16;
    Result.pm_nType     = IPv4;
    SG_TRACE_END
    return (Result);
} // NetAddr::MapFromV6 ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Represent the netmask as an IP address, eg. '255.255.255.0'
 *
 * @return A %NetAddr representation of the netmask
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::MaskAddr
() const
{
    SG_TRACE_START
    NetAddr Result(*this);
    Result.pm_BuildFromMask ();
    for (int i=0; i<NetAddr::Size; i++)
    {
        Result.pm_nAddr[i] = Result.pm_nMaskAddr[i];
    }
    SG_TRACE_END
    return (Result);
} // NetAddr::MaskAddr()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Represent the netmask as an IP address, eg. '255.255.255.0'
 *
 * @return A %NetAddr representation of the netmask
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::CiscoWildcard
() const
{
    SG_TRACE_START
    NetAddr Result(*this);
    Result.pm_BuildFromMask ();
    for (int i=0; i<NetAddr::Size; i++)
    {
        Result.pm_nAddr[i] = ~Result.pm_nMaskAddr[i];
    }
    SG_TRACE_END
    return (Result);
} // NetAddr::CiscoWildcard()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Return the first address from network. If *this NetAddr is an
 * IPv4 address, the result represents the network address.
 *
 * @return The first address of *this network.
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::FirstAddr
() const
{
    SG_TRACE_START
    NetAddr Result(*this);
    UINT    nStart;

    if (pm_nMaskAddr[0] == 0)
    {
        Result.pm_BuildFromMask();
    }
    if ((pm_nType & IPv6) == 0)
    {   // must be IPv4 only
        nStart = 12;
    }
    else
    {
        nStart = 0;
    }
    for (int i=nStart; i<NetAddr::Size; i++)
    {
        Result.pm_nAddr[i] = pm_nAddr[i] & Result.pm_nMaskAddr[i];
    }
#if 0
    Result.pm_nType         = pm_nType;
    Result.pm_nMask         = pm_nMask;
    Result.pm_nError        = pm_nError;
#endif
    SG_TRACE_END
    return (Result);
} // NetAddr::FirstAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Return the first usable address from network. If *this address
 * is an IPv6 address, the first usable address is the first address
 * of *this network. For IPv4 Addresses it is the successor of the
 * netaddress.
 *
 * @return The first usable address of *this network
 * @see NetAddr::FirstAddr
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::FirstUsableAddr
() const
{
    SG_TRACE_START
    NetAddr Result(*this);

    if (((pm_nType & IPv4) > 0) && (pm_nMask == 32))
    {
        SG_TRACE_END
        return (Result);
    }
    if ((pm_nType & IPv6) > 0)
    {
        SG_TRACE_END
        return (FirstAddr ());
    }
    Result = FirstAddr ();
    Result++;
    SG_TRACE_END
    return (Result);
} // NetAddr::FirstUsableAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Return the last address of *this network. For an IPv4 address, the
 * result represents the broadcast address of *this network.
 *
 * @return The last address of *this network
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::LastAddr
() const
{
    SG_TRACE_START
    NetAddr Result(*this);
    UINT    nStart;

    if (pm_nMaskAddr[0] == 0)
    {
        Result.pm_BuildFromMask();
    }
    if ((pm_nType & IPv6) == 0)
    {   // must be IPv4 only
        nStart = 12;
    }
    else
    {
        nStart = 0;
    }
    for (int i=nStart; i<NetAddr::Size; i++)
    {
        Result.pm_nAddr[i] = pm_nAddr[i] | ~Result.pm_nMaskAddr[i];
    }
    Result.pm_nType     = pm_nType;
    Result.pm_nMask     = pm_nMask;
    Result.pm_nError    = pm_nError;
    SG_TRACE_END
    return (Result);
}  // NetAddr::LastAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Return the last usable address from network. If *this is an IPv6
 * address, the result is the last address of *this network.
 *
 * @return The last usable address of *this network.
 * @see NetAddr::LastAddr
 */
//////////////////////////////////////////////////////////////////////
NetAddr
NetAddr::LastUsableAddr
() const
{
    SG_TRACE_START
    NetAddr Result(*this);

    if (((pm_nType & IPv4) > 0) && (pm_nMask == 32))
    {
        SG_TRACE_END
        return (Result);
    }
    if ((pm_nType & IPv6) > 0)
    {
        SG_TRACE_END
        return (LastAddr ());
    }
    Result = LastAddr ();
    Result--;
    SG_TRACE_END
    return (Result);
} // NetAddr::LastUsableAddr ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Create pm_nMaskAddr from pm_nMask
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::pm_BuildFromMask
()
{
    SG_TRACE_START
    UINT i;
    UINT nBytes;
    UINT nBits;
    UINT nBitMask;
    UINT nStart;

    for (i=0; i < NetAddr::Size; i++)
    {
        pm_nMaskAddr[i] = 0;
    }
    nBits = pm_nMask;
    nBytes = (pm_nMask / 8);
    if ((pm_nType & IPv6) == 0) // must be IPv4 only
    {
        nStart = 12;
    }
    else
    {
        nStart = 0;
    }
    for (i=0; i < nBytes; i++)
    {
        pm_nMaskAddr[i+nStart] = 0xff;
        nBits -= 8;
    }
    nBytes = i;
    nBitMask = 0x80;
    for (i=0; i < nBits; i++)
    {
        pm_nMaskAddr[nBytes+nStart] |= (nBitMask >> i);
    }
    SG_TRACE_END
} // NetAddr::pm_BuildFromMask ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Convert *this address into a string representation.
 *
 * @param nFormat the format of the string representation
 * @param nBase   the base of the string representation.
 *                (Eg. Base=SEDECIMAL will result in ff.ff.ff.0)
 * @return        a string representation of *this address
 * @see ADDRESS_FORMAT
 * @see ADDRESS_BASE
 */
//////////////////////////////////////////////////////////////////////
string
NetAddr::ToString
(
    const ADDRESS_BASE nBase,
    const ADDRESS_FORMAT nFormat
) const
{
    SG_TRACE_START
    int             nNumBytes;
    int             nRealBytes;
    int             nOutputBytes;
    ADDRESS_BASE    nWantBase;
    UINT            nOctet;
    bool            bWantFill;
    bool            bDidCompression;
    bool            bDoOutput;
    string          strReturn;

    if (nBase == NetAddr::STD)
    {
        nWantBase = NetAddr::SEDECIMAL;
    }
    else
    {
        nWantBase = nBase;
    }
    nRealBytes      = 0;
    nOutputBytes    = NetAddr::Size;
    bDidCompression = false;
    if (nFormat != NetAddr::Compressed)
    {
        bDoOutput = true;
    }
    else
    {
        bDoOutput = false;
    }
    if ((pm_nType & IPv4) > 0)
    {
        nOutputBytes -= 4;
    }
    if ((pm_nType & IPv6) == 0)
    {
        nOutputBytes = 0;
    }
    nNumBytes = 0;
    while (nNumBytes < nOutputBytes)
    {   // addr is v6
        nOctet = (pm_nAddr[nNumBytes] << 8);
        nNumBytes++;
        nOctet += pm_nAddr[nNumBytes];
        nNumBytes++;
        //////////////////////////////////////////////////
        //
        //      insert leading zeros if necessary
        //
        //////////////////////////////////////////////////
        bWantFill = false;
        if ((nRealBytes > 0) || (nFormat == NetAddr::Full))
        {
            bWantFill = true;
        }
        if ((nFormat == NetAddr::Short) && (nOctet == 0))
        {
            bWantFill = false;
        }
        if ((nOctet != 0) || (bWantFill == true)
        ||  (bDoOutput == true))
        {
            strReturn += pm_WordToString (nOctet, nWantBase, bWantFill);
            nRealBytes += 2;
        }
        if (((nNumBytes) % 2) == 0)
        {
            if ((nRealBytes == 0) && (bDidCompression == false))
            {
                strReturn += ":";
                bDidCompression = true;
            }
            if ((nNumBytes < NetAddr::Size) && (nRealBytes > 0))
            {
                strReturn += ":";
                nRealBytes = 0;
            }
        }
    } // while()
    if ((pm_nType & IPv4) > 0)
    {
        //////////////////////////////////////////////////
        //
        //      output of last 4 bytes
        //
        //////////////////////////////////////////////////
        if (nBase == NetAddr::STD)
        {
            nWantBase = NetAddr::DECIMAL;
            bWantFill = false;
        }
        else
        {
            nWantBase = nBase;
            bWantFill = true;
        }
        for (int i=12; i<NetAddr::Size; i++)
        {
            strReturn += pm_ByteToString (pm_nAddr[i], nWantBase, bWantFill);
            if (i<15)   // last byte
            {
                strReturn += '.';
            }
        }
    }
    SG_TRACE_END
    return (strReturn);
} // NetAddr::pm_ToString ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Scan a given string if it is a valid IPv[4|6] address.\n
 * Calling this function we know:\n
 *      - if the address is syntactically correct\n
 *      - how many octets are in the string\n
 *      - how many octets we have to insert in an expansion (::)\n
 *      - type of address (pm_nType gets set)\n
 *
 * @param Addr  A string representation of an IP address.
 * @return      The number of octest in the string.
 */
//////////////////////////////////////////////////////////////////////
UINT
NetAddr::pm_PreScanStringAddress
(
    const string& Addr
)
{
    SG_TRACE_START
    UINT    nNumOctets;             // number of octets in string
    UINT    nNumColons;             // number of colons in string
    UINT    nNumDots;               // number of dots in string
    UINT    nLastColon;             // position of last colon
    UINT    nLastDot;               // position of last dot
    bool    bIsCompressed;          // "::" in string
    bool    bHaveMask;              // "/" in string
    UINT    i;

    pm_nType        = NetAddr::Invalid;// assume address is not valid
    nNumColons      = 0;
    nNumDots        = 0;
    nNumOctets      = 0;
    nLastColon      = 0;
    nLastDot        = 0;
    bIsCompressed   = false;
    bHaveMask       = false;
    if (Addr.size() == 0)
    {
        pm_SetError (NetAddr::E_AddressIsEmpty);
        SG_TRACE_END
        return (0);
    }
    for (i=0; i < Addr.size(); i++)
    {
        if (Addr[i] == ':')
        {
            if ((pm_nType & IPv4) > 0)
            {   // already seen dots, no more : allowed
                pm_SetError (NetAddr::E_WrongIPv6);
                SG_TRACE_END
                return (0);
            }
            pm_nType = NetAddr::IPv6;
            if ((i-nLastColon) == 1)
            {
                if (bIsCompressed)
                {   // already found ::
                    pm_SetError (NetAddr::E_ExpansionOnlyOnce);
                    SG_TRACE_END
                    return (0);
                }
                bIsCompressed = true;
            }
            else if (i>0)
            {
                nNumOctets++;
            }
            nLastColon = i;
            nLastDot = i;
            nNumColons++;
        }
        else if (Addr[i] == '.')
        {
            pm_nType = NetAddr::IPv4;
            if ((i-nLastDot) == 1)
            {
                pm_SetError (NetAddr::E_MissingValue);
                SG_TRACE_END
                return (0);
            }
            else if ((i-nLastDot) > 4)
            {
                pm_SetError (NetAddr::E_WrongIPv4);
                SG_TRACE_END
                return (0);
            }
            nLastDot = i;
            nNumDots++;
        }
        else if (Addr[i] == '/')
        {
            if (((pm_nType & IPv4) != 0) && ((i - nLastDot) > 4))
            {
                pm_SetError (NetAddr::E_WrongIPv4);
                SG_TRACE_END
                return (0);
            }
            else if (((pm_nType & IPv6)!=0) && ((i-nLastDot) > 5))
            {
                pm_SetError (NetAddr::E_WrongIPv6);
                SG_TRACE_END
                return (0);
            }
            if (i+1 == Addr.size())
            {
                pm_SetError (NetAddr::E_NetmaskMissing);
                SG_TRACE_END
                return (0);
            }
            if ((Addr[i-1] != ':') && ((pm_nType & IPv4) == 0))
            {
                nNumOctets++;
            }
            bHaveMask = true;
        }
        else if ((bHaveMask == true)
             && ((Addr[i] < '0') || (Addr[i] > '9')))
        {
            pm_SetError (NetAddr::E_NetmaskDecimal);
            SG_TRACE_END
            return (0);
        }
        else if ( ((Addr[i] < '0') || (Addr[i] > '9'))
             &&   ((Addr[i] < 'a') || (Addr[i] > 'f'))
             &&   ((Addr[i] < 'A') || (Addr[i] > 'F')))
        {
            pm_SetError (NetAddr::E_IllegalChars);
            SG_TRACE_END
            return (0);
        }
    } // for ()
    if (pm_nType == NetAddr::Invalid)
    {
        pm_SetError (NetAddr::E_WrongIPv6);
        SG_TRACE_END
        return (0);
    }
    if (((pm_nType & IPv4) != 0) && (bHaveMask == false)
    &&  ((i - nLastDot) > 3))
    {
        pm_SetError (NetAddr::E_WrongIPv4);
        SG_TRACE_END
        return (0);
    }
    else if (((pm_nType & IPv6) != 0) && (bHaveMask == false)
         &&  ((i - nLastDot) > 5))
    {
        pm_SetError (NetAddr::E_WrongIPv6);
        SG_TRACE_END
        return (0);
    }
    //////////////////////////////////////////////////
    //      if the string contained no mask
    //      the last octet was not counted
    //////////////////////////////////////////////////
    if ((bHaveMask == false) && (Addr[Addr.size() -1] != ':'))
    {
        if ((pm_nType & IPv4) == 0)
            nNumOctets++;
    }
    if ((nNumDots != 0) && (nNumDots != 3))
    {
        pm_SetError (NetAddr::E_WrongIPv4);
        SG_TRACE_END
        return (0);
    }
    if (nNumColons > 7)
    {
        pm_SetError (NetAddr::E_WrongIPv6);
        SG_TRACE_END
        return (0);
    }
    if ((nNumColons == 6) && (nNumDots != 3))
    {
        pm_SetError (NetAddr::E_WrongIPv6);
        SG_TRACE_END
        return (0);
    }
    if ((nNumOctets == 0) && (nNumDots == 0))
    {
        pm_SetError (NetAddr::E_AddressIsEmpty);
        SG_TRACE_END
        return (0);
    }
    return (nNumOctets);
} // NetAddr::pm_PreScanStringAddress ( const string& Addr, UINT& nNumOctets )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Convert a string representation of an IP address to our internal
 * Format.
 *
 * @param Addr  A string representation of an IP address
 */
//////////////////////////////////////////////////////////////////////
void
NetAddr::pm_FromString
(
    const string& Addr
)
{
    SG_TRACE_START
    AddrNumber      Val;            // value of letter
    AddrNumber      CurrentByte;    // value of current byte
    unsigned int    Current;        // pointer in Addr to current letter
    unsigned int    NumLetters;     // number of already collected letters
    unsigned int    NextColon;      // number of letters to the next colon
    unsigned int    LettersNeeded;  // number of letters to build a byte
    unsigned int    i;
    UINT            nNumOctets;
    bool            bFoundDot;
    bool            bExpanded;
    ADDRESS_BASE    nBase;

    pm_Init ();
    if (Addr == "")
    {
        pm_nType = NetAddr::ANY;
        SG_TRACE_END
        return;
    }
    //////////////////////////////////////////////////
    //
    //      check if ip is numeric
    //
    //////////////////////////////////////////////////
    // FIXME:
    int  E;
    UINT A;
    A = StrToNum<UINT> (Addr, E);
    if (E == -2)    // overflow
        E = 0;
    if (E == 0)
    {
        pm_nType = NetAddr::IPv4;
        pm_nMask = 32;
        //////////////////////////////////////////////////
        //      swap bytes on little endians
        //////////////////////////////////////////////////
        int x = 1;
        if (*((char *) &x) != 0)
        {   // little endian, swap bytes
            A = ((A>>8) & 0x00FF00FFL) | ((A<< 8) & 0xFF00FF00L);
            A = (A >> 16) | (A << 16);
        }
        char* C = (char*) &A;
        pm_nAddr[12] = C[0];
        pm_nAddr[13] = C[1];
        pm_nAddr[14] = C[2];
        pm_nAddr[15] = C[3];
        SG_TRACE_END
        return;
    }
    //////////////////////////////////////////////////
    //
    //      check string
    //
    //////////////////////////////////////////////////
    nNumOctets = pm_PreScanStringAddress (Addr);
    if (pm_nType == NetAddr::Invalid)
    {   // given string is not an IP Address
        Resolve (Addr, 0);
        if (pm_nError != E_OK)
        {   // not a hostname either so
            // make sure address is empty and invalid
            pm_Init();
            SG_TRACE_END
            return;
        }
        SG_TRACE_END
        return;
    }
    //////////////////////////////////////////////////
    //
    //      evaluate string
    //
    //////////////////////////////////////////////////
    Val             = 0;
    NumLetters      = 0;
    CurrentByte     = 0;
    nBase           = NetAddr::SEDECIMAL;
    bExpanded       = false;
    bFoundDot       = false;
    if (((pm_nType & IPv6) == false)
    &&  ((pm_nType & IPv4) == true))
    {   // IPv4 only, fill only last 4 bytes
        CurrentByte = 12;
    }
    Current = 0;
    LettersNeeded = 0;
    while (Current < Addr.size ())
    {
        //////////////////////////////////////////////////
        //
        //      check how many letters there are to
        //      the next colon (or dot)
        //
        //////////////////////////////////////////////////
        if (NumLetters == 0)
        {
            LettersNeeded = 0;
            NextColon = Current;
            while ((NextColon < Addr.size())
            && (Addr[NextColon] != ':')
            && (Addr[NextColon] != '/')
            && (Addr[NextColon] != '.'))
            {
                NextColon++;
            }
            if (Addr[NextColon] == '.')
            {
                bFoundDot = true;
                nBase = NetAddr::DECIMAL;
            }
            NextColon -= Current;
            if (bFoundDot)
            {   // eat up to dot
                LettersNeeded = NextColon;
            }
            else
            {
                switch (NextColon)
                {
                case 0:
                    LettersNeeded = (UINT)-1;
                    break;
                case 1:
                    LettersNeeded = 1;
                    break;
                case 2:
                    LettersNeeded = 2;
                    break;
                case 3:
                    LettersNeeded = 1;
                    break;
                case 4:
                    LettersNeeded = 2;
                    break;
                default:
                    pm_SetError (NetAddr::E_WrongIPv6);
                    SG_TRACE_END
                    return;
                }
            }
        }
        if (NumLetters == LettersNeeded)
        {
            if (NextColon == 3)
                LettersNeeded = 2;
            if ((!bFoundDot)
            && ((NextColon == 2) || (NextColon == 1)))
            {   // one byte missing
                pm_nAddr[CurrentByte] = 0;
                CurrentByte++;
            }
            pm_nAddr[CurrentByte] = Val;
            CurrentByte++;
            NumLetters = 0;
            Val = 0;
        }
        if (Addr[Current] == ':')
        {
            if ((NextColon == 0) && (bExpanded == false))
            {
                int NeedFill;
                bExpanded = true;
                NeedFill = (8 - nNumOctets)*2;
                if ((pm_nType & IPv4) > 0)
                    NeedFill -= 4;
                if ((pm_nType & IPv6) == 0)
                    NeedFill -= 4;
                if (NeedFill < 0)
                    NeedFill = 0; // FIXME: reached?
                if (CurrentByte+NeedFill > NetAddr::Size)
                    NeedFill = 0;
                for (i = 0; i<(UINT) NeedFill; i++)
                {
                    pm_nAddr[CurrentByte] = 0;
                    CurrentByte++;
                }
            }
        }
        else if (Addr[Current] == '.')
        {}
        else if (Addr[Current] == '/')
            break;  // found netmask
        else
        {
            Val *= nBase;
            Val += pm_cFromString (Addr[Current], nBase);
            NumLetters++;
        }
        Current++;
    } // while ()
    if (NumLetters == LettersNeeded)
    {
        if ((NumLetters == 1) && ((pm_nType & IPv4) == 0))
        {
            pm_nAddr[CurrentByte] = 0;
            CurrentByte++;
        }
        pm_nAddr[CurrentByte] = Val;
        CurrentByte++;
        NumLetters = 0;
        Val = 0;
    }
    //////////////////////////////////////////////////
    //
    //      evaluate netmask
    //
    //////////////////////////////////////////////////
    if (Addr[Current] == '/')
    {
        pm_nMask = 0;
        Current++;
        while (Current < Addr.size())
        {
            pm_nMask *= NetAddr::DECIMAL;
            pm_nMask += pm_cFromString (Addr[Current], NetAddr::DECIMAL);
            Current++;
        }
        if (((pm_nType & NetAddr::IPv6) == 0)
        &&   (pm_nMask > NetAddr::MaxMaskIPv4))
        {
            pm_SetError (NetAddr::E_OutOfRange);
            SG_TRACE_END
            return;
        }
        if ((pm_nMask > NetAddr::MaxMask) || (pm_nMask < 0))
        {
            pm_nError = NetAddr::E_OutOfRange;
            SG_TRACE_END
            return;
        }
        if (pm_nMask == 0)
        {
            pm_nError = NetAddr::E_NetmaskMissing;
            SG_TRACE_END
            return;
        }
    }
    if ((pm_nType == NetAddr::IPv4)
    &&  (pm_nMask > NetAddr::MaxMaskIPv4))
        pm_nMask = MaxMaskIPv4;
    SG_TRACE_END
} // NetAddr::pm_FromString ( const char* Addr )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Internal converter. Convert @a cLetter into a number, honours
 * @a nBase. (Eg: 'c' -> 12)
 *
 * @param cLetter       the letter (character) to convert.
 * @param nBase         the letter is in this base (eg. SEDECIMAL)
 * @return              A number representation of @a cLetter
 */
//////////////////////////////////////////////////////////////////////
AddrNumber
NetAddr::pm_cFromString
(
    AddrNumber cLetter,
    const ADDRESS_BASE nBase
) const
{
    SG_TRACE_START
    if ((nBase < NetAddr::MinBase)
    ||  (nBase > NetAddr::MaxBase))
    {
        // pm_SetError (E_OutOfRange);
        SG_TRACE_END
        return ('0');
    }
    if ((cLetter < '0') || (cLetter > '9'))
    {
        cLetter = toupper (cLetter);
        if ((cLetter < 'A') || (cLetter > 'F'))
        {
            // pm_SetError (E_IllegalChars);
            SG_TRACE_END
            return ('0');
        }
        cLetter -= ('A' - 10);
    }
    else
    {
        cLetter -= '0';
    }
    if (cLetter > nBase)
    {
        // pm_SetError (E_OutOfRange);
        SG_TRACE_END
        return ('0');
    }
    SG_TRACE_END
    return (cLetter);
} // NetAddr::pm_cFromString ( char cLetter )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Internal converter. Convert a word (integer) into a string
 * (Eg: 1212 -> 'cc'
 *
 * @param nNumber       The Number to convert.
 * @param nBase         The base to convert to (eg: SEDECIMAL).
 * @param bFill         If true, fill missing leading characters
 *                      with '0's.
 * @return              A string representation of @a nNumber
 */
//////////////////////////////////////////////////////////////////////
string
NetAddr::pm_WordToString
(
    UINT nNumber,
    const ADDRESS_BASE nBase,
    const bool bFill
) const
{
    SG_TRACE_START
    const char*     Sedecimals = "0123456789ABCDEF";
    const unsigned char NumberOfDigits[] = {16,11,8,7,7,6,6,6,5,5,5,5,5,5,4};
    string          strReturn;

    if ((nBase < NetAddr::MinBase)
    ||  (nBase > NetAddr::MaxBase))
    {
        // pm_SetError (E_OutOfRange);
        SG_TRACE_END
        return ("0");
    }
    if (nNumber == 0)
    {
        if (! bFill)
        {
            SG_TRACE_END
            return ("0");
        }
        for (nNumber=0; nNumber<NumberOfDigits[nBase-2]; nNumber++)
            strReturn = "0" + strReturn;
        SG_TRACE_END
        return (strReturn);
    }
    while (nNumber > 0)
    {
        strReturn = Sedecimals [nNumber % nBase] + strReturn;
        nNumber /= nBase;
    }
    if (bFill)
    {
        nNumber = NumberOfDigits[nBase-2] - strReturn.size();
        while (nNumber > 0)
        {
            strReturn = "0" + strReturn;
            nNumber--;
        }
    }
    SG_TRACE_END
    return (strReturn);
} // NetAddr::pm_ByteToString ( char cNumber )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/**
 * Internal converter. Convert a byte into a string representation.
 * (Eg: 12 -> "12")
 *
 * @param nNumber       The byte to convert.
 * @param nBase         The base the number will be represented
 * @param bFill         Fill missing leading characters with '0's.
 * @return              A string representation of @a nNumber
 */
//////////////////////////////////////////////////////////////////////
string
NetAddr::pm_ByteToString
(
    AddrNumber nNumber,
    const ADDRESS_BASE nBase,
    const bool bFill
) const
{
    const char*     Sedecimals = "0123456789ABCDEF";
    const char      NumberOfDigits[] = {8,6,4,4,4,3,3,3,3,3,3,3,3,3,2};
    string          strReturn;

    if ((nBase < NetAddr::MinBase) ||  (nBase > NetAddr::MaxBase))
    {
        // pm_SetError (E_OutOfRange);
        SG_TRACE_END
        return ("0");
    }
    if (nNumber == 0)
    {
        if (! bFill)
        {
            SG_TRACE_END
            return ("0");
        }
        for (nNumber=0; nNumber<NumberOfDigits[nBase-2]; nNumber++)
            strReturn = "0" + strReturn;
        SG_TRACE_END
        return (strReturn);
    }
    while (nNumber > 0)
    {
        strReturn = Sedecimals [nNumber % nBase] + strReturn;
        nNumber /= nBase;
    }
    if (bFill)
    {
        nNumber = NumberOfDigits[nBase-2] - strReturn.size();
        while (nNumber > 0)
        {
            strReturn = "0" + strReturn;
            nNumber--;
        }
    }
    SG_TRACE_END
    return (strReturn);
} // NetAddr::pm_ByteToString ( char cNumber )
//////////////////////////////////////////////////////////////////////

/** NetAddr output operator
 */
std::ostream&
operator <<
(
	std::ostream& o,
	const NetAddr& Addr
)
{
	o << Addr.ToString();
	if ( Addr.pm_nPort != 0 )
		o << ":" << Addr.pm_nPort;
	return ( o );
} // ostream& operator << ( NetAddr )

