/// @file fg_proto.cxx
/// A class for handling the multiplayer protocol
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2006-2015
/// @copyright	GPLv3
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#include <fglib/fg_typcnvt.hxx>
#include "fg_proto.hxx"

const char *ClientTypeToString[] =
{
	"NOCLIENT",
	"FGFS",
	"FGMS",
	"FGAS",
	"FGLS",
	"OBSERVER",
};

//////////////////////////////////////////////////////////////////////

ProtocolHandler::ProtocolHandler
()
{
	m_SetSenderID = false;
}

//////////////////////////////////////////////////////////////////////

fgms::eRETURNVALS
ProtocolHandler::ReadConfig
(
	const string& Filename,
	SGPropertyNode& m_Properties
)
{
	try
	{
		readProperties ( Filename, &m_Properties );
	}
	catch ( sg_io_exception& ex )
	{
		cerr << "Could not read configuration '" << Filename << "' : "
		  << ex.getMessage() << endl;
		return ( fgms::FAILED );
	}
	if ( ! m_Properties.hasValue ( "/multiplayer/system/protospec" ) )
	{
		cerr << "no /multiplayer/system/protospec defined in config"
		     << endl;
		return ( fgms::FAILED );
	}
	string ProtoSpec = m_Properties.getStringValue (
	  "/multiplayer/system/protospec" );
	if (! ReadProtoSpec (ProtoSpec, m_Properties ) )
	{
		return ( fgms::FAILED );
	}
	return ( fgms::SUCCESS );
} //  ProtocolHandler::ReadConfig ()

//////////////////////////////////////////////////////////////////////

/**
 * Read the protocol specification (normally called mp-proto-spec.xml)
 * and build m_ID2Prop and m_Prop2ID
 * @param Filename	the file to read
 * @param m_Properties	properties of the reader (fgms, fgfs)
 * @param OneTimeProperties	if not NULL we also fill the OTPS
 * @param ToSendProperties	filled in with properties which have
 * 				'sendovermp' attribute. if OneTimeProperties
 * 				is provided, ToSendProperties must be provided,
 * 				too
 * @return fgms::SUCCESS
 * @return fgms::FAILED
 */
fgms::eRETURNVALS
ProtocolHandler::ReadProtoSpec
(
	const string& Filename,
	const SGPropertyNode& m_Properties
)
{
	SGPropertyNode MPProperties;
	try
	{
		readProperties ( Filename, &MPProperties );
	}
	catch ( sg_io_exception& ex )
	{
		cerr << "Could not read protocol specification '"
		     << Filename << "' : "
		     << ex.getMessage() << endl;
		return ( fgms::FAILED );
	}
	// check for compatible protocol version
	if ( ! MPProperties.hasValue ( "/multiplayer/system/minproto/major" ) )
	{
		cerr << "no /multiplayer/minproto/system/major defined in "
		     << "protocol specification!" << endl;
		return ( fgms::FAILED );
	}
	if ( ! MPProperties.hasValue ( "/multiplayer/system/minproto/minor" ) )
	{
		cerr << "no /multiplayer/system/minproto/minor defined in "
		     << "protocol specification!" << endl;
		return ( fgms::FAILED );
	}
	if ( ! MPProperties.hasValue("/multiplayer/system/protoversion/major"))
	{
		cerr << "no /multiplayer/system/protoversion/major defined in "
		     << "protocol specification!" << endl;
		return ( fgms::FAILED );
	}
	if ( ! MPProperties.hasValue("/multiplayer/system/protoversion/minor"))
	{
		cerr << "no /multiplayer/system/protoversion/minor defined in "
		     << "protocol specification!" << endl;
		return ( fgms::FAILED );
	}
	if ( ! MPProperties.hasValue ( "/multiplayer/system/resendtime" ) )
	{
		cerr << "no /multiplayer/system/resendtime defined in "
		     << "protocol specification!" << endl;
		return ( fgms::FAILED );
	}
	if ( ! MPProperties.hasValue ( "/multiplayer/system/maxretries" ) )
	{
		cerr << "no /multiplayer/system/maxretries defined in "
		     << "protocol specification!" << endl;
		return ( fgms::FAILED );
	}
	int min_major, min_minor, my_major, my_minor, my_proto;
	min_major = MPProperties.getIntValue (
	  "/multiplayer/system/minproto/major" );
	min_minor = MPProperties.getIntValue (
	  "/multiplayer/system/minproto/minor" );
	my_major  = MPProperties.getIntValue (
	  "/multiplayer/system/protoversion/major" );
	my_minor  = MPProperties.getIntValue (
	  "/multiplayer/system/protoversion/minor" );
	if ( min_major > my_major )
	{
		cerr << "protocol specification is too new!" << endl;
		cerr << "we use v" << my_major << "." << my_minor
			<< " but v" << min_major << "." << min_minor
			<< " is required by protocol specification!" << endl;
		exit ( fgms::FAILED );
	}
	my_proto  = my_major << 16;
	my_proto += my_minor;
	m_Peer.SetMagic ("FGFS");
	m_Peer.EnableAutomaticHeaders ();
	m_Peer.SetProtocolVersion (my_proto);
	m_Peer.SetResendTime (
	  MPProperties.getIntValue ( "/multiplayer/system/resendtime" ) );
	m_Peer.SetMaxRetries (
	  MPProperties.getIntValue ( "/multiplayer/system/maxretries" ) );
	// now step through the complete property-map
	// and build our internal maps
	vector<SGPropertyNode_ptr> Map;
	vector<SGPropertyNode_ptr>::iterator i;
	SGPropertyNode_ptr Node;
	const string ProtoPath = "/multiplayer/propertymap/";
	string Path;
	uint32_t ID;
	uint32_t Attr;
	Node = MPProperties.getNode ( "multiplayer/propertymap" );
	if ( Node == NULL )
	{
		SG_LOG ( SG_IO, SG_DEBUG,
		  "ReadProtoSpec: no property specification found!" );
		return ( fgms::FAILED );
	}
	Map = Node->getChildren ( "prop" );
	// step through every node
	for ( i = Map.begin(); i != Map.end(); i++ )
	{
		Path = ( *i )->getStringValue ( "path" );
		ID   = ( *i )->getIntValue ( "id" );
		m_ID2Prop[ID]	= Path;
		m_Prop2ID[Path]	= ID;
		Attr = ( *i )->getAttributes ();
		// check if the property is provided
		Node = (SGPropertyNode*) m_Properties.getNode ( Path.c_str() );
		if ( Node == 0 )
		{
			Node = new SGPropertyNode;
		}
		Node->setAttributes ( Attr );
		if ( Attr & SGPropertyNode::SEND_OVER_MP_ONCE )
		{	
			m_OneTimeProperties.push_back ( ID );
		}
		else if ( Attr & SGPropertyNode::SEND_OVER_MP_ONCHANGE )
		{	
			m_OnChangeProperties.push_back ( ID );
		}
		else if ( Attr & SGPropertyNode::SEND_OVER_MP_ALWAYS )
		{
			m_ToSendProperties.push_back ( ID );
		}
	}
	return ( fgms::SUCCESS );
} // ProtocolHandler::ReadProtoSpec ()

//////////////////////////////////////////////////////////////////////

void
ProtocolHandler::m_ReadSpecial
(
	uint32_t ID,
	UDP_Peer& Peer,
	NetPacket* Msg,
	fgms::t_PropertyList& ReceivedProperties
)
{
	uint32_t val32;

	switch ( ID )
	{
	case fgms::REGISTER_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read REGISTER_SERVER" );
		break;
	case fgms::REGISTER_FGFS:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read REGISTER_FGFS" );
		break;
	case fgms::SERVER_QUIT:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read SERVER_QUIT" );
		break;
	case fgms::FGFS_QUIT:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read FGFS_QUIT" );
		break;
	case fgms::REGISTER_OK:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read REGISTER_OK" );
		break;
	case fgms::GENERAL_ERROR:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read GENERAL_ERROR" );
		break;
	case fgms::AUTH_CLIENT:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read AUTH_CLIENT" );
		break;
	case fgms::AUTH_OK:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read AUTH_OK" );
		break;
	case fgms::AUTH_FAILED:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read AUTH_FAILED" );
		break;
	case fgms::AUTHENTICATED:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read AUTHENTICATED" );
		break;
	case fgms::USE_FGAS:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read USE_FGAS" );
		break;
	case fgms::NO_FGAS:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read NO_FGAS" );
		break;
	case fgms::YOU_ARE_HUB:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read YOU_ARE_HUB" );
		break;
	case fgms::YOU_ARE_LEAVE:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read YOU_ARE_LEAVE" );
		break;
	case fgms::NEW_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read NEW_SERVER" );
		break;
	case fgms::REQUEST_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read REQUEST_SERVER" );
		break;
	case fgms::USE_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read USE_SERVER" );
		break;
	case fgms::REGISTER_AGAIN:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read REGISTER_AGAIN" );
		break;
	case fgms::SET_CLIENT_ID:
		SG_LOG ( SG_IO, SG_DEBUG, "    Read SET_CLIENT_ID" );
		// we receive a new client ID for us, but have not yet
		// read the property from the packet. So we flag here
		// that we have to set the ID after the packet was
		// processed completely
		m_SetSenderID = true;
		break;
	case fgms::PING:
		val32 = Msg->Read_uint32 ();
		SG_LOG ( SG_IO, SG_DEBUG, "    Read PING" );
		SG_LOG ( SG_IO, SG_DEBUG, "    Read TS: " << val32 );
		Peer.Write_uint32 ( fgms::PONG );
		Peer.Write_uint32 ( val32 );
		Peer.Send ();
		SG_LOG ( SG_IO, SG_DEBUG, "    Write PONG" );
		break;
	case fgms::PONG:
		val32 = Msg->Read_uint32 ();
		m_Properties.setIntValue ( "/multiplayer/system/pong_timestamp", val32 );
		SG_LOG ( SG_IO, SG_DEBUG, "    Read PONG" );
		SG_LOG ( SG_IO, SG_DEBUG, "    Read TS: " << val32);
		break;
	default:
		cerr << "ProtocolHandler::m_ReadSpecial() : unknown ID "
		     << ID << endl;
	}
	ReceivedProperties.push_back ( ID );
} // ProtocolHandler::m_ReadSpecial ()

//////////////////////////////////////////////////////////////////////

void
ProtocolHandler::m_WriteSpecial
(
	uint32_t  ID,
	UDP_Peer& Peer,
	UDP_Peer::eNETPACKET_FLAGS Flags
)
{
	time_t now;

	Peer.Write_uint32 (ID, Flags);
	switch ( ID )
	{
	case fgms::REGISTER_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write REGISTER_SERVER" );
		break;
	case fgms::REGISTER_FGFS:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write REGISTER_FGFS" );
		break;
	case fgms::SERVER_QUIT:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write SERVER_QUIT" );
		break;
	case fgms::FGFS_QUIT:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write FGFS_QUIT" );
		break;
	case fgms::REGISTER_OK:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write REGISTER_OK" );
		break;
	case fgms::GENERAL_ERROR:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write GENERAL_ERROR" );
		break;
	case fgms::YOU_ARE_HUB:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write YOU_ARE_HUB" );
		break;
	case fgms::YOU_ARE_LEAVE:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write YOU_ARE_LEAVE" );
		break;
	case fgms::NEW_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write NEW_SERVER" );
		break;
	case fgms::SET_CLIENT_ID:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write SET_CLIENT_ID" );
		break;
	case fgms::AUTH_CLIENT:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write AUTH_CLIENT" );
		break;
	case fgms::AUTH_OK:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write AUTH_OK" );
		break;
	case fgms::AUTH_FAILED:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write AUTH_FAILED" );
		break;
	case fgms::AUTHENTICATED:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write AUTHENTICATED" );
		break;
	case fgms::USE_FGAS:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write USE_FGAS" );
		break;
	case fgms::NO_FGAS:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write NO_FGAS" );
		break;
	case fgms::REQUEST_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write REQUEST_SERVER" );
		break;
	case fgms::USE_SERVER:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write USE_SERVER" );
		break;
	case fgms::REGISTER_AGAIN:
		SG_LOG ( SG_IO, SG_DEBUG, "    Write REGISTER_AGAIN" );
		break;
	case fgms::PING:
		now = time ( 0 );
		Peer.Write_uint32 (now, Flags);
		SG_LOG ( SG_IO, SG_DEBUG, "    Write PING" );
		SG_LOG ( SG_IO, SG_DEBUG, "    Write TS: " << now);
		break;
	case fgms::PONG:
		// Peer.Write_uint32 (m_Value, Flags);
		SG_LOG ( SG_IO, SG_DEBUG, "    Write PONG: " << ID);
		// SG_LOG ( SG_IO, SG_DEBUG, "    Write TS: " << m_Value);
		break;
	default:
		cerr << "ProtocolHandler::m_WriteSpecial() : unknown ID "
		     << ID << endl;
	}
} // ProtocolHandler::m_WriteSpecial ()

//////////////////////////////////////////////////////////////////////

fgms::eRETURNVALS
ProtocolHandler::ParsePacket
(
	t_ReceivePacket* Packet,
	UDP_Peer& Peer,
	fgms::t_PropertyList& ReceivedProperties,
	SGPropertyNode&  Properties
) throw ( sg_exception )
{
	Peer.SetTarget ( Packet->Sender ); // for PING replies
	ReceivedProperties.clear ();
	if ( Packet == NULL )
	{
		throw new sg_exception ( "Packet is null!",
			"ProtocolHandler::ParsePacket()" );
	}
	SG_LOG ( SG_IO, SG_DEBUG, "----> message received from "
	  << Packet->Sender << " " << Packet->Buffer->BytesUsed() << " bytes." );
	SG_LOG ( SG_IO, SG_DEBUG, "  SenderID   : " << Packet->SenderID );
	SG_LOG ( SG_IO, SG_DEBUG, "  Clienttype : "
	  << ClientTypeToString[ Packet->ClientType ]
	  << " (" << Packet->ClientType << ")" );
	if ( Packet->Flags & UDP_Peer::RELIABLE )
	{
		SG_LOG ( SG_IO, SG_DEBUG, "  packet is reliable" );
	}
	else
	{
		SG_LOG ( SG_IO, SG_DEBUG, "  packet is unreliable" );
	}
	NetPacket* Msg = Packet->Buffer;
	try
	{
		while ( Msg->RemainingData() > 0 )
		{
			uint32_t ID;
			const char* Path;
			UDP_Peer::eDATATYPES Type;
			ID   = Msg->Read_uint32();
			Path = m_ID2Prop [ID].c_str ();
			Type = ( UDP_Peer::eDATATYPES ) ( ID / 100000 );
			SG_LOG ( SG_IO, SG_DEBUG, "  read ID " << ID
			  << " (Type " << Type << ") "
			  << "Path: '" << Path << "'");
			string  val_s;
			uint32_t Size;
			switch ( Type )
			{
			case UDP_Peer::INTERNAL:
				SG_LOG ( SG_IO, SG_DEBUG,
				  "  Type INTERNAL Val: " << ID );
				m_ReadSpecial ( ID, Peer, Msg, ReceivedProperties );
				break;
			case UDP_Peer::INT8:
				Properties.setIntValue (
				  Path, Msg->Read_int8 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type INT8 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::UINT8:
				Properties.setIntValue (
				  Path, Msg->Read_uint8 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type UINT8 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::INT16:
				Properties.setIntValue (
				  Path, Msg->Read_int16 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type INT16 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::UINT16:
				Properties.setIntValue (
				  Path, Msg->Read_uint16 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type UINT16 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::INT32:
				Properties.setIntValue (
				  Path, Msg->Read_int32 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type INT32 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::UINT32:
				Properties.setIntValue (
				  Path, Msg->Read_uint32 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type UINT32 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::INT64:
				Properties.setIntValue (
				  Path, Msg->Read_int64 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type INT64 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::UINT64:
				Properties.setIntValue (
				  Path, Msg->Read_uint64 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type UINT64 Val: "
				  << Properties.getIntValue (Path) );
				break;
			case UDP_Peer::FLOAT:
				Properties.setFloatValue (
				  Path, Msg->Read_float () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type FLOAT Val: "
				  << Properties.getFloatValue (Path) );
				break;
			case UDP_Peer::DOUBLE:
				Properties.setDoubleValue (
				  Path, Msg->Read_double () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type DOUBLE Val: "
				  << Properties.getDoubleValue (Path) );
				break;
			case UDP_Peer::STRING:
				val_s = Msg->ReadString ();
				Properties.setStringValue (
				  Path, val_s.c_str () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type String Val: "
				  << Properties.getStringValue (Path) );
				break;
			case UDP_Peer::dt_OPAQUE:
				Size = Msg->Read_uint32 ();
				Msg->Skip (Size);
				SG_LOG ( SG_IO, SG_DEBUG, "  Type OPAQUE");
				break;
			case UDP_Peer::BOOL:
				Properties.setBoolValue (
				  Path, Msg->Read_uint32 () );
				SG_LOG ( SG_IO, SG_DEBUG, "  Type BOOL Val: "
				  << Properties.getBoolValue (Path) );
				break;
			default:
				SG_LOG ( SG_IO, SG_DEBUG, "  unsupported ID!");
				return ( fgms::FAILED );
			}
		} // while
	} // try
	catch (sg_exception & ex)
	{
		SG_LOG ( SG_IO, SG_DEBUG, "  malformed data in packet");
		return ( fgms::FAILED );
	}
	if ( m_SetSenderID == true )
	{
		SetSenderID ();
	}
	return ( fgms::SUCCESS );
} // ProtocolHandler::ParsePacket ()

//////////////////////////////////////////////////////////////////////

fgms::eRETURNVALS
ProtocolHandler::m_Skip
(
	uint32_t Type,
	NetPacket* Msg
)
{
	uint32_t Size;
	try
	{
		switch ( Type )
		{
		case UDP_Peer::INTERNAL:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INTERNAL");
			// until now, internal data is always 32-bit int
			Msg->Read_uint32 ();
			break;
		case UDP_Peer::INT8:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT8");
			Msg->Read_int8 ();
			break;
		case UDP_Peer::UINT8:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type UINT8");
			Msg->Read_int8 ();
			break;
		case UDP_Peer::INT16:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT16");
			Msg->Read_int16 ();
			break;
		case UDP_Peer::UINT16:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT16");
			Msg->Read_int16 ();
			break;
		case UDP_Peer::INT32:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT32");
			Msg->Read_int32 ();
			break;
		case UDP_Peer::UINT32:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT32");
			Msg->Read_int32 ();
			break;
		case UDP_Peer::INT64:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT64");
			Msg->Read_int64 ();
			break;
		case UDP_Peer::UINT64:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type INT64");
			Msg->Read_int64 ();
			break;
		case UDP_Peer::FLOAT:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type FLOAT");
			Msg->Read_float ();
			break;
		case UDP_Peer::DOUBLE:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type DOUBLE");
			Msg->Read_double ();
			break;
		case UDP_Peer::STRING:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type String");
			Size = Msg->Read_uint32 ();
			Msg->Skip (Size);
			break;
		case UDP_Peer::dt_OPAQUE:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type OPAQUE");
			Size = Msg->Read_uint32 ();
			Msg->Skip (Size);
			break;
		case UDP_Peer::BOOL:
			SG_LOG ( SG_IO, SG_DEBUG, "  Type BOOL");
			Msg->Read_int32 ();
			break;
		default:
			SG_LOG ( SG_IO, SG_DEBUG, "  unsupported ID!");
			return ( fgms::FAILED );
		}
	}
	catch (sg_exception & ex)
	{
		SG_LOG ( SG_IO, SG_DEBUG, "  malformed data in packet");
		return ( fgms::FAILED );
	}
	return ( fgms::SUCCESS );
} // ProtocolHandler::Skip ()

//////////////////////////////////////////////////////////////////////

fgms::eRETURNVALS
ProtocolHandler::WriteProps
(
	UDP_Peer& Peer,
	const SGPropertyNode& m_Properties,
	fgms::t_PropertyList& SendProps,
	const UDP_Peer::eNETPACKET_FLAGS Flags
)
{
	uint32_t ID;
	const char*		Path;
	SGPropertyNode*		Node;
	UDP_Peer::eDATATYPES	Type;
	NetAddr			Target;
	fgms::t_PropertyList::iterator i;

	Target = Peer.GetTarget ();
	SG_LOG ( SG_IO, SG_DEBUG, "<---- writing message to " << Target );
	if ( Flags & UDP_Peer::RELIABLE )
	{
		SG_LOG ( SG_IO, SG_DEBUG, "  packet is reliable" );
	}
	else
	{
		SG_LOG ( SG_IO, SG_DEBUG, "  packet is unreliable" );
	}
	SG_LOG ( SG_IO, SG_DEBUG, "  SenderID   : " << Peer.GetSenderID() );
	SG_LOG ( SG_IO, SG_DEBUG, "  Clienttype : "
	  << ClientTypeToString[ Peer.GetClientType() ]
	  << " (" << Peer.GetClientType() << ")" );
	for ( i = SendProps.begin(); i!= SendProps.end(); i++ )
	{
		ID   = ( *i );
		Path = m_ID2Prop[ID].c_str ();
		Node = (SGPropertyNode*) m_Properties.getNode ( Path );
		Type = ( UDP_Peer::eDATATYPES ) ( ID / 100000 );
		if (( Node == NULL ) && ( Type != UDP_Peer::INTERNAL ) )
		{
			SG_LOG ( SG_IO, SG_BULK,
			  "WriteProps: I do not provide property '"
			  << ID << "'" );
			continue;
		}
		SG_LOG ( SG_IO, SG_DEBUG, "  write ID " << ID
		  << " (Type " << Type << ") "
		  << "Path: '" << Path << "'");
		int32_t val32;
		int64_t val64;
		float   val_f;
		double  val_d;
		string  val_s;
		switch ( Type )
		{
		case UDP_Peer::INTERNAL:
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type INTERNAL Val: " << ID);
			m_WriteSpecial ( ID, Peer, Flags );
			break;
		case UDP_Peer::INT8:
		case UDP_Peer::UINT8:
		case UDP_Peer::INT16:
		case UDP_Peer::UINT16:
		case UDP_Peer::INT32:
		case UDP_Peer::UINT32:
			val32 = Node->getIntValue ();
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type INTXX Val: " << val32);
			Peer.WriteID1_int32 (ID, val32, Flags);
			break;
		case UDP_Peer::INT64:
		case UDP_Peer::UINT64:
			val64 = Node->getIntValue ();
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type INT64 Val: " << val64);
			Peer.WriteID1_int64 (ID, val64, Flags);
			break;
		case UDP_Peer::FLOAT:
			val_f = Node->getFloatValue ();
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type FLOAT Val: " << val_f);
			Peer.WriteID1_float (ID, val_f, Flags);
			break;
		case UDP_Peer::DOUBLE:
			val_d = Node->getDoubleValue ();
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type DOUBLE Val: " << val_d);
			Peer.WriteID1_double (ID, val_d, Flags);
			break;
		case UDP_Peer::STRING:
			val_s = Node->getStringValue ();
			Peer.WriteString (ID, val_s, Flags);
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type STRING Val: " << val_s);
			break;
		case UDP_Peer::dt_OPAQUE:
			SG_LOG ( SG_IO, SG_DEBUG, "    Type OPAQUE!");
			break;
		case UDP_Peer::BOOL:
			val32 = Node->getIntValue ();
			SG_LOG ( SG_IO, SG_DEBUG,
			  "    Type BOOL Val: " << val32);
			Peer.WriteID1_int32 (ID, val32, Flags);
			break;
		default:
			SG_LOG ( SG_IO, SG_DEBUG,
			  "WriteProps: Unsupported property-type: '"
			  << ID << "'" );
			break;
		}
	} // for ()
	return ( fgms::SUCCESS );
} // ProtocolHandler::WriteProps ()

//////////////////////////////////////////////////////////////////////

void
ProtocolHandler::SetSenderID
()
{
	uint64_t ID;
	int	 E;
	
	ID = StrToNum<uint64_t>  (
	  m_Properties.getStringValue ( "/multiplayer/registration/client_id" ),
	  E
	);
	if ( E )
	{
		SG_LOG ( SG_IO, SG_DEBUG,
		  "ProtocolHandler::SetSenderID: illegal ID: "
		  << ID << "'" );
		return;
	}
	m_Peer.SetSenderID ( ID );
	m_SetSenderID = false;
} // ProtocolHandler::SetSenderID()

//////////////////////////////////////////////////////////////////////

