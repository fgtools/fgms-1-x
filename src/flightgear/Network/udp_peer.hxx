// udp_peer.hxx - manage packets for server/clients
//
// This file is part of fgms
//
// Copyright (C) 2006  Oliver Schroeder
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see
// <http://www.gnu.org/licenses/>.
//

#ifndef NETPEER_H
#define NETPEER_H

#include <time.h>
#include <list>
#include <map>
#include <Network/netsocket.hxx>
#include <Network/netpacket.hxx>
#include <Network/netpacketpool.hxx>
#include <Network/checksum.hxx>

namespace fgms
{

typedef enum
{
	FAILED,
	SUCCESS,
} eRETURNVALS;

/// client types
typedef enum
{       
	NOCLIENT,       // unset
	FGFS,           // a normal client
	FGMS,           // FlightGear Multiplayer Server
	FGAS,           // FlightGear Authentication Service
	FGLS,           // FlightGear List Server
	OBSERVER        // an observer
} eCLIENTTYPES;

}; // namespace fgms

/** Type of received packets
 */
class t_ReceivePacket
{
public:
	NetPacket*  Buffer;
	NetAddr     Sender;
	uint32_t    Magic;
	uint32_t    Flags;
	uint32_t    MessageCounter;
	uint64_t    SenderID;
	bool        HasErrors;
	string      ErrorMesg;
	uint32_t    ClientType;
};

/** Base class for UDP based network peers
 */
class UDP_Peer
{
public:
	/// some constants
	enum eCONSTANTS
	{
		MAX_PACKET_SIZE = 1400,   ///< maximum size of UDP packets
	};
	/// flags which can be written to packets
	enum eNETPACKET_FLAGS
	{
		RELIABLE    = 1,    ///< request a receiption confirmation
		UNRELIABLE  = 2     ///< fire and forget
		// the next flag is 4
	};
	/// data types
	enum eDATATYPES
	{
		INTERNAL = 0,
		INT8,
		UINT8,
		INT16,
		UINT16,
		INT32,
		UINT32,
		FLOAT,
		BOOL,
		DOUBLE,
		INT64,
		UINT64,
		STRING,
		dt_OPAQUE
	};
	/// network error
	enum eNETERROR
	{
		ne_ERROR	= -1,
		ne_TIMEOUT	= -2
	};
	/// constructor
	UDP_Peer ();
	/// constructor with socket
	UDP_Peer ( const NetSocket& Socket );
	/// constructor with IP/Hostname and Port
	UDP_Peer ( const string& HostIP, const uint32_t Port );
	/// destructor
	virtual ~UDP_Peer ();
	/// clone a udp_peer
	void Clone ( const UDP_Peer Peer );
	/// Listen on HostIP on Port
	bool Listen ( const string& HostIP, const uint32_t Port );
	/// Wait for clients
	int WaitForClients ( int Timeout = 0 );
	/// Set default values (called by constructors)
	void SetDefaults ();
	/// Set a socket for this peer.
	/// @param Socket
	///   The socket is expected to be ready to use
	virtual void SetSocket ( const NetSocket& Socket );
	void SetSocket ( const UDP_Peer& Peer );
	/// Set the magic number (part of the automatic header)
	/// @param Magic
	///   a 32 bit value to quickly decide if the packet really
	///   is for us.
	inline virtual void SetMagic ( uint32_t Magic )
	{
		m_Magic = Magic;
	};
	/// Set the Magic to the first 4 chars of Magic
	void SetMagic ( const char* Magic )
	{
		char* tmp = (char*) &m_Magic;
#if (__BYTE_ORDER == __LITTLE_ENDIAN)
		tmp[0] = Magic[3];
		tmp[1] = Magic[2];
		tmp[2] = Magic[1];
		tmp[3] = Magic[0];
#else
		tmp[0] = Magic[0];
		tmp[1] = Magic[1];
		tmp[2] = Magic[2];
		tmp[3] = Magic[3];
#endif
	};
	/// Set the protocol version (part of the automatic header)
	/// @param ProtocolVersion
	///   a 32 bit value to identify the used protocol version
	inline virtual void SetProtocolVersion ( uint32_t ProtocolVersion )
	{
		m_ProtocolVersion = ProtocolVersion;
	};
	/// Set the client type (part of the automatic header)
	/// @param ClientType
	///   identify the type of the client
	/// @see mpmessages.hxx
	inline virtual void SetClientType ( fgms::eCLIENTTYPES ClientType )
	{
		m_ClientType = ClientType;
	};
	/// only valid with enabled automatic headers
	inline virtual fgms::eCLIENTTYPES GetClientType () const
	{
		return ( m_ClientType );
	};
	/// Set the client ID (part of the automatic header)
	/// @param SenderID
	///   unique client ID (server generated)
	inline virtual void SetSenderID ( uint64_t SenderID )
	{
		m_SenderID = SenderID;
	};
	/// en-/disable packet encryption
	void EnableCrypt ( const string & Password );
	void DisableCrypt ();
	/// @return the unique id of this sender
	/// only valid with enabled automatic headers
	inline virtual uint64_t GetSenderID () const
	{
		return ( m_SenderID );
	};
	/// enable automatic headers
	void EnableAutomaticHeaders ();
	/// disable automatic headers
	void DisableAutomaticHeaders ();
	/// Send all packets to over the socket. Resend all RELIABLE
	/// packets for which we did not yet receive an acknoledgement.
	virtual void Send ();
	/// Add a buffer to the send queue.
	/// @param Buffer
	///   the buffer to send
	void AddBuffer ( NetPacket* Buffer );
	/// Set MaxRetries. This sender will retry to send a RELIABLE
	/// packet over the socket until MaxRetries is reached or an
	/// acknowledgement is received.
	/// only valid with enabled automatic headers
	inline void SetMaxRetries ( const uint32_t MaxRetries )
	{
		m_MaxRetries = MaxRetries;
	};
	/// Set the resend interval in which RELIABLE packets will be resent
	/// if no acknowledgment was received.
	/// @TODO currently @ResendTime is measured in seconds, but should
	///       really be measured as a fractal of a second. to be implemented
	/// only valid with enabled automatic headers
	inline void SetResendTime ( const time_t ResendTime )
	{
		m_ResendTime = ResendTime;
	};
	/// return the resend interval
	inline uint32_t GetResendTime ()
	{
		return ( m_ResendTime );
	};

	/// Set host and port we want to send packets to.
	/// @param TargetHost
	///   hostname or IP we want to send to
	/// @param Port
	///   port to send to
	void SetTarget ( const string& TargetHost, int Port );
	/// Set host and port we want to send packets to.
	/// @param Addr a NetAddr representing the target host
	void SetTarget ( const NetAddr& Addr );
	/// get the target as a NetAddr
	inline const NetAddr GetTarget ()
	{
		return m_Target;
	}
	/// Inform this sender, that an acknowledgment was received. Since this
	/// class is a sender, it can not receive acknowledgments itself.
	/// It must be notified by another class.
	/// only valid with enabled automatic headers
	void GotAck ( const uint32_t PacketID );
	/// check if m_CurrentPacket contains any data
	bool PacketHasData () const;
	/// Receive packets
	virtual void Receive ();
	/** determine if there are still packets in the queue
	 *
	 * @return true if this receiver has still packets to be read
	 * @code
	 * Receiver.Receive();
	 * while (Receiver.HasPackets())
	 * {
	 *    Packet = Receiver.NextPacket();
	 *    DoSomthingWith (Packet);
	 * }
	 * @endcode
	 * @see NextPacket
	 * @see Receive
	 */
	bool        HasPackets () const;
	/// @return the next packet in the receive queue, or
	/// @return 0, if there are no more packets in the queue
	t_ReceivePacket*  NextPacket () throw ( sg_exception );
	/// recycle m_CurrentPacket
	void RecycleBuffer ( NetPacket* NetBuffer );
	/// @return the address of the sender
	NetAddr     GetSender () const;
	/// @return the flags of the current packet (after you have called
	///         Nextpacket)
	/// only valid with enabled automatic headers
	uint32_t    GetFlags () const;
	/// @return true: if the packet is meant to be reliable (transport)
	/// only valid with enabled automatic headers
	bool        isReliable () const;
	/// @return the client type of the current packet (after you have called
	///         Nextpacket)
	/// @return the message counter of the current packet
	///	    (after you have called Nextpacket)
	/// only valid with enabled automatic headers
	uint32_t    GetMessageCounter () const;
	/// @return the client ID of the current packet (after you have called
	///         Nextpacket)
	/// @return true if there were any errors in the packetheader
	/// only valid with enabled automatic headers
	bool        GetHasErrors () const;
	/// @return a message decribing the error (if there was any)
	string      GetErrorMesg () const;
	/// @return a message decribing the error (if there was any)
	inline uint32_t GetHeaderSize () const
	{
		return ( m_HeaderSize );
	};
	/*
	/// Write_methods write methods
	/// Write data to the receiver with these functions.
	/// The optional parameter Flags decides if the sender
	/// waits an ackownledgment for this data or not. If sent as
	/// RELIABLE, the complete packet is turned into a RELIABLE
	/// packet, regardless of other data. \n
	/// If the current packet does not provide enough free space to store
	/// the data, a new packet is requested from the pool and the data is
	/// written to the new packet.
	*/
	/* Write values */
	///
	bool Write_int8 ( const int8_t& Data,
	                  const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_uint8 ( const uint8_t& Data,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_int16 ( const int16_t& Data,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_uint16 ( const uint16_t& Data,
	                    const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_int32 ( const int32_t& Data,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_uint32 ( const uint32_t& Data,
	                    const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_int64 ( const int64_t& Data,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_uint64 ( const uint64_t& Data,
	                    const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_float ( const float& Data,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool Write_double ( const double& Data,
	                    const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/* write ID and value. Ensure both are sent within one packet.*/
	///
	bool WriteID1_int8 ( const uint32_t ID,
	                     const int8_t& Data,
	                     const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_uint8 ( const uint32_t ID,
	                      const uint8_t& Data,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_int16 ( const uint32_t ID,
	                      const int16_t& Data,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_uint16 ( const uint32_t ID,
	                       const uint16_t& Data,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_int32 ( const uint32_t ID,
	                      const int32_t& Data,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_uint32 ( const uint32_t ID,
	                       const uint32_t& Data,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_int64 ( const uint32_t ID,
	                      const int64_t& Data,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_uint64 ( const uint32_t ID,
	                       const uint64_t& Data,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_float ( const uint32_t ID,
	                      const float& Data,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID1_double ( const uint32_t ID,
	                       const double& Data,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/* write ID and two values. Ensure that all are sent within one packet */
	///
	bool WriteID2_int8 ( const uint32_t ID,
	                     const int8_t& Data1,
	                     const int8_t& Data2,
	                     const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_uint8 ( const uint32_t ID,
	                      const uint8_t& Data1,
	                      const uint8_t& Data2,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_int16 ( const uint32_t ID,
	                      const int16_t& Data1,
	                      const int16_t& Data2,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_uint16 ( const uint32_t ID,
	                       const uint16_t& Data1,
	                       const uint16_t& Data2,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_int32 ( const uint32_t ID,
	                      const int32_t& Data1,
	                      const int32_t& Data2,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_uint32 ( const uint32_t ID,
	                       const uint32_t& Data1,
	                       const uint32_t& Data2,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_int64 ( const uint32_t ID,
	                      const int64_t& Data1,
	                      const int64_t& Data2,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_uint64 ( const uint32_t ID,
	                       const uint64_t& Data1,
	                       const uint64_t& Data2,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_float ( const uint32_t ID,
	                      const float& Data1,
	                      const float& Data2,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID2_double ( const uint32_t ID,
	                       const double& Data1,
	                       const double& Data2,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/* write ID and three values. Ensure that all are sent within one packet. */
	///
	bool WriteID3_int8 ( const uint32_t ID,
	                     const int8_t& Data1,
	                     const int8_t& Data2,
	                     const int8_t& Data3,
	                     const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_uint8 ( const uint32_t ID,
	                      const uint8_t& Data1,
	                      const uint8_t& Data2,
	                      const uint8_t& Data3,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_int16 ( const uint32_t ID,
	                      const int16_t& Data1,
	                      const int16_t& Data2,
	                      const int16_t& Data3,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_uint16 ( const uint32_t ID,
	                       const uint16_t& Data1,
	                       const uint16_t& Data2,
	                       const uint16_t& Data3,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_int32 ( const uint32_t ID,
	                      const int32_t& Data1,
	                      const int32_t& Data2,
	                      const int32_t& Data3,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_uint32 ( const uint32_t ID,
	                       const uint32_t& Data1,
	                       const uint32_t& Data2,
	                       const uint32_t& Data3,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_int64 ( const uint32_t ID,
	                      const int64_t& Data1,
	                      const int64_t& Data2,
	                      const int64_t& Data3,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_uint64 ( const uint32_t ID,
	                       const uint64_t& Data1,
	                       const uint64_t& Data2,
	                       const uint64_t& Data3,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_float ( const uint32_t ID,
	                      const float& Data1,
	                      const float& Data2,
	                      const float& Data3,
	                      const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteID3_double ( const uint32_t ID,
	                       const double& Data1,
	                       const double& Data2,
	                       const double& Data3,
	                       const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/* write opaque data */
	/*
	/// Opaque data is encoded as length|data (regardless of encoding type).
	/// Ensure that all values are sent within one packet.
	*/
	///
	bool WriteOpaque ( const char* Data,
	                   const uint32_t Size,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteOpaque ( const uint32_t ID,
	                   const char* Data,
	                   const uint32_t Size,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/*  write strings*/
	/*
	/// strings are encoded as opaque data (regardless of encoding type).
	/// Ensure that all values are sent within one packet.
	*/
	///
	bool WriteString ( const string& Str,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	///
	bool WriteString ( const uint32_t ID,
	                   const string& Str,
	                   const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/// write a c-style string (as is)
	bool WriteCStr ( const char* Str,
	                 const eNETPACKET_FLAGS Flags = UNRELIABLE );
	/// write a c-style string (as is)
	bool WriteCStr ( const uint32_t ID,
	                 const char* Str,
	                 const eNETPACKET_FLAGS Flags = UNRELIABLE );
protected:
	class t_AwaitAckPacket
	{
	public:
		time_t      SentTime;
		uint32_t    SentCounter;
		NetPacket*  Buffer;
	};
	// PacketID, Buffer
	std::map<uint32_t, t_AwaitAckPacket*>   m_AwaitAckQueue;
	std::list<t_ReceivePacket*>             m_ReceiveQueue;
	std::list<NetPacket*>                   m_SendQueue;
	NetPacket::eBUFFER_ENCODING_TYPE        m_Encoding;
	NetSocket           m_DataSocket;
	bool                m_HaveSocket;
	bool                m_AutomaticHeaders;
	uint32_t            m_Magic;
	uint32_t            m_ProtocolVersion;
	fgms::eCLIENTTYPES  m_ClientType;
	uint32_t            m_Flags;
	uint32_t            m_MessageCounter;
	uint32_t            m_HeaderSize;
	uint64_t            m_SenderID;
	bool                m_UseCrypt;
	uint32_t            m_MaxRetries;
	time_t              m_ResendTime;
	uint32_t            m_CheckSumIndex;
	uint32_t            m_FlagIndex;
	bool                m_HasHeader;
	uint32_t            m_CryptPassword[4];
	NetAddr             m_Target;
	NetAddr             m_Sender;
	NetPacketPool*      m_BufferPool;
	NetPacket*          m_CurrentPacket;
	t_ReceivePacket*    m_RcvdPacket;
	/// automatically check a header of new rceived packets
	void CheckHeader ( t_ReceivePacket* Buffer );
	/// automatically write a header to new requested packets
	void WriteHeader ();
	/// Push the packet to the send queue. Automatically add the
	/// buffer the m_AwaitAckQueue if it is marked as RELIABLE
	void PushBuffer ();
	/// Write flags to the current packet.
	void WriteFlags ( const eNETPACKET_FLAGS Flags );
	/// Check if the current packets has enough free space
	/// left to store @Size bytes of data.
	void CheckSize ( const uint32_t Size );
	/// Check if we have packets in the m_AwaitAckQueue that
	/// needs to be resent.
	void CheckAckQueue ();
}; // class UDP_Peer

#endif

