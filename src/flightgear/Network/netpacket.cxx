// netpacket.cxx -  NetPacket is a buffer for network packets
//
// This file is part of fgms
//
// Copyright (C) 2006  Oliver Schroeder
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see
// <http://www.gnu.org/licenses/>.
//

// #define SG_DEBUG_ALL

#include <iostream>
#include <simgear/debug/debug.hxx>
#include "netpacket.hxx"

//////////////////////////////////////////////////////////////////////

NetPacket::NetPacket
(
        const uint32_t Size
)
{
	SG_TRACE_START
	if ( Size == 0 )
	{
		SG_TRACE_END
		return;
	}
	m_Buffer        = new char[Size];
	m_Capacity      = Size;
	m_CurrentIndex  = 0;
	m_BytesInUse    = 0;
	m_SelfAllocated = true;
	m_EncodingType  = NetPacket::XDR;
	Clear();  // FIXME: probably not needed
	SG_TRACE_END
} // NetPacket::NetPacket ( uint32_t Size )

//////////////////////////////////////////////////////////////////////

NetPacket::~NetPacket
()
{
	SG_TRACE_START
	if ( ( m_Buffer ) && ( m_SelfAllocated ) )
	{
		delete[] m_Buffer;
	}
	SG_TRACE_END
} // NetPacket::~NetPacket()

//////////////////////////////////////////////////////////////////////

void
NetPacket::Clear
()
{
	SG_TRACE_START
	if ( m_Buffer )
	{
		memset ( m_Buffer, 0, m_Capacity );
	}
	m_CurrentIndex = 0;
	m_BytesInUse   = 0;
	SG_TRACE_END
} // NetPacket::Clear()

//////////////////////////////////////////////////////////////////////

void
NetPacket::Start
()
{
	SG_TRACE_START
	m_CurrentIndex = 0;
	SG_TRACE_END
} // NetPacket::Start ()

//////////////////////////////////////////////////////////////////////

void
NetPacket::Reset
()
{
	SG_TRACE_START
	m_CurrentIndex = 0;
	m_BytesInUse   = 0;
	SG_TRACE_END
} // NetPacket::Reset ()

//////////////////////////////////////////////////////////////////////

void
NetPacket::Copy
(
        const NetPacket& Packet
)
{
	SG_TRACE_START
	if ( m_Capacity != Packet.m_Capacity )
	{
		delete m_Buffer;
		m_Capacity = Packet.m_Capacity;
		m_Buffer  = new char[m_Capacity];
	}
	memcpy ( m_Buffer, Packet.m_Buffer, m_Capacity );
	m_Capacity      = Packet.m_Capacity;
	m_BytesInUse    = Packet.m_BytesInUse;
	m_CurrentIndex  = Packet.m_CurrentIndex;
	SG_TRACE_END
} // NetPacket::Copy ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::SetIndex
(
        const uint32_t Index
)
{
	SG_TRACE_START
	if ( ( !m_Buffer ) || ( Index > m_BytesInUse ) )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	m_CurrentIndex = Index;
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::SetIndex ()

//////////////////////////////////////////////////////////////////////

void
NetPacket::SetEncoding
(
        const eBUFFER_ENCODING_TYPE encoding
)
{
	SG_TRACE_START
	m_EncodingType = encoding;
	SG_TRACE_END
} // NetPacket::SetEncoding()

//////////////////////////////////////////////////////////////////////

NetPacket::eBUFFER_ENCODING_TYPE
NetPacket::GetEncoding
() const
{
	SG_TRACE_START
	SG_TRACE_RETURN ( m_EncodingType )
	return ( m_EncodingType );
} // NetPacket::GetEncoding()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Skip
(
        const uint32_t NumberofBytes
)
{
	SG_TRACE_START
	uint32_t Index = m_CurrentIndex + NumberofBytes;
	if ( ( !m_Buffer ) || ( Index > m_Capacity ) )
	{
		SG_TRACE_RETURN ( 0 )
		return ( 0 );
	}
	m_CurrentIndex = Index;
	if ( m_CurrentIndex > m_BytesInUse )
	{
		m_BytesInUse = m_CurrentIndex;
	}
	SG_TRACE_RETURN ( m_CurrentIndex )
	return ( m_CurrentIndex );
} // NetPacket::Skip ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Available
() const
{
	SG_TRACE_START
	SG_TRACE_RETURN ( m_Capacity - m_CurrentIndex )
	return ( m_Capacity - m_CurrentIndex );
} // NetPacket::Available ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Capacity
() const
{
	SG_TRACE_START
	SG_TRACE_RETURN ( m_Capacity )
	return ( m_Capacity );
} // NetPacket::Capacity ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::BytesUsed
() const
{
	SG_TRACE_START
	SG_TRACE_RETURN ( m_BytesInUse )
	return ( m_BytesInUse );
} // NetPacket::BytesUsed ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::isAvailable
(
        const uint32_t Size
) const
{
	SG_TRACE_START
	if ( Available() < Size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::isAvailable  ( const uint32_t Size )

//////////////////////////////////////////////////////////////////////

void
NetPacket::SetBuffer
(
        const char* Buffer, const uint32_t Size
)
{
	SG_TRACE_START
	if ( ( !Buffer ) || ( Size == 0 ) )
	{
		SG_TRACE_END
		return;
	}
	if ( ( m_Buffer ) && ( m_SelfAllocated ) )
	{
		delete[] m_Buffer;
	}
	m_SelfAllocated = false;
	m_Buffer	= ( char* ) Buffer;
	m_Capacity	= Size;
	m_CurrentIndex	= 0;
	m_BytesInUse	= Size;
	SG_TRACE_END
} // NetPacket::SetBuffer ()

//////////////////////////////////////////////////////////////////////

void
NetPacket::SetUsed
(
        const uint32_t UsedBytes
)
{
	SG_TRACE_START
	m_BytesInUse = UsedBytes;
	SG_TRACE_END
} // NetPacket::SetUsed ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::RemainingData
() const
{
	SG_TRACE_START
	SG_DEBUG_OUT ( m_BytesInUse - m_CurrentIndex
	  << " bytes left for reading" );
	SG_TRACE_RETURN ( m_BytesInUse - m_CurrentIndex )
	return ( m_BytesInUse - m_CurrentIndex );
} // NetPacket::RemainingData ()

//////////////////////////////////////////////////////////////////////
//
// eXtended Tiny Encryption Algorithm
// originally developed by David Wheeler and Roger Needham at the Computer
// Laboratory of Cambridge University

void
NetPacket::xtea_encipher
(
	unsigned int num_cycles,
	uint32_t v[2],
	uint32_t const k[4]
)
{
	unsigned int i;
	const uint32_t delta = 0x9E3779B9;
	uint32_t v0 = v[0], v1 = v[1], sum = 0;

	for (i=0; i < num_cycles; i++)
	{
		v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + k[sum & 3]);
		sum += delta;
		v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + k[(sum>>11) & 3]);
	}
	v[0] = v0; v[1] = v1;
} // NetPacket::xtea_encipher ()
 
//////////////////////////////////////////////////////////////////////

void
NetPacket::xtea_decipher
(
	unsigned int num_cycles,
	uint32_t v[2],
	uint32_t const k[4]
)
{
	unsigned int i;
	const uint32_t delta = 0x9E3779B9;
	uint32_t v0 = v[0], v1 = v[1], sum = delta * num_cycles;

	for (i=0; i < num_cycles; i++) {
		v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + k[(sum>>11) & 3]);
		sum -= delta;
		v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + k[sum & 3]);
	}
	v[0] = v0; v[1] = v1;
} // NetPacket::xtea_decipher ()

//////////////////////////////////////////////////////////////////////

void
NetPacket::Encrypt
(
	const uint32_t Key[4],
	const uint32_t Offset
)
{
	SG_TRACE_START
	uint32_t Len = m_BytesInUse;
	uint32_t Mod = Len % XTEA_BLOCK_SIZE;

	if ( ( ! m_Buffer ) || ( m_BytesInUse == 0 ) )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "NetPacket::Encrypt() - nothing to encrypt!" );
		SG_TRACE_END
		return;
	}
	if ( m_BytesInUse <= Offset )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "NetPacket::Enrypt() - Offset>Used Bytes!" );
		SG_TRACE_END
		return;
	}
	for ( uint32_t i=Offset; i<=Len-XTEA_BLOCK_SIZE; i+=XTEA_BLOCK_SIZE )
	{
		xtea_encipher (32, (uint32_t*) &m_Buffer[i], Key);
	}
	if (Mod)
	{
		uint32_t Off = Len - XTEA_BLOCK_SIZE;
		xtea_encipher (32, (uint32_t*) &m_Buffer[Off], Key);
	}
	SG_TRACE_END
} // NetPacket::Encrypt

//////////////////////////////////////////////////////////////////////

void
NetPacket::Decrypt
(
	const uint32_t Key[4],
	const uint32_t Offset
)
{
	SG_TRACE_START
	uint32_t Len = m_BytesInUse;
	uint32_t Mod = Len % XTEA_BLOCK_SIZE;

	if ( ( ! m_Buffer ) || ( m_BytesInUse == 0 ) )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "NetPacket::Decrypt() - nothing to encrypt!" );
		SG_TRACE_END
		return;
	}
	if ( m_BytesInUse <= Offset )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,  "NetPacket::Decrypt() - Offset>Used Bytes!" );
		SG_TRACE_END
		return;
	}
	if (Mod)
	{
		uint32_t Off = Len - XTEA_BLOCK_SIZE;
		xtea_decipher (32, (uint32_t*) &m_Buffer[Off], Key);
	}
	for ( uint32_t i=Offset; i<=Len-XTEA_BLOCK_SIZE; i+=XTEA_BLOCK_SIZE )
	{
		xtea_decipher (32, (uint32_t*) &m_Buffer[i], Key);
	}
	SG_TRACE_END
} // NetPacket::Decrypt

//////////////////////////////////////////////////////////////////////

bool
NetPacket::WriteOpaque
(
        const void* Data,
        const uint32_t Size
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		// no buffer, so nothing to do but something is wrong
		sg_exception ex ( "no buffer", "NetPacket::WriteOpaque ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
		return ( false );
	}
	if ( ( ! Data ) || ( Size == 0 ) )
	{
		// no data, so we are done
		SG_TRACE_RETURN ( "no data" )
		return ( true );
	}
	// round data size to a multiple of XDR datasize
	int rndup = 0;
	if ( m_EncodingType == NetPacket::XDR )
	{
		int AlignBytes = sizeof ( xdr_data_t );
		rndup = Size % AlignBytes;
		if ( rndup )
		{
			rndup = AlignBytes - rndup;
		}
	}
	if ( Available() < Size+rndup )
	{
		// not enough space left for data
		SG_TRACE_RETURN ( "not enough space left" )
		return ( false );
	}
	Write_uint32 ( Size+rndup );
	char* Index = ( char* ) m_Buffer + m_CurrentIndex;
	memcpy ( Index, Data, Size );
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += Size;
		m_BytesInUse += Size;
	}
	if ( rndup )
	{
		memset ( m_Buffer+m_CurrentIndex, 0, rndup );
		if ( m_CurrentIndex == m_BytesInUse )
		{
			m_CurrentIndex += rndup;
			m_BytesInUse += rndup;
		}
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::WriteOpaque ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::WriteString
(
        const string& Str
)
{
	SG_TRACE_START
	SG_TRACE_END
	return ( WriteOpaque ( ( const void* ) Str.c_str(), Str.size()+1 ) );
} // WriteString ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::WriteCStr
(
        const char* Str,
	uint32_t Size
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		// no buffer, so nothing to do but something is wrong
		sg_exception ex ( "no buffer", "NetPacket::WriteCStr ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	if ( ( ! Str ) || ( Size == 0 ) )
	{
		// no data, so we are done
		SG_TRACE_RETURN ( "no data" )
		return ( true );
	}
	uint32_t len = Size;
	if ( Size == 0 )
	{
		len = strlen ( Str );
	}
	if ( len == 0 )
	{
		// no data, so we are done
		SG_TRACE_RETURN ( "no data" )
		return ( true );
	}
	if ( Available() < len )
	{
		// not enough space left for data
		SG_TRACE_RETURN ( "not enough space left" )
		return ( false );
	}
	char* Index = ( char* ) m_Buffer + m_CurrentIndex;
	memcpy ( Index, Str, len );
	Index += len;
	*Index = '0';
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += len;
		m_BytesInUse += len;
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::WriteCStr ()

//////////////////////////////////////////////////////////////////////

void
NetPacket::ReadOpaque
(
        void* Buffer,
        uint32_t& Size
) throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t data_available = RemainingData();
	if ( ( ! m_Buffer ) || ( data_available == 0 ) )
	{
		sg_exception ex ( "no more data to read",
		  "NetPacket::ReadOpaque ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	Size = Read_uint32 ();
	if (data_available < Size)
	{
		sg_exception ex ( "invalid length",
		  "NetPacket::ReadOpaque ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	char* ret = ( char* )  &m_Buffer[m_CurrentIndex];
	memcpy ( Buffer, ret, Size );
	m_CurrentIndex += Size;
	SG_TRACE_END
} // NetPacket::ReadOpaque ()

//////////////////////////////////////////////////////////////////////

string
NetPacket::ReadString
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t data_available = RemainingData();
	if ( ( ! m_Buffer ) || ( data_available == 0 ) )
	{
		sg_exception ex ( "no more data to read",
		  "NetPacket::ReadString ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	uint32_t Size = Read_uint32 ();
	if (data_available < Size)
	{
		sg_exception ex ( "invalid length",
		  "NetPacket::ReadString ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	string ret = ( char* )  &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += Size;
	SG_TRACE_END
	return ( ret );
} // NetPacket::ReadString ()

//////////////////////////////////////////////////////////////////////

string
NetPacket::ReadCStr
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ( ! m_Buffer ) || ( RemainingData() == 0 ) )
	{
		sg_exception ex ( "no more data to read",
		  "NetPacket::ReadCStr ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	string ret = ( char* )  &m_Buffer[m_CurrentIndex];
	int len = ret.size () + 1;
	m_CurrentIndex += len;
	SG_TRACE_END
	return ( ret );
} // NetPacket::ReadCStr ()

//////////////////////////////////////////////////////////////////////

int8_t
NetPacket::Read_XDR_int8
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_int32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_int8 ( Data ) );
} // NetPacket::Read_XDR_int8 ()

//////////////////////////////////////////////////////////////////////

uint8_t
NetPacket::Read_XDR_uint8
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_uint8 ( Data ) );
} // NetPacket::Read_XDR_uint8 ()

//////////////////////////////////////////////////////////////////////

int16_t
NetPacket::Read_XDR_int16
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_int16 ( Data ) );
} // NetPacket::Read_XDR_int16 ()

//////////////////////////////////////////////////////////////////////

uint16_t
NetPacket::Read_XDR_uint16
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_uint16 ( Data ) );
} // NetPacket::Read_XDR_uint16 ()

//////////////////////////////////////////////////////////////////////

int32_t
NetPacket::Read_XDR_int32
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_int32 ( Data ) );
} // NetPacket::Read_XDR_int32 ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Read_XDR_uint32
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_uint32 ( Data ) );
} // NetPacket::Read_XDR_uint32 ()

//////////////////////////////////////////////////////////////////////

int64_t
NetPacket::Read_XDR_int64
() throw ( sg_exception )
{
	SG_TRACE_START
	uint64_t Data;
	try
	{
		Data = Read_NONE_uint64 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_int64 ( Data ) );
} // NetPacket::Read_XDR_int64 ()

//////////////////////////////////////////////////////////////////////

uint64_t
NetPacket::Read_XDR_uint64
() throw ( sg_exception )
{
	SG_TRACE_START
	uint64_t Data;
	try
	{
		Data = Read_NONE_uint64 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_uint64 ( Data ) );
} // NetPacket::Read_XDR_uint64 ()

//////////////////////////////////////////////////////////////////////

float
NetPacket::Read_XDR_float
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_float ( Data ) );
} // NetPacket::Read_XDR_float ()

//////////////////////////////////////////////////////////////////////

double
NetPacket::Read_XDR_double
() throw ( sg_exception )
{
	SG_TRACE_START
	uint64_t Data;
	try
	{
		Data = Read_NONE_uint64 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( XDR_decode_double ( Data ) );
} // NetPacket::Read_XDR_double ()

//////////////////////////////////////////////////////////////////////

int8_t
NetPacket::Read_NET_int8
() throw ( sg_exception )
{
	SG_TRACE_START
	int8_t Data;
	try
	{
		Data = Read_NONE_int8 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_uint8 ( Data ) );
} // NetPacket::Read_NET_int8 ()

//////////////////////////////////////////////////////////////////////

uint8_t
NetPacket::Read_NET_uint8
() throw ( sg_exception )
{
	SG_TRACE_START
	uint8_t Data;
	try
	{
		Data = Read_NONE_uint8 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_uint8 ( Data ) );
} // NetPacket::Read_NET_uint8 ()

//////////////////////////////////////////////////////////////////////

int16_t
NetPacket::Read_NET_int16
() throw ( sg_exception )
{
	SG_TRACE_START
	int16_t Data;
	try
	{
		Data = Read_NONE_uint16 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_int16 ( Data ) );
} // NetPacket::Read_NET_int16 ()

//////////////////////////////////////////////////////////////////////

uint16_t
NetPacket::Read_NET_uint16
() throw ( sg_exception )
{
	SG_TRACE_START
	uint16_t Data;
	try
	{
		Data = Read_NONE_uint16 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_uint16 ( Data ) );
} // NetPacket::Read_NET_uint16 ()

//////////////////////////////////////////////////////////////////////

int32_t
NetPacket::Read_NET_int32
() throw ( sg_exception )
{
	SG_TRACE_START
	int32_t Data;
	try
	{
		Data = Read_NONE_int32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_int32 ( Data ) );
} // NetPacket::Read_NET_int32 ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Read_NET_uint32
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_uint32 ( Data ) );
} // NetPacket::Read_NET_uint32 ()

//////////////////////////////////////////////////////////////////////

int64_t
NetPacket::Read_NET_int64
() throw ( sg_exception )
{
	SG_TRACE_START
	int64_t Data;
	try
	{
		Data = Read_NONE_int64 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_int64 ( Data ) );
} // NetPacket::Read_NET_int64 ()

//////////////////////////////////////////////////////////////////////

uint64_t
NetPacket::Read_NET_uint64
() throw ( sg_exception )
{
	SG_TRACE_START
	uint64_t Data;
	try
	{
		Data = Read_NONE_uint64 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_uint64 ( Data ) );
} // NetPacket::Read_NET_int64 ()

//////////////////////////////////////////////////////////////////////

float
NetPacket::Read_NET_float
() throw ( sg_exception )
{
	SG_TRACE_START
	uint32_t Data;
	try
	{
		Data = Read_NONE_uint32 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_float ( Data ) );
} // NetPacket::Read_NET_float ()

//////////////////////////////////////////////////////////////////////

double
NetPacket::Read_NET_double
() throw ( sg_exception )
{
	SG_TRACE_START
	uint64_t Data;
	try
	{
		Data = Read_NONE_uint64 ();
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( NET_decode_double ( Data ) );
} // NetPacket::Read_NET_double ()

//////////////////////////////////////////////////////////////////////

int8_t
NetPacket::Read_NONE_int8
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int8_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_int8 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	int8_t* Data = (int8_t*) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( * Data );
} // NetPacket::Read_NONE_int8 ()

//////////////////////////////////////////////////////////////////////

uint8_t
NetPacket::Read_NONE_uint8
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint8_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_uint8 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	uint8_t* Data = (uint8_t*) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_uint8 ()

//////////////////////////////////////////////////////////////////////

int16_t
NetPacket::Read_NONE_int16
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int16_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_int16 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	int16_t* Data = ( int16_t* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_int16 ()

//////////////////////////////////////////////////////////////////////

uint16_t
NetPacket::Read_NONE_uint16
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint16_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_uint16 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	uint16_t* Data = ( uint16_t* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_uint16 ()

//////////////////////////////////////////////////////////////////////

int32_t
NetPacket::Read_NONE_int32
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int32_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_int32 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	int32_t* Data = ( int32_t* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_int32  ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Read_NONE_uint32
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint32_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_uint32 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	uint32_t* Data = ( uint32_t* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_uint32  ()

//////////////////////////////////////////////////////////////////////

int64_t
NetPacket::Read_NONE_int64
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int64_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_int64 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	int64_t* Data = ( int64_t* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_int64  ()

//////////////////////////////////////////////////////////////////////

uint64_t
NetPacket::Read_NONE_uint64
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint64_t );
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_uint64 ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	uint64_t* Data = ( uint64_t* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_uint64  ()

//////////////////////////////////////////////////////////////////////

float
NetPacket::Read_NONE_float
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( float );
	if ( size > sizeof ( xdr_data_t ) )
	{
		sg_exception ex ( "wrong size of float",
		                  "NetPacket::Read_XDR_float ()" );
		SG_TRACE_RETURN ( "wrong size of float" )
		throw ( ex );
	}
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_float ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	float* Data = ( float* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_float  ()

//////////////////////////////////////////////////////////////////////

double
NetPacket::Read_NONE_double
() throw ( sg_exception )
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( double );
	if ( size != sizeof ( xdr_data2_t ) )
	{
		sg_exception ex ( "wrong size of double",
		                  "NetPacket::Read_XDR_double ()" );
		SG_TRACE_RETURN ( "wrong size of double" )
		throw ( ex );
	}
	if ( size > RemainingData () )
	{
		sg_exception ex ( "no more data to read",
		                  "NetPacket::Read_NONE_double ()" );
		SG_TRACE_RETURN ( "no more data" )
		throw ( ex );
	}
	double* Data = ( double* ) &m_Buffer[m_CurrentIndex];
	m_CurrentIndex += size;
	SG_TRACE_END
	return ( *Data );
} // NetPacket::Read_NONE_double  ()

//////////////////////////////////////////////////////////////////////

int8_t
NetPacket::Read_int8
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_int8 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_int8 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_int8 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_int8 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_int8 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_int8 ()

//////////////////////////////////////////////////////////////////////

uint8_t
NetPacket::Read_uint8
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_uint8 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_uint8 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_uint8 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_uint8 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_uint8 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_uint8 ()

//////////////////////////////////////////////////////////////////////

int16_t
NetPacket::Read_int16
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_int16 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_int16 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_int16 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_int16 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_int16 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_int16 ()

//////////////////////////////////////////////////////////////////////

uint16_t
NetPacket::Read_uint16
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_uint16 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_uint16 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_uint16 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_uint16 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_uint16 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_uint16 ()

//////////////////////////////////////////////////////////////////////

int32_t
NetPacket::Read_int32
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_int32 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_int32 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_int32 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_int32 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_int32 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_int32 ()

//////////////////////////////////////////////////////////////////////

uint32_t
NetPacket::Read_uint32
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_uint32 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_uint32 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_uint32 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_uint32 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_uint32 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_uint32 ()

//////////////////////////////////////////////////////////////////////

int64_t
NetPacket::Read_int64
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_int64 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_int64 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_int64 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_int64 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_int64 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_int64 ()

//////////////////////////////////////////////////////////////////////

uint64_t
NetPacket::Read_uint64
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_uint64 ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_uint64 () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_uint64 () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_uint64 () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_uint64 ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_uint64 ()

//////////////////////////////////////////////////////////////////////

float
NetPacket::Read_float
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_float ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_float () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_float () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_float () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_float ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_float ()

//////////////////////////////////////////////////////////////////////

double
NetPacket::Read_double
() throw ( sg_exception )
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		sg_exception ex ( "no buffer", "NetPacket::Read_double ()" );
		SG_TRACE_RETURN ( "no buffer" )
		throw ( ex );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Read_XDR_double () );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Read_NET_double () );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Read_NONE_double () );
		default:
			sg_exception ex ( "unknown encoding type",
			                  "NetPacket::Read_double ()" );
			SG_TRACE_END
			throw ( ex );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_END
	return ( 0 ); // keep compilers happy
} // Read_double ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_int8
(
        const int8_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( XDR_encode_int8 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_int8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_uint8
(
        const uint8_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( XDR_encode_uint8 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_uint8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_int16
(
        const int16_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( XDR_encode_int16 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_int16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_uint16
(
        const uint16_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( XDR_encode_uint16 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_uint16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_int32
(
        const int32_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( XDR_encode_int32 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_int32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_uint32
(
        const uint32_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( XDR_encode_uint32 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_uint32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_int64
(
        const int64_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint64 ( XDR_encode_int64 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_int64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_uint64
(
        const uint64_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint64 ( XDR_encode_uint64 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_uint64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_float
(
        const float& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( xdr_data_t );
	if ( size != sizeof ( float ) )
	{
		sg_exception ex ( "wrong size of double",
		                  "NetPacket::Write_XDR_double ()" );
		SG_TRACE_RETURN ( "wrong size of double" )
		throw ( ex );
	}
	try
	{
		Write_NONE_uint32 ( XDR_encode_float ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_float ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_XDR_double
(
        const double& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( xdr_data2_t );
	if ( size != sizeof ( double ) )
	{
		sg_exception ex ( "wrong size of double",
		                  "NetPacket::Write_XDR_double ()" );
		SG_TRACE_RETURN ( "wrong size of double" )
		throw ( ex );
	}
	try
	{
		Write_NONE_uint64 ( XDR_encode_double ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_XDR_double ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_int8
(
        const int8_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_int8 ( NET_encode_int8 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_int8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_uint8
(
        const uint8_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint8 ( NET_encode_uint8 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_uint8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_int16
(
        const int16_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_int16 ( NET_encode_int16 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_int16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_uint16
(
        const uint16_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint16 ( NET_encode_uint16 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_uint16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_int32
(
        const int32_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_int32 ( NET_encode_int32 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_int32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_uint32
(
        const uint32_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( NET_encode_uint32 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_uint32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_int64
(
        const int64_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_int64 ( NET_encode_int64 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_int64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_uint64
(
        const uint64_t& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint64 ( NET_encode_uint64 ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_uint64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_float
(
        const float& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint32 ( NET_encode_float ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_float ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NET_double
(
        const double& Data
)
{
	SG_TRACE_START
	try
	{
		Write_NONE_uint64 ( NET_encode_double ( Data ) );
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		throw ( ex );
	}
	SG_TRACE_RETURN ( "true" )
	return ( true );
} // NetPacket::Write_NET_double ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_int8
(
        const int8_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int8_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	int8_t* Index = ( int8_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_int8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_uint8
(
        const uint8_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint8_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	uint8_t* Index = ( uint8_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_uint8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_int16
(
        const int16_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int16_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	int16_t* Index = ( int16_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_int16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_uint16
(
        const uint16_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint16_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	uint16_t* Index = ( uint16_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_uint16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_int32
(
        const int32_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int32_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	int32_t* Index = ( int32_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_int32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_uint32
(
        const uint32_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint32_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	uint32_t* Index = ( uint32_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_uint32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_int64
(
        const int64_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( int64_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	int64_t* Index = ( int64_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_int64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_uint64
(
        const uint64_t& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( uint64_t );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	uint64_t* Index = ( uint64_t* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_uint64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_float
(
        const float& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( float );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	float* Index = ( float* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_float ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_NONE_double
(
        const double& Data
)
{
	SG_TRACE_START
	unsigned int size;
	size = sizeof ( double );
	if ( Available() < size )
	{
		SG_TRACE_RETURN ( "false" )
		return ( false );
	}
	double* Index = ( double* ) &m_Buffer[m_CurrentIndex];
	*Index = Data;
	if ( m_CurrentIndex == m_BytesInUse )
	{
		m_CurrentIndex += size;
		m_BytesInUse += size;
	}
	SG_TRACE_RETURN ( "true" )
	return true;
} // NetPacket::Write_NONE_double ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_int8
(
        const int8_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_int8 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_int8 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_int8 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_int8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_uint8
(
        const uint8_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_uint8 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_uint8 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_uint8 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_uint8 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_int16
(
        const int16_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_int16 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_int16 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_int16 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); //  never reached
} // NetPacket::Write_int16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_uint16
(
        const uint16_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_uint16 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_uint16 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_uint16 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_uint16 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_int32
(
        const int32_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_int32 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_int32 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_int32 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_int32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_uint32
(
        const uint32_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_uint32 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_uint32 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_uint32 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_uint32 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_int64
(
        const int64_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_int64 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_int64 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_int64 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_int64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_uint64
(
        const uint64_t& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_uint64 ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_uint64 ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_uint64 ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_uint64 ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_float
(
        const float& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_float ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_float ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_float ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_float ()

//////////////////////////////////////////////////////////////////////

bool
NetPacket::Write_double
(
        const double& Data
)
{
	SG_TRACE_START
	if ( ! m_Buffer )
	{
		SG_TRACE_RETURN ( "no buffer" )
		return ( false );
	}
	try
	{
		switch ( m_EncodingType )
		{
		case NetPacket::XDR:
			SG_TRACE_END
			return ( Write_XDR_double ( Data ) );
		case NetPacket::NET:
			SG_TRACE_END
			return ( Write_NET_double ( Data ) );
		case NetPacket::NONE:
			SG_TRACE_END
			return ( Write_NONE_double ( Data ) );
		default:
			SG_TRACE_END
			return ( false );
		}
	}
	catch ( sg_exception& ex )
	{
		SG_TRACE_END
		return ( false );
	}
	return ( false ); // never reached
} // NetPacket::Write_double ()

//////////////////////////////////////////////////////////////////////

