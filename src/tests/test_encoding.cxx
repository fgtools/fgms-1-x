#include <iostream>
// #include <server/debug.hxx>
#include <flightgear/Network/encoding.hxx>

using namespace std;

template <typename T1, typename T2, typename T3>
bool CheckError ( int test, T1 t1, T2 t2, T3 t3 )
{
        if (t1 == t3)
        {
          cout << "Test #" << test << " OK" << endl;
          return (true);
        }
        cout << endl;
        cout << "###################################" << endl;
        cout << "ERROR in test " << test << ":" << endl;
        cout << "have " << t1 << " but should be: " << t3 << endl;
        return (false);
}

int main ()
{
        int     test = 0;
        int     errors = 0;

        sglog().setLogLevels( SG_IO, SG_INFO );      // tiny_xdr

        // 8-Bit
        test++;
        int8_t c = 0x12, c3;
        uint32_t c2;
        c2 = XDR_encode_int8 (c);
        c3 = XDR_decode_int8 (c2);
        if (CheckError (test, c, c2, c3))
                errors++;

        // 16-Bit
        test++;
        int16_t s = 0x1234, s3;
        uint32_t s2;
        s2 = XDR_encode_int16 (s);
        s3 = XDR_decode_int16 (s2);
        if (CheckError (test, s, s2, s3))
                errors++;

        // 32-Bit
        test++;
        int32_t i = 0x12345678, i2, i3;
        i2 = XDR_encode_int32 (i);
        i3 = XDR_decode_int32 (i2);
        if (CheckError (test, i, i2, i3))
                errors++;

        // 64-Bit
        test++;
        int64_t l = 0x1234567812345678, l2, l3;
        l2 = XDR_encode_int64 (l);
        l3 = XDR_decode_int64 (l2);
        if (CheckError (test, l, l2, l3))
                errors++;

        // float
        test++;
        float f = 0x12345678, f3;
        uint32_t f2;
        f2 = XDR_encode_float (f);
        f3 = XDR_decode_float (f2);
        if (CheckError (test, f, f2, f3))
                errors++;

        // double
        test++;
        double d = 0x1234567812345678, d3;
        uint64_t d2;
        d2 = XDR_encode_double (d);
        d3 = XDR_decode_double (d2);
        if (CheckError (test, d, d2, d3))
                errors++;

        // 8-Bit
        test++;
        c2 = NET_encode_int8 (c);
        c3 = NET_decode_int8 (c2);
        if (CheckError (test, c, c2, c3))
                errors++;

        // 16-Bit
        test++;
        s2 = NET_encode_int16 (s);
        s3 = NET_decode_int16 (s2);
        if (CheckError (test, s, s2, s3))
                errors++;

        // 32-Bit
        test++;
        i2 = NET_encode_int32 (i);
        i3 = NET_decode_int32 (i2);
        if (CheckError (test, i, i2, i3))
                errors++;

        // 64-Bit
        test++;
        l2 = NET_encode_int64 (l);
        l3 = NET_decode_int64 (l2);
        if (CheckError (test, l, l2, l3))
                errors++;

        // float
        test++;
        f2 = NET_encode_float (f);
        f3 = NET_decode_float (f2);
        if (CheckError (test, f, f2, f3))
                errors++;

        // double
        test++;
        d2 = NET_encode_double (d);
        d3 = NET_decode_double (d2);
        if (CheckError (test, d, d2, d3))
                errors++;


        return (errors);
}
