#include <iostream>
#include <flightgear/Network/netpacketpool.hxx>
// #include <server/debug.hxx>

using namespace std;

int main ()
{
	int i;

	NetPacketPool	Pool (1400, 10, 3);
	NetPacket*	Buffer[13];

	cout << "starte mit 10 buffer" << endl;
	cout << "Poolsize: " << Pool.Size() << endl;
	for (i=0; i<12; i++)
	{
		Buffer[i] = Pool.GetBuffer();
	}
	cout << "12 buffer sollen belegt sein" << endl;
	cout << "Buf0: " << Buffer[0] << endl;
	cout << "Buf0.capacity " << Buffer[0]->Capacity() << endl;
	cout << "Buf1.capacity " << Buffer[1]->Capacity() << endl;
	cout << "Buf2.capacity " << Buffer[2]->Capacity() << endl;
	cout << "Buf11.capacity " << Buffer[11]->Capacity() << endl;
	cout << "Poolsize: " << Pool.Size() << endl;
	cout << "maintain()" << endl;
	Pool.Maintain();
	cout << "Poolsize: " << Pool.Size() << endl;
	for (i=0; i<12; i++)
	{
		Pool.RecycleBuffer (Buffer[i]);
	}
	cout << "12 buffer wieder frei" << endl;
	cout << "Buf0: " << Buffer[0] << endl;
	cout << "Poolsize: " << Pool.Size() << endl;
	cout << "maintain()" << endl;
	Pool.Maintain();
	cout << "Poolsize: " << Pool.Size() << endl;
	cout << "belege 4 buffer" << endl;
	for (i=0; i<4; i++)
	{
		Buffer[i] = Pool.GetBuffer();
	}
	cout << "Poolsize: " << Pool.Size() << endl;
	cout << "gebe 4 buffer wieder frei" << endl;
	for (i=0; i<4; i++)
	{
		Pool.RecycleBuffer (Buffer[i]);
	}
	cout << "Poolsize: " << Pool.Size() << endl;
	cout << "setze Capacity auf 1000" << endl;
	Pool.SetCapacity (1000);
	cout << "Poolsize: " << Pool.Size() << endl;
	return (0);
}
