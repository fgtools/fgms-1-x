#include <string>
#include <fstream>
#include "simgear/debug/logstream.hxx"

using namespace std;

int
main( int argc, char* argv[] )
{
    ofstream LogFile;

    sglog().setLogLevels( SG_ALL, SG_INFO );
    LogFile.open("test.log", ios::out|ios::app);
    sglog().set_output(LogFile);

    int i = 12345;
    long l = 54321L;
    double d = 3.14159;
    string s = "Hello world!";

    cout << "test" << endl;
    SG_LOG( SG_EVENT, SG_INFO, "event::info "
				 << "i=" << i
				 << ", l=" << l
				 << ", d=" << d
	                         << ", d*l=" << d*l
				 << ", s=\"" << s << "\"" );
    sglog().enable_with_date (true);
    SG_LOG( SG_EVENT, SG_INFO, "event::info "
				 << "i=" << i
				 << ", l=" << l
				 << ", d=" << d
	                         << ", d*l=" << d*l
				 << ", s=\"" << s << "\"" );

    // This shouldn't appear in log output:
    SG_LOG( SG_EVENT, SG_DEBUG, "event::debug "
	    << "- this should be seen - "
	    << "d=" << d
	    << ", s=\"" << s << "\"" );

    return 0;
}
