/// @file fgasclient.cxx
/// A simple authentication client for the flightgear authentication serice
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2006-2015
/// @copyright	GPLv3
///
/// This is only a simple client doing nothing more than sending an
/// authentication request to the authentication service and printing
/// the result. It can also server as a small and simple example of
/// a program using the network library
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#include <flightgear/Network/fg_proto.hxx>

using namespace std;

// #define ENABLE_ENCRYPTION

class fgasclient : public ProtocolHandler
{
public:
	int	Init ( const string& Configname );
	void	Create_Auth_Packet ();
	void	Loop ();
	uint32_t GetAnswer ();
private:
	uint32_t m_ResendTime;
	string	 m_ServerSecret;
}; // class fgasclient

string Username;
string Password;

//////////////////////////////////////////////////////////////////////

int
fgasclient::Init
(
	const string& Configname
)
{
	SG_TRACE_START
	string		ServerName;
	int		ServerPort;
	int		MyPort;

	sglog().setLogLevels( SG_ALL, SG_DEBUG );
	m_Peer.SetSenderID ( 1234 );
	if ( ReadConfig ( Configname, m_Properties ) == fgms::FAILED )
		return (fgms::FAILED);
	m_Properties.setStringValue ("/multiplayer/registration/callsign",
	  Username.c_str() );
	m_Properties.setStringValue ("/multiplayer/registration/password",
	  Password.c_str() );
	m_Properties.setStringValue ( "/multiplayer/registration/client_id",
	 "1234" );
	ServerName     = m_Properties.getStringValue ( "/fgas/name" );
	ServerPort     = m_Properties.getIntValue    ( "/fgas/port" );
	m_ServerSecret = m_Properties.getStringValue ( "/fgas/secret" );
	MyPort         = m_Properties.getIntValue    (
	  "/multiplayer/myself/port" );
	if (! m_Peer.Listen ("", MyPort))
		return (fgms::FAILED);
	m_Peer.SetTarget (ServerName, ServerPort);
	m_Peer.SetClientType ( fgms::FGMS );
	m_ResendTime = m_Peer.GetResendTime ();
	SG_LOG ( SG_NETWORK, SG_INFO, "### using Server "
	  << ServerName << ":" << ServerPort );
	SG_TRACE_END
	return (fgms::SUCCESS);
} // fgasclient::InitConnections ()

//////////////////////////////////////////////////////////////////////

void
fgasclient::Create_Auth_Packet
()
{
	SG_TRACE_START

	fgms::t_PropertyList	SendProps;

	SendProps.push_back ( fgms::AUTH_CLIENT );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/callsign"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/password"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/client_id"] );
	WriteProps (m_Peer, m_Properties, SendProps );
	SG_LOG ( SG_NETWORK, SG_DEBUG, "sending authentication..." );
	m_Peer.Send ();
	SG_TRACE_END
} // fgasclient::Create_Auth_Packet()

//////////////////////////////////////////////////////////////////////

uint32_t
fgasclient::GetAnswer
()
{
	SG_TRACE_START
	t_ReceivePacket*  Packet;

	m_Peer.Receive ();
	while (m_Peer.HasPackets())
	{
		try
		{
			Packet = m_Peer.NextPacket ();
		}
		catch (sg_exception & ex)
		{
			return ( fgms::FAILED );
		}
		if (Packet == 0)
		{
			SG_LOG ( SG_SYSTEMS, SG_ALERT, "no packet!" );
			continue;
		}
		if ( Packet->ClientType != fgms::FGAS )
		{
			SG_LOG ( SG_SYSTEMS, SG_ALERT, "received packet is not from fgas!" );
			continue;
		}
		fgms::t_PropertyList RcvdProps;
		ParsePacket (Packet, m_Peer, RcvdProps, m_Properties );
		while ( RcvdProps.size () > 0 )
		{
			int Command = RcvdProps.front ();
			RcvdProps.pop_front ();
			if (Command < fgms::INTERNAL_COMMAND)
			{
				switch (Command)
				{
				case fgms::AUTH_OK:
					return ( fgms::SUCCESS );
					break;
				case fgms::AUTH_FAILED:
					return ( fgms::FAILED );
					break;
				case fgms::GENERAL_ERROR:
					return ( fgms::FAILED );
					break;
				default:
					SG_LOG ( SG_NETWORK, SG_ALERT, "unhandled command: " << Command );
					return ( fgms::FAILED );
				}
			}
		}
	}
	return (fgms::FAILED);
	SG_TRACE_END
} // fgasclient::GetAnswer ()

//////////////////////////////////////////////////////////////////////

void
fgasclient::Loop
()
{
	SG_TRACE_START
	int	 Client;
	bool	 Authenticated = false;
	uint32_t Code;

	while (! Authenticated)
	{
		if (! Authenticated)
		{
			Create_Auth_Packet ();
		}
		Client = m_Peer.WaitForClients (m_ResendTime);
		if (Client == UDP_Peer::ne_TIMEOUT)
		{
			continue;
		}
		if (! Client)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "fgasclient::Loop() - Bytes <= 0!");
			continue;
		}
		// something on the wire (clients)
		Code = GetAnswer ();
		if (Code == fgms::SUCCESS)
		{
			SG_LOG ( SG_NETWORK, SG_DEBUG, "Authenticated" );
		}
		else if (Code == fgms::FAILED)
		{
			SG_LOG ( SG_NETWORK, SG_DEBUG,
			   "Authentication failed: " << "code: " << Code << " "
			  << " answer: " 
			  << m_Properties.getStringValue ("/multiplayer/system/errormessage")
			);
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_DEBUG,
			   "Authentication failed with unknown "
			  << "code: " << Code
			  << " answer: "
			  << m_Properties.getStringValue ("/multiplayer/system/errormessage")
			);
		}
		Authenticated = true;
	}
	SG_TRACE_END
} // fgasclient::Loop ()

//////////////////////////////////////////////////////////////////////

int main ( int argc, char* argv[] )
{
	if (argc < 4)
	{
		cout << "syntax fgasclient "
		     << "config.xml username password"
		     << endl;
		cout << "use 'fgms::FAILED' to test a failed authentication" << endl;
		return (fgms::FAILED);
	}
	Username = argv[2];
	Password = argv[3];
	fgasclient Client;
	if (! Client.Init (argv[1]))
		return (fgms::FAILED);
	Client.Loop ();
	return (fgms::SUCCESS);
}

