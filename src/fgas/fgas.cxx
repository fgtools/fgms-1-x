///
/// @file fgas.cxx
/// flightgear authentication service
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2008-2015
/// @copyright	GPLv3
///
/// This is the authentication service for the flightgear multiplayer
/// network. It is a standalone server only doing authentication. A client
/// can send an authentication request to the authentication service and
/// gets the corresponding answer. Only fgms is considered as a valid
/// client for this service and all fgms clients must be configured with
/// the correct passwort. The communication between fgms and the
/// authentication service is encrypted using XTAE (Extended Tiny
/// Encryption Algorithm). XTEA isn't the most secure algorithm on the
/// planet but is fast, simple and strong enough to keep hobby
/// cryptographers from breaking the encryption (proof me wrong! :)
/// Since XTEA demands a 128-bit key, we build an md5 hash of the provided
/// password which happens to be 128-bit by coincidence.
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#define FGAS_VERSION "0.0.1-pre-alpha1"

#include <unistd.h>
#include <fglib/fg_daemon.hxx>
#include <fglib/fg_typcnvt.hxx>
#include <flightgear/Network/fg_proto.hxx>
#ifdef _MSC_VER
#include <getopt/getopt.h>
#endif

using namespace std;

// #define ENABLE_ENCRYPTION


class fgas : public ProtocolHandler
{
public:
	fgas ();
	int Init ();
	void Loop ();
	void Done ();
	void WantExit ();
	int  ParseParams ( int argc, char* argv[] );
private:
	string GetPassphrase ( const NetAddr &SenderAddress );
	void SendFailed ( const NetAddr & Sender, const uint32_t Cmd,
	  const string & Msg );
	void Register ();
	void HandleAuthRequest ( t_ReceivePacket* Packet );
	void HandleCommand ( int Command, const NetAddr& Sender,
	  t_ReceivePacket* Packet );
	void HandleRequest ();
	void Maintainance ();
	void PrintHelp ();

	enum eSTATE
	{
		REGISTER,
		RUNNING
	};
	class FGLS
	{
	public:
		time_t		LastSeen;
		time_t		RegisteredAt;
		uint64_t	NumPingsSent;
		uint64_t	NumPingsRcvd;
		uint64_t	NumPongs;
		NetAddr		Addr;
	};
	eSTATE	m_State;
	bool	m_WantExit;
	uint32_t m_ResendTime;
	FGLS	m_FGLS;
	uint32_t m_CheckInterval;
}; // class fgas

//////////////////////////////////////////////////////////////////////

fgas::fgas
()
{
	m_WantExit = false;
	sglog().setLogLevels ( SG_ALL, SG_WARN );
} // fgas::fgas ()

//////////////////////////////////////////////////////////////////////

void
fgas::WantExit
()
{
	m_WantExit = true;
} // fgas::WantExit ()

//////////////////////////////////////////////////////////////////////

int
fgas::Init
()
{
	string	Addr;
	int	Port;

	Addr	= m_Properties.getStringValue ( "/fgas/addr" );
	Port	= m_Properties.getIntValue    ( "/fgas/port" );
	if (! m_Peer.Listen (Addr, Port) )
	{
		return (fgms::FAILED);
	}
	m_Peer.SetClientType (fgms::FGAS);
	// copy our registration data
	const char* Name     = m_Properties.getStringValue ( "/fgas/name" );
	const char* Location = m_Properties.getStringValue ( "/fgas/location" );
	m_CheckInterval = m_Properties.getIntValue (                          
	          "/multiplayer/system/checkinterval" );
	if ( m_CheckInterval == 0 )
		m_CheckInterval = 10;
	m_Properties.setIntValue    (
	  "/multiplayer/registration/clienttype", fgms::FGAS);
	m_Properties.setStringValue (
	  "/multiplayer/registration/name", Name );
	m_Properties.setStringValue (
	  "/multiplayer/registration/location", Location );
	m_Properties.setIntValue    (
	  "/multiplayer/registration/port", Port);
	m_ResendTime = m_Peer.GetResendTime ();
	SG_LOG ( SG_NETWORK, SG_WARN, "# listening to port " << Port);
	Addr   = m_Properties.getStringValue ( "/fgls/addr" );
	Port   = m_Properties.getIntValue    ( "/fgls/port" );
	m_FGLS.Addr.Assign ( Addr, Port );
	m_FGLS.LastSeen		= 0;
	m_FGLS.RegisteredAt	= 0;
	m_FGLS.NumPingsRcvd	= 0;
	m_FGLS.NumPingsSent	= 0;
	m_FGLS.NumPongs		= 0;
	m_State = REGISTER;
	sglog().enable_with_date ( true );
	return (fgms::SUCCESS);
} // fgas::Init ()

//////////////////////////////////////////////////////////////////////

void
fgas::Done
()
{
	if ( m_State != RUNNING )
		return;
	fgms::t_PropertyList    SendProps;
	SendProps.push_back ( fgms::SERVER_QUIT );
	m_Peer.SetTarget ( m_FGLS.Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // fgas::Done ()

//////////////////////////////////////////////////////////////////////

void
fgas::PrintHelp
()
{
	cerr << endl;
	cerr << "syntax: fgas [options] configname" << endl;
	cerr << "\n"
		"options are:\n"
		"-h        print this help screen\n"
		"-v LEVEL  verbosity (loglevel) in range 1 (much) and 4 (few)\n"
		"          default is 3, 0 prints *allot*\n"
		"\n"
		"commandline parameters always override config file options\n"
		"\n";
} // fgas::PrintHelp ()

//////////////////////////////////////////////////////////////////////

int
fgas::ParseParams
(
	int   argc,
	char* argv[]
)
{
	int m;
	int e;

	//
	// parse the commandline twice. First run to get the name
	// of the config file
	//
	while ( ( m=getopt ( argc, argv, "hv:" ) ) != -1 )
	{
		switch ( m )
		{
		case 'h':
			PrintHelp ();
			return ( fgms::FAILED );
		case 'v':
			break;
		default:
			cerr << endl;
			return ( fgms::FAILED );
		}
	}
	if ( optind >= argc )
	{
		PrintHelp ();
		cerr << "Expected configfilename." << endl;
		cerr << endl;
		return ( fgms::FAILED );
	}
	if ( optind < argc-1 )
	{
		PrintHelp ();
		cerr << "Too much parameters, only expected configfilename."
		     << endl;
		cerr << endl;
		return ( fgms::FAILED );
	}
	m_Properties.setStringValue ( "/fgas/config_name", argv[optind] );
	if ( ReadConfig ( argv[optind], m_Properties ) == fgms::FAILED )
	{
		return (fgms::FAILED);
	}
	//
	// Parse the commandline a second time, so that the
	// options overwrite the config file values
	//
	optind = 1; // reset index
	int v;
	while ( ( m=getopt ( argc, argv, "v:" ) ) != -1 )
	{
		switch ( m )
		{
		case 'v':
			v = StrToNum<int> ( optarg, e );
			if ( e )
			{
				cerr << "invalid value for Loglevel: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgas/log_level", v );
			sglog().setLogLevels ( SG_ALL, ( sgDebugPriority ) v);
			break;
		}
	}

        return ( fgms::SUCCESS );
} // fgas::ParseParams ()

//////////////////////////////////////////////////////////////////////

void
fgas::Register
()
{
	static time_t LastRegister = 0;
	static int register_count = 0;
	time_t now = time( 0 );

	if ( ( now - LastRegister ) < m_ResendTime )
		return;
	register_count++;
	#if 0
	if ( register_count > 3 )
	{
		SG_LOG ( SG_NETWORK, SG_WARN,
		  "No response to REGISTER, giving up..." );
		m_WantExit = true;
		return;
	}
	#endif
	fgms::t_PropertyList    SendProps;

	SendProps.push_back ( fgms::REGISTER_SERVER );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/clienttype"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/name"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/location"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/port"] );
	SG_LOG ( SG_NETWORK, SG_DEBUG,
	  "sending registration #" << register_count << "..." );
	m_Peer.SetTarget ( m_FGLS.Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // fgas::Register ()

//////////////////////////////////////////////////////////////////////

void
fgas::HandleAuthRequest
(
	t_ReceivePacket* Packet
)
{
	fgms::t_PropertyList SendProps;

	if ( Packet->ClientType != fgms::FGMS )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "received packet is not from fgms!" );
		SendProps.push_back ( fgms::AUTH_FAILED );
		WriteProps (m_Peer, m_Properties, SendProps );
		SendFailed (Packet->Sender, fgms::GENERAL_ERROR,
		  "Client type prohibited" );
		return;
	}
	if (m_State == REGISTER)
	{
		SendProps.push_back ( fgms::AUTH_FAILED );
		WriteProps (m_Peer, m_Properties, SendProps );
		SendFailed (Packet->Sender, fgms::GENERAL_ERROR,
		  "Authentication Service temporarily not available");
		return;
	}
	string	UserName = m_Properties.getStringValue (
	  "/multiplayer/registration/callsign" );
	string	UserPass = m_Properties.getStringValue (
	  "/multiplayer/registration/password" );
	SG_LOG ( SG_NETWORK, SG_INFO, "Authrequest User: '" << UserName
	  << "' Pass: '" << UserPass << "'" );
	m_Peer.SetTarget ( Packet->Sender );
	SendProps.push_back (
	  m_Prop2ID["/multiplayer/registration/client_id"] );
	// for testing, deny username 'fail'
	if ( UserName == "fgms::FAILED" )
	{
		SG_LOG ( SG_NETWORK, SG_INFO, "sending fgms::AUTH_FAILED..." );
		m_Properties.setStringValue (
		  "/multiplayer/system/errormessage",
		  "username 'fail', FAILED" );
		SendProps.push_back ( fgms::AUTH_FAILED );
		SendProps.push_back (
		  m_Prop2ID["/multiplayer/system/errormessage"] );
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
		return;
	}
	// FIXME: need a backend with user data to do actual authentication
	// SendFailed (Packet->Sender, fgms::AUTH_OK, "fgms::AUTH_OK");
	SG_LOG ( SG_NETWORK, SG_INFO, "sending fgms::AUTH_OK..." );
	SendProps.push_back ( fgms::AUTH_OK );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	m_Peer.DisableCrypt ();
} // fgas::HandleAuthRequest

//////////////////////////////////////////////////////////////////////

string
fgas::GetPassphrase
(
	const NetAddr &SenderAddress
)
{
	vector<SGPropertyNode_ptr> Map;
	vector<SGPropertyNode_ptr>::iterator i;
	SGPropertyNode_ptr AllowedClients;
	string SenderIP = SenderAddress.ToString();
	string SenderPass;

	AllowedClients = m_Properties.getNode ("/allowed_clients");
	if (AllowedClients == NULL)
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		   "no client specification found!" );
		return ("");
	}
	Map = AllowedClients->getChildren ("client");
	for (i = Map.begin(); i != Map.end(); i++)
	{
		if ((*i)->getStringValue ("addr") == SenderIP)
		{
			SenderPass = (*i)->getStringValue ("pass");
			SG_LOG ( SG_NETWORK, SG_DEBUG,
			  "for client " << SenderIP << " I found "
			  << "password '" << SenderPass << "'" );
			return ((*i)->getStringValue ("pass"));
		}
	}
	return ("");
} // fgas::GetPassphrase ()

//////////////////////////////////////////////////////////////////////

void
fgas::SendFailed
(
	const NetAddr & Sender,
	const uint32_t Cmd,
	const string & Msg
)
{
	fgms::t_PropertyList SendProps;

	SendProps.push_back ( Cmd );
	SendProps.push_back ( m_Prop2ID["/multiplayer/system/errormessage"] );
	m_Properties.setStringValue (
	  "/multiplayer/system/errormessage", Msg.c_str() );
	SG_LOG ( SG_NETWORK, SG_DEBUG, "sending " << Msg << "..." );
	m_Peer.SetTarget (Sender);
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // SendFailed ()

//////////////////////////////////////////////////////////////////////

void
fgas::HandleCommand
(
	int Command,
	const NetAddr& Sender,
	t_ReceivePacket* Packet
)
{
	uint64_t Tmp = m_Peer.GetSenderID ();
	switch (Command)
	{
	case fgms::REGISTER_OK:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "Registered with FGLS, got client id " << Tmp );
		m_FGLS.LastSeen = time ( 0 );
		m_FGLS.RegisteredAt = m_FGLS.LastSeen;
		m_State = RUNNING;
		break;
	case fgms::GENERAL_ERROR:
		SG_LOG ( SG_NETWORK, SG_ALERT, m_Properties.getStringValue (
		  "/multiplayer/system/errormessage") );
		break;
	case fgms::AUTH_CLIENT:
		HandleAuthRequest ( Packet );
		break;
	case fgms::SET_CLIENT_ID:
		// SetSenderID ();
		break;
	case fgms::REGISTER_AGAIN:
		m_State = REGISTER;
		break;
	case fgms::PING:
		if ( Sender == m_FGLS.Addr )
		{
			m_FGLS.LastSeen = time ( 0 );
			m_FGLS.NumPingsRcvd++;
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "got PING from FGLS " << Sender );
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "got PING from UNKNOWN " << Sender );
		}
		break;
	case fgms::PONG:
		if ( Sender == m_FGLS.Addr )
		{
			m_FGLS.LastSeen = time ( 0 );
			m_FGLS.NumPongs++;
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "got PONG from FGLS " << Sender );
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "got PONG from UNKNOWN " << Sender );
		}
		break;
	default:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "unhandled command: " << Command );
	}
} // fgas::HandleCommand ()

//////////////////////////////////////////////////////////////////////

void
fgas::Maintainance
()
{
	fgms::t_PropertyList SendProps;
	time_t now = time ( 0 );
	time_t timeout = m_CheckInterval * 2; // miss 3 pings

	time_t diff = now - m_FGLS.LastSeen;
	if ( diff > timeout )
	{	// register again
		m_State = REGISTER;
		SG_LOG ( SG_NETWORK, SG_ALERT, "FLGS: last seen "
		  << now - m_FGLS.LastSeen
		  << " seconds ago, registering again" );
		return;
	}
	if ( diff < m_CheckInterval )
	{
		return;
	}
	//////////////////////////////////////////////////
	// PING fgls
	//////////////////////////////////////////////////
	SendProps.push_back ( fgms::PING );
	m_Peer.SetTarget ( m_FGLS.Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending PING to fgls..." << m_FGLS.Addr );
	m_FGLS.NumPingsSent++;
	m_Peer.Send ();
} // fgas::Maintainance

//////////////////////////////////////////////////////////////////////

void
fgas::HandleRequest
()
{
	NetAddr	Sender;
	string	SenderPassphrase;
	t_ReceivePacket*  Packet;

	m_Peer.Receive ();
	while (m_Peer.HasPackets())
	{
		Sender = m_Peer.GetSender ();
#ifdef ENABLE_ENCRYPTION
		SenderPassphrase = GetPassphrase (Sender);
		if (SenderPassphrase == "")
		{
			SG_LOG ( SG_SYSTEMS, SG_ALERT,
			  "no passphrase for client "
			  << Sender.ToString());
			SendFailed (Sender, fgms::GENERAL_ERROR,
			  "client is not allowed");
			return;
		}
		m_Peer.EnableCrypt (SenderPassphrase);
#endif
		try
		{
			Packet = m_Peer.NextPacket ();
		}
		catch (sg_exception & ex)
		{
			SendFailed (Sender, fgms::GENERAL_ERROR,
			  ex.getFormattedMessage () );
			continue;
		}
		if (Packet == 0)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "no packet" );
			continue;
		}
		fgms::t_PropertyList RcvdProps;
		ParsePacket ( Packet, m_Peer, RcvdProps, m_Properties );
		while ( RcvdProps.size () > 0 )
		{
			int Command = RcvdProps.front ();
			RcvdProps.pop_front ();
			if (Command < fgms::INTERNAL_COMMAND)
			{
				HandleCommand (Command, Sender, Packet);
			}
		}
	}
} // fgas::HandleRequest

//////////////////////////////////////////////////////////////////////

void
fgas::Loop
()
{
	int Client;
	time_t now;
	static time_t LastCheck = time ( 0 );

	while ( m_WantExit == false )
	{
		if (m_State == REGISTER)
		{
			Register ();
		}
		Client = m_Peer.WaitForClients ( m_ResendTime );
		if (Client == UDP_Peer::ne_TIMEOUT)
		{
			// SG_LOG ( SG_NETWORK, SG_DEBUG, "sending queue..." );
			// m_Peer.Send ();
		}
		else if (! Client)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "fgas::Loop() - Bytes <= 0!");
			continue;
		}
		else if (Client > 0)
		{	// something on the wire (clients)
			HandleRequest ();
		}
		now = time ( 0 );
		if ( ( now - LastCheck ) >= m_CheckInterval )
		{
			Maintainance ();
			LastCheck = now;
		}
	}
} // fgas::Loop ()

//////////////////////////////////////////////////////////////////////

fgas Server;

void
SigHandler
(
	int SigNum
)
{
	SG_LOG ( SG_NETWORK, SG_ALERT, "caught signal " << SigNum );
	SG_LOG ( SG_NETWORK, SG_ALERT, "exiting... " );
	Server.WantExit ();
	signal ( SigNum, SigHandler );
}

//////////////////////////////////////////////////////////////////////

int
main
(
	int argc,
	char* argv[]
)
{
	if (! Server.ParseParams ( argc, argv ) )
	{
		return (fgms::FAILED);
	}
	if (! Server.Init () )
	{
		return (fgms::FAILED);
	}
	signal ( SIGINT, SigHandler );
	Server.Loop ();
	Server.Done ();
	return (fgms::SUCCESS);
}

