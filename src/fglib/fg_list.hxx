/// @file fg_list.hxx
/// Thread safe list implementation
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2006-2015
/// @copyright	GPLv3
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef FG_BASE_LIST_HEADER
#define FG_BASE_LIST_HEADER

#include <iostream>
#include <memory>

namespace fgms
{

template <typename T> class list;
template <typename T> class list_iterator;

template <typename T>
class list_element
{
protected:
	friend class	list<T>;
	friend class	list_iterator<T>;
	typedef T	value_t;
	typedef T&	reference_t;
	value_t		m_data;
	std::shared_ptr<list_element>	m_prev;
	std::shared_ptr<list_element>	m_next;

	list_element (): m_prev(0), m_next(0) {};
	list_element ( const T& v ): m_data(v), m_prev(0), m_next(0) {};
	reference_t get_data () { return m_data; };
	virtual bool operator ==  ( const list_element & e )
	{
		if ( m_data == e.m_data )
			return true;
		return false;
	};
	virtual bool operator !=  ( const list_element & e )
	{
		if ( m_data == e.m_data )
			return false;
		return true;
	};
private:
	list_element ( const list_element& e );
};

template <typename T>
class list_iterator
{
	friend class	list<T>;
	typedef T	value_t;
	typedef T&	reference_t;
	typedef T*	pointer_t;
	typedef std::shared_ptr< list_element<T> > element_p;
protected:
	element_p	m_current;
public:
	list_iterator (): m_current(0) {};

	list_iterator ( const list_iterator & i )
	{
		this->assign ( i );
	};

	list_iterator ( const element_p & e )
	{
		m_current = e;
	};

	pointer_t operator->( void ) const
	{
		return & ( m_current->m_data );
	};

	reference_t operator * ()
	{
		return  m_current->m_data;
	};

	list_iterator operator ++ ( int )
	{
		if ( m_current != 0 )
		{
			m_current = m_current->m_next;
		}
		list_iterator i (m_current);
		return i;
	};

	list_iterator operator -- ( const int )
	{
		if ( m_current != 0 )
		{
			m_current = m_current->m_prev;
		}
		list_iterator i (m_current);
		return i;
	};

	bool operator == ( const list_iterator & i ) const
	{
		if ( m_current == i.m_current )
			return true;
		return false;
	};

	bool operator == ( const int & i ) const
	{
		if ( m_current == i )
			return true;
		return false;
	};

	bool operator != ( const list_iterator & i ) const
	{
		if ( m_current == i.m_current )
			return false;
		return true;
	};

	bool operator != ( const int & i ) const
	{
		if ( m_current == i )
			return false;
		return true;
	};

	void operator = ( const list_iterator & i )
	{
		assign ( i );
	};

	void operator = ( const element_p & e )
	{
		m_current = e;
	};

	bool is_valid ()
	{
		if (m_current)
			return true;
		return false;
	};

	void invalidate ()
	{
		m_current = 0;
	};

private:
	void assign ( const list_iterator& i )
	{
		m_current = i.m_current;
	};
}; // class list_iterator<T>

template <class T>
class list
{
public:
	typedef list_iterator<T>	iterator;
	typedef list_element<T> 	element_t;
	typedef std::shared_ptr< list_element<T> >	element_p;
	
	list (): m_size(0), m_first (0), m_last(0), m_max(0) {};

	~list ()
	{
		clear ();
	};

	size_t size ()
	{
		return ( m_size );
	};

	void clear ()
	{
		iterator i, j;
		i = m_first;
		while ( i != end_of_list )
		{
			j = i;
			i++;
			j.m_current->m_prev = 0;
			j.m_current->m_next = 0;
			j.m_current.reset ();
		}
		m_size  = 0;
		m_first = 0;
		m_last  = 0;
	};

	iterator push_back ( const T& elem )
	{
		element_p new_elem = std::shared_ptr<element_t> (new element_t ( elem ) );
		if ( m_first == 0 )
		{
			m_first = new_elem;
			m_last  = new_elem;
			m_max++;
			m_size++;
			iterator i (m_first); // = m_first;
			return i;
		}
		m_max++;
		m_size++;
		new_elem->m_prev = m_last;
		m_last->m_next   = new_elem;
		m_last = new_elem;
		iterator i ( m_last );
		return i;
	};

	iterator begin ()
	{
		iterator i ( m_first );
		return i;
	};

	iterator& end ()
	{
		return end_of_list;
	};

	iterator erase ( const iterator& i )
	{
		element_p current = i.m_current;
		if ( current == 0 )
			return end_of_list;
		if ( m_first == current )
		{
			m_first = current->m_next;
			if ( m_first != 0)
			{
				m_first->m_prev = 0;
			}
		}
		if ( m_last == current )
		{
			m_last = current->m_prev;
			if ( m_last != 0)
			{
				m_last->m_next = 0;
			}
		}
		element_p j = current->m_next;
		if ( j != 0 )
		{
			j->m_prev = current->m_prev;
			if ( j->m_prev != 0 )
				j->m_prev->m_next = j;
		}
		current->m_prev = 0;
		current->m_next = 0;
		m_size--;
		return j;
	}

private:
	size_t		m_size;
	size_t		m_max;
	element_p	m_first;
	element_p	m_last;
	iterator	end_of_list;
}; // class list<T>

}; // namespace fgms

#endif
