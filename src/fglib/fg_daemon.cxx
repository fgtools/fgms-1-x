//////////////////////////////////////////////////////////////////////
//
// implement the class "cDaemon", which does everything necessary
// to become a daemon
//
// This file is part of fgms.
//
// Copyright (C) 2006 Oliver Schroeder <fgms@postrobot.de>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see
// <http://www.gnu.org/licenses/>.
//
//////////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/stat.h>
#ifdef _MSC_VER
#include <process.h> // getpid()
typedef int pid_t;
#else
#include <sys/wait.h>
#endif
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <iostream>
#include <simgear/debug/logstream.hxx>
#include "fg_daemon.hxx"

pid_t cDaemon::PidOfDaemon;     // remember who we are
list <pid_t> cDaemon::Children; // keep track of our children

//////////////////////////////////////////////////////////////////////
// SigHandler ()
//////////////////////////////////////////////////////////////////////
#if 0
void cDaemon::SigHandler ( int SigType )
{
    if (SigType == SIGCHLD)
    {
        int stat;
        while (waitpid (-1, &stat, WNOHANG) > 0)
            /* intentionally empty */ ;
        signal (SigType,SigHandler);
        return;
    }
    switch (SigType)
    {
    case  1:
        SG_LOG ( SG_NETWORK, SG_ALERT, "caught SIGHUP");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Hangup (POSIX)");
        break;
    case  2:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGINT! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Interrupt (ANSI)");
        break;
    case  3:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGQUIT! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Quit (POSIX)");
        break;
    case  4:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGILL! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Illegal instruction (ANSI)");
        break;
    case  5:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGTRAP! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Trace trap (POSIX)");
        break;
    case  6:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGABRT! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "IOT trap (4.2 BSD)");
        break;
    case  7:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGBUS! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "BUS error (4.2 BSD)");
        break;
    case  8:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGFPE! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Floating-point exception (ANSI)");
        break;
    case  9: // never caught!
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGKILL! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Kill, unblockable (POSIX)");
        break;
    case 10:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGUSR1! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "User-defined signal 1 (POSIX)");
        break;
    case 11:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGSEGV! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Segmentation violation (ANSI)");
        break;
    case 12:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGUSR2! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "User-defined signal 2 (POSIX)");
        break;
    case 13:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGPIPE! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Broken pipe (POSIX)");
        break;
    case 14:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGALRM! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Alarm clock (POSIX)");
        break;
    case 15:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGTERM! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Termination (ANSI)");
        break;
    case 16:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGSTKFLT! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Stack fault");
        break;
    case 17:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGCHLD! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Child status has changed (POSIX)");
        break;
    case 18:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGCONT! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Continue (POSIX)");
        break;
    case 19:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGSTOP! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Stop, unblockable (POSIX)");
        break;
    case 20:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGTSTP! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Keyboard stop (POSIX)");
        break;
    case 21:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGTTIN! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Background read from tty (POSIX)");
        break;
    case 22:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGTTOU! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Background write to tty (POSIX)");
        break;
    case 23:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGURG! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Urgent condition on socket (4.2 BSD)");
        break;
    case 24:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGXCPU! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "CPU limit exceeded (4.2 BSD)");
        break;
    case 25:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGXFSZ! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "File size limit exceeded (4.2 BSD)");
        break;
    case 26:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGVTALRM! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Virtual alarm clock (4.2 BSD)");
        break;
    case 27:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGPROF! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Profiling alarm clock (4.2 BSD)");
        break;
    case 28:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGWINCH! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Window size change (4.3 BSD, Sun)");
        break;
    case 29:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGIO! ");
        SG_LOG ( SG_NETWORK, SG_ALERT, "I/O now possible (4.2 BSD)");
        break;
    case 30:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by SIGPWR!");
        SG_LOG ( SG_NETWORK, SG_ALERT, "Power failure restart (System V)");
        break;
    default:
        SG_LOG ( SG_NETWORK, SG_ALERT, "killed by signal " << SigType << "!");
    }
    cDaemon::KillAll ();
    signal (SigType,SigHandler);
}
#endif

//////////////////////////////////////////////////////////////////////
// Daemonize ()
// installs the signal-handler and makes ourself a daemon
//////////////////////////////////////////////////////////////////////
int cDaemon::Daemonize () // make us a daemon
{
#ifndef _MSC_VER
    pid_t pid;

    //
    // fork to get rid of our parent
    //
    if ( (pid = fork ()) < 0)
        return (-1);    // something went wrong!
    else if ( pid > 0 ) // parent-process
    {
        PidOfDaemon = 0;
        exit (0);       // good-bye dad!
    }
    //
    // well, my child, do well!
    //
    // close unused file descriptors (not necessary)
    // int t;
    // for (t = 32; t >= 3; t--)
    //   close(t);
    PidOfDaemon = getpid();
    setsid ();      // become a session leader
    // chdir ("/"); // make sure, we're not on a mounted fs
    umask (0);      // clear the file creation mode
#endif // !_MSC_VER
    return (0);     // ok, that's all volks!
}

//////////////////////////////////////////////////////////////////////
// kill our children and ourself
//////////////////////////////////////////////////////////////////////
#if 0
void cDaemon::KillAll ()
{
    list <pid_t>::iterator aChild;

    aChild = Children.begin ();
    while ( aChild != Children.end () )
    {
        if ( kill ((*aChild), SIGTERM))
            kill ((*aChild), SIGKILL);
        aChild++;
    }
    exit (0);
}
#endif

//////////////////////////////////////////////////////////////////////
// AddChild ()
// inserts the ChildsPid in the list of our Children.
// So we can keep track of them and kill them if necessary,
// e.g. the daemon dies.
//////////////////////////////////////////////////////////////////////
#if 0
void cDaemon::AddChild ( pid_t ChildsPid )
{
    Children.push_back (ChildsPid);
}

int cDaemon::NumChilds ()
{
    return (Children.size ());
}
#endif

cDaemon::cDaemon()
{
    //
    // catch some signals
    //
#if 0
    signal (SIGINT,SigHandler);
    signal (SIGHUP,SigHandler);
    signal (SIGTERM,SigHandler);
    signal (SIGCHLD,SigHandler);
#endif
    PidOfDaemon = getpid();
}

cDaemon::~cDaemon ()
{
//    KillAll ();
}

